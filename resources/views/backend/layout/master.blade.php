<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title> @yield('title') | Dashboard</title>
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <meta name="description" content="" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta property="og:title" content="" />
    <meta property="og:type" content="" />
    <meta property="og:url" content="" />
    <meta property="og:image" content="" />
    <!-- Favicon -->
    @include('backend.includes.csslink')
</head>

<body>
    <div class="screen-overlay"></div>
    <aside class="navbar-aside" id="offcanvas_aside">
        <div class="aside-top">
            <a href="{{ route('dashboard.index') }}" class="brand-wrap">
                <img src="{{ asset('backend/assets/imgs/theme/logo.svg') }}" class="logo" alt="Nest Dashboard" />
            </a>
            <div>
                <button class="btn btn-icon btn-aside-minimize"><i
                        class="text-muted material-icons md-menu_open"></i></button>
            </div>
        </div>
        @include('backend.includes.sidebar')
    </aside>
    <main class="main-wrap pb-5">
        @include('backend.includes.header')
        @yield('contant')
        <!-- content-main end// -->

    </main>
    @include('backend.includes.footer')
    @include('backend.includes.script')
    @stack('script')
</body>

</html>
