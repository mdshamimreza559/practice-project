@extends('backend.layout.master')
@section('title', 'Nest')
@section('contant')
    <section class="content-main">
        <div class="content-header">
            <div>
                <h2 class="content-title card-title">Dashboard</h2>
                <p>Whole data about your business here</p>
            </div>
            <div>
                <a href="#" class="btn btn-primary"><i class="text-muted material-icons md-post_add"></i>Create
                    report</a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="card card-body mb-4">
                    <article class="icontext">
                        <span class="icon icon-sm rounded-circle bg-primary-light"><i
                                class="text-primary material-icons md-monetization_on"></i></span>
                        <div class="text">
                            <h6 class="mb-1 card-title">Customer</h6>
                            <span>{{ count($user) }}</span>
                        </div>
                    </article>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card card-body mb-4">
                    <article class="icontext">
                        <span class="icon icon-sm rounded-circle bg-success-light"><i
                                class="text-success material-icons md-local_shipping"></i></span>
                        <div class="text">
                            <h6 class="mb-1 card-title">Orders</h6>
                            <span>{{ count($order) }}</span>
                        </div>
                    </article>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card card-body mb-4">
                    <article class="icontext">
                        <span class="icon icon-sm rounded-circle bg-warning-light"><i
                                class="text-warning material-icons md-qr_code"></i></span>
                        <div class="text">
                            <h6 class="mb-1 card-title">Products</h6>
                            <span>{{ count($products) }}</span>
                            <span class="text-sm"> In {{ $categories->count() }} Categories </span>
                        </div>
                    </article>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="card card-body mb-4">
                    <article class="icontext">
                        <span class="icon icon-sm rounded-circle bg-info-light"><i
                                class="text-info material-icons md-shopping_basket"></i></span>
                        <div class="text">
                            <h6 class="mb-1 card-title">Monthly Earning</h6>
                            <span>$6,982</span>
                            <span class="text-sm"> Based in your local time. </span>
                        </div>
                    </article>
                </div>
            </div>
        </div>

        <div class="card mb-4">
            <header class="card-header">
                <h4 class="card-title">Latest orders</h4>
                <form action="{{ route('order.search') }}" method="get">
                    @csrf
                    <div class="row align-items-center">

                        <div class="col-md-5">


                            <div class="input-group">
                                <input name="order_search" type="search" class="form-control form-control-sm"
                                    placeholder="search...">

                            </div>


                        </div>
                        <div class="col-md-2 col-6">
                            <input type="text" name="date" value="" class="form-control"
                                placeholder="date..." />
                        </div>
                        <div class="col-md-2 col-6">
                            <div class="custom_select">
                                <select name="condition" class="form-select select-nice">
                                    <option value="">Status</option>
                                    <option value="pending">pending</option>
                                    <option value="processing"> processing</option>
                                    <option value="delivered">delivered</option>
                                    <option value="cancelled">cancelled</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <button class="btn btn-sm btn-success">submit</button>
                        </div>
                    </div>
                </form>
            </header>
            <div class="card-body">
                <div class="table-responsive">
                    <div class="table-responsive">
                        <table class="table align-middle table-nowrap mb-0">
                            <thead class="table-light">
                                <tr>
                                    <th scope="col" class="text-center">
                                        <div class="form-check align-middle">
                                            <input class="form-check-input" type="checkbox" id="transactionCheck01" />
                                            <label class="form-check-label" for="transactionCheck01"></label>
                                        </div>
                                    </th>
                                    <th class="align-middle" scope="col">Order ID</th>
                                    <th class="align-middle" scope="col">Email</th>
                                    <th class="align-middle" scope="col">Date</th>
                                    <th class="align-middle" scope="col">Total</th>
                                    <th class="align-middle" scope="col">delivery status</th>
                                    <th class="align-middle" scope="col">Payment Status</th>
                                    <th class="align-middle" scope="col">Payment Method</th>
                                    <th class="align-middle" scope="col">View Details</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($orders as $data)
                                    <tr>
                                        <td class="text-center">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="transactionCheck02" />
                                                <label class="form-check-label" for="transactionCheck02"></label>
                                            </div>
                                        </td>
                                        <td><a href="#" class="fw-bold">#{{ $data->order_number }}</a></td>
                                        <td>{{ $data->email }}</td>
                                        <td>
                                            <span
                                                style="color:#0fad01">{{ Carbon\Carbon::parse($data->created_at)->format('d-M-Y ') }}
                                            </span>

                                            <span
                                                style="color:#0445a4">{{ Carbon\Carbon::parse($data->created_at)->format('g:ia') }}
                                            </span>
                                        </td>
                                        <td>${{ $data->total_amount }}</td>
                                        <td>
                                            <span class="text-success">{{ $data->condition }}</span>
                                        </td>
                                        <td>
                                            <span class="text-success">{{ $data->payment_status }}</span>
                                        </td>
                                        @if ($data->payment_method == 'cod')
                                            <td>Cash On Delivery </td>
                                        @else
                                            <td><i class="material-icons md-payment font-xxl text-muted mr-5"></i>
                                            </td>
                                        @endif

                                        <td>
                                            <a href="{{ route('order.show', $data->id) }}" class="btn btn-xs"> View
                                                details</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- table-responsive end// -->
            </div>
        </div>



        {{ $orders->links('vendor.pagination.custom') }}


    </section>
@endsection

@push('script')
    <script>
        $(function() {

            $('input[name="date"]').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $('input[name="date"]').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format(
                    'MM/DD/YYYY'));
            });

            $('input[name="date"]').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
            });

        });
    </script>
@endpush
