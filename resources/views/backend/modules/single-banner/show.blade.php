@extends('backend.layout.master')

@section('title', 'Single Banner Show')

@section('contant')
    <div class="container">
        <div class="row justify-content-center py-5">
            <div class="col-lg-7">
                <div class="card">
                    <div class="card-header">
                        <h4>@yield('title')</h4>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-bordered table-hover table-sm">
                            <tbody>
                                @php
                                    $status = $singleBanner->status;
                                    $create = $singleBanner->created_at;
                                    $update = $singleBanner->updated_at;
                                @endphp
                                <tr>
                                    <th>ID </th>
                                    <td>{{ $singleBanner->id }}</td>
                                </tr>
                                <tr>
                                    <th> Title </th>
                                    <td>{{ $singleBanner->name }}</td>
                                </tr>
                                <tr>
                                    <th> status </th>
                                    @if ($status == 1)
                                        <td class="text-success">Active</td>
                                    @else
                                        <td class="text-danger">Inactive</td>
                                    @endif
                                </tr>
                                <tr>
                                    <th> Price </th>
                                    <td>{{ $singleBanner->price }}</td>
                                </tr>
                                <tr>
                                    <th> Short Description </th>
                                    <td>{{ $singleBanner->short_description }}</td>
                                </tr>
                                <tr>
                                    <th> Description </th>
                                    <td>{{ $singleBanner->description }}</td>
                                </tr>
                                <tr>
                                    <th> Created At </th>
                                    <td>{{ $singleBanner->created_at->toDayDateTimeString() }}</td>
                                </tr>
                                <tr>
                                    <th> Created At </th>
                                    @if ($create == $update)
                                        <td class="text-danger">Not updated yet</td>
                                    @else
                                        <td class="text-success">{{ $update->toDayDateTimeString() }}</td>
                                    @endif
                                </tr>
                                <tr>
                                    <th>Photo</th>
                                    <td><img width="190px"
                                            src="{{ asset('image/banner/single-banner/' . $singleBanner->photo) }}"
                                            alt="{{ $singleBanner->name }}">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <a href="{{ route('single-banner.index') }}">
                            <button class="btn btn-success btn-sm">Back</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
