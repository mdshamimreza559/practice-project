@extends('backend.layout.master')

@section('title', 'Single Banner List')

@section('contant')
    <div class="container">
        <div class="row py-5">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h4>@yield('title')</h4>
                        <a href="{{ route('single-banner.create') }}">
                            <button class="btn btn-sm btn-success">create</button>
                        </a>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>Title</th>
                                    <th>Status</th>
                                    <th>Short Description</th>
                                    <th>Photo</th>
                                    <th>Time</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($models as $banner)
                                    @php
                                        $create = $banner->created_at;
                                        $update = $banner->updated_at;
                                        $status = $banner->status;
                                    @endphp
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $banner->name }}</td>
                                        <td class="text-center">
                                            <div class="form-check form-switch">
                                                <input class="form-check-input" value="{{ $banner->id }}" type="checkbox"
                                                    name="status" <?php echo $status == 1 ? 'checked' : ''; ?> role="switch"
                                                    id="flexSwitchCheckDefault">
                                            </div>
                                        <td>{{ $banner->short_description }}</td>
                                        <td><img width="100px"
                                                src="{{ asset('image/banner/single-banner/' . $banner->photo) }}"
                                                alt=""></td>
                                        <td>
                                            <p class="mb-0"><small>{{ $create->toDayDateTimeString() }}</small></p>
                                            <p class="mb-0 text-success">
                                                <small>{{ $create == $update ? 'Not Updated yet' : $update->toDayDateTimeString() }}</small>
                                            </p>
                                        </td>
                                        <td>
                                            <div class="d-flex">
                                                <a href="{{ route('single-banner.show', $banner->id) }}">
                                                    <button class="btn btn-sm btn-success me-1">
                                                        <i class="fa-solid fa-eye    "></i>
                                                    </button>
                                                </a>
                                                <a href="{{ route('single-banner.edit', $banner->id) }}">
                                                    <button class="btn btn-sm btn-warning me-1">
                                                        <i class="fa-sloid fa-edit    "></i>
                                                    </button>
                                                </a>
                                                {!! Form::open([
                                                    'method' => 'delete',
                                                    'id' => 'banner_delete_form_' . $banner->id,
                                                    'route' => ['single-banner.destroy', $banner->id],
                                                ]) !!}
                                                {!! Form::button('<i class="fa-solid fa-trash"></i>', [
                                                    'class' => 'btn btn-sm btn-danger banner_delete_btn',
                                                    'data-id' => $banner->id,
                                                    'id' => 'banner_delete_btn_' . $banner->id,
                                                ]) !!}
                                                {!! Form::close() !!}
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if (Session::has('msg'))
        @push('script')
            <script>
                Swal.fire({
                    position: 'top-end',
                    toast: true,
                    icon: '<?php echo session('cls'); ?>',
                    title: '<?php echo session('msg'); ?>',
                    showConfirmButton: false,
                    timer: 2000
                })
            </script>
        @endpush
    @endif
@endsection

@push('script')
    <script>
        $("input[name=status]").on('change', function() {
            let value = $(this).prop('checked') == true ? 1 : 2;
            let id = $(this).val();
            $.ajax({
                type: "post",
                url: "{{ route('single.status') }}",
                data: {
                    _token: "{{ csrf_token() }}",
                    mode: value,
                    id: id,
                },
                success: function(response) {
                    if (response.status == 1) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            toast: true,
                            title: 'single banner status active successfully',
                            showConfirmButton: false,
                            timer: 2000
                        })
                    } else {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            toast: true,
                            title: 'single banner status inactive successfully',
                            showConfirmButton: false,
                            timer: 2000
                        })
                    }
                }

            })
        })

        $('.banner_delete_btn').on('click', function() {
            let id = $(this).attr('data-id')
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#banner_delete_form_' + id).submit()
                }
            })
        })
    </script>
@endpush
