{!! Form::label('name', 'Title') !!}
{!! Form::text('name', null, [
    'class' => 'form-control form-control-sm',
    'placeholder' => 'Insert single banner title',
]) !!}
@error('name')
    <p><small>{{ $message }}</small></p>
@enderror
{!! Form::label('status', 'Status', ['class' => 'mt-4']) !!}
{!! Form::select('status', [1 => 'Active', 2 => 'Inactive'], null, [
    'class' => 'form-select form-select-sm ',
    'placeholder' => 'Insert single banner status',
]) !!}
@error('status')
    <p><small>{{ $message }}</small></p>
@enderror

{!! Form::label('short_description', 'Short Description', ['class' => 'mt-4']) !!}
{!! Form::textarea('short_description', null, [
    'class' => 'form-control form-control-sm',
    'placeholder' => 'Insert single banner short description',
]) !!}
{!! Form::label('photo', 'Photo', ['class' => 'mt-4']) !!}
{!! Form::file('photo', ['id' => 'photo', 'class' => 'form-control form-control-sm']) !!}
@error('photo')
    <p><small>{{ $message }}</small></p>
@enderror


@push('script')
    <script>
        $('#photo').on('change', function(e) {
            let file = e.target.files[0]
            file = URL.createObjectURL(file)
            $('#banner_img').attr('src', file)
            $('.show-banner').show()
        })
    </script>
@endpush
