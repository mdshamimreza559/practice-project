{!! Form::label('name', 'Name') !!}
{!! Form::text('name', null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Membar Name..']) !!}
@error('name')
    <p class="text-danger position-absolute"><small>{{ $message }}</small></p>
@enderror

{!! Form::label('skills', 'Skills', ['class' => 'mt-4']) !!}
{!! Form::text('skills', null, ['class' => 'form-control form-control-sm', 'placeholder' => 'member skills..']) !!}
@error('skills')
    <p class="text-danger position-absolute"><small>{{ $message }}</small></p>
@enderror

{!! Form::label('email', 'Email', ['class' => 'mt-4']) !!}
{!! Form::email('email', null, [
    'class' => 'form-control form-control-sm',
    'placeholder' => 'e.g member@gmail.com',
]) !!}
@error('skills')
    <p class="text-danger position-absolute"><small>{{ $message }}</small></p>
@enderror

{!! Form::label('status', 'Status', ['class' => 'mt-4']) !!}
{!! Form::select('status', [1 => 'Active', 2 => 'Inactive'], null, [
    'class' => 'form-select form-select-sm',
    'placeholder' => 'Select member status',
]) !!}
@error('status')
    <p class="text-danger position-absolute"><small>{{ $message }}</small></p>
@enderror

{!! Form::label('fb_link', 'FaceBook Link', ['class' => 'mt-4']) !!}
{!! Form::text('fb_link', null, [
    'class' => 'form-control form-control-sm',
    'placeholder' => 'fb_link...',
]) !!}
@error('fb_link')
    <p class="text-danger position-absolute"><small>{{ $message }}</small></p>
@enderror

{!! Form::label('twitter_link', 'Twitter Link', ['class' => 'mt-4']) !!}
{!! Form::text('twitter_link', null, [
    'class' => 'form-control form-control-sm',
    'placeholder' => 'twitter_link...',
]) !!}
@error('twitter_link')
    <p class="text-danger position-absolute"><small>{{ $message }}</small></p>
@enderror

{!! Form::label('linkdin_link', 'Linkdin Link', ['class' => 'mt-4']) !!}
{!! Form::text('linkdin_link', null, [
    'class' => 'form-control form-control-sm',
    'placeholder' => 'linkdin_link...',
]) !!}
@error('linkdin_link')
    <p class="text-danger position-absolute"><small>{{ $message }}</small></p>
@enderror

{!! Form::label('google_link', 'Google Link', ['class' => 'mt-4']) !!}
{!! Form::text('google_link', null, [
    'class' => 'form-control form-control-sm',
    'placeholder' => 'googel_link...',
]) !!}
@error('google_link')
    <p class="text-danger position-absolute"><small>{{ $message }}</small></p>
@enderror

{!! Form::label('photo', 'Photo', ['class' => 'mt-4']) !!}
{!! Form::file('photo', ['class' => 'member_photo form-control form-control-sm']) !!}
@error('photo')
    <p class="text-danger position-absolute"><small>{{ $message }}</small></p>
@enderror

<div id="member_photo" class="mt-2" style="display: none">
    <img id="show_member_photo" alt="">
</div>


@push('script')
    <script>
        $('.member_photo').on('change', function(e) {
            let files = e.target.files[0]
            files = URL.createObjectURL(files)
            $('#show_member_photo').attr('src', files)
            $('#member_photo').show()
        })
    </script>
@endpush
