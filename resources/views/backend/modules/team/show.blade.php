@extends('backend.layout.master')
@section('title', 'Member Details')
@section('contant')
    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4>@yield('title')</h4>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover table-bordered table-sm">
                            <tbody>
                                <tr>
                                    <th> ID</th>
                                    <td> {{ $ourTeam->id }}</td>
                                </tr>
                                <tr>
                                    <th> Member Name</th>
                                    <td> {{ ucfirst($ourTeam->name) }}</td>
                                </tr>
                                <tr>
                                    <th> Skills </th>
                                    <td> {{ ucfirst($ourTeam->skills) }}</td>
                                </tr>
                                <tr>
                                    <th> Email </th>
                                    <td> {{ $ourTeam->email }}</td>
                                </tr>
                                <tr>
                                    @php
                                        $status = $ourTeam->status;
                                    @endphp
                                    <th> Status </th>
                                    @if ($status == 1)
                                        <td class="text-success"> Active </td>
                                    @else
                                        <td class="text-danger"> Inactive </td>
                                    @endif

                                </tr>
                                <tr>
                                    <th> Facebook </th>
                                    <td> {{ $ourTeam->fb_link }}</td>
                                </tr>
                                <tr>
                                    <th> Twitter </th>
                                    <td> {{ $ourTeam->twitter_link }}</td>
                                </tr>
                                <tr>
                                    <th> Linkdin </th>
                                    <td> {{ $ourTeam->linkdin_link }}</td>
                                </tr>
                                <tr>
                                    <th> Google </th>
                                    <td> {{ $ourTeam->google_link }}</td>
                                </tr>
                                <tr>
                                    <th> Created At </th>
                                    <td> {{ Carbon\Carbon::parse($ourTeam->created_at)->format('d-M-Y') }}</td>
                                </tr>
                                <tr>
                                    <th> Updated At </th>
                                    <td> {{ $ourTeam->updated_at->toDayDateTimeString() }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <a href="{{ route('teams.index') }}">
                            <button class="btn btn-success btn-sm">Back</button>
                        </a>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection
