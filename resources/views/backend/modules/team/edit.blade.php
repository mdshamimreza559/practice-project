@extends('backend.layout.master')
@section('title', 'Update Team Member')
@section('contant')
    <div class="row mt-3 justify-content-center">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">@yield('title')</div>
                <div class="card-body">
                    {!! Form::model($ourTeam, ['method' => 'put', 'route' => ['member.update', $ourTeam->id], 'files' => true]) !!}
                    @include('backend.modules.team.form');
                    <div class="member-img">
                        <img src="{{ asset('image/team/' . $ourTeam->photo) }}" alt="">
                    </div>
                    {!! Form::button('submit', ['class' => 'btn btn-sm btn-success mt-4', 'type' => 'submit']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
