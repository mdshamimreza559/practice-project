@extends('backend.layout.master')
@section('title', 'Add Team Member')
@section('contant')
    <div class="row justify-content-center mt-4">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h4>@yield('title')</h4>
                </div>
                <div class="card-body">
                    {!! Form::open(['route' => 'teams.store', 'method' => 'post', 'files' => true]) !!}
                    @include('backend.modules.team.form');
                    {!! Form::button('submit', ['class' => 'btn btn-sm btn-success mt-4', 'type' => 'submit']) !!}
                    {!! Form::close() !!}
                </div>
            </div>

        </div>
    </div>
@endsection
