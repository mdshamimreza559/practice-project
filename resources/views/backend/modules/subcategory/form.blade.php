{!! Form::label('category_id', 'Category Name') !!}
{!! Form::select('category_id', $category, null, [
    'class' => 'form-select form-select-sm',
    'placeholder' => 'Select category name',
]) !!}
{!! Form::label('name', 'Name', ['class' => 'mt-4']) !!}
{!! Form::text('name', null, [
    'id' => 'name',
    'class' => 'form-control form-control-sm',
    'placeholder' => 'Enter sub category Name',
]) !!}
{!! Form::label('slug', 'Slug', ['class' => 'mt-4']) !!}
{!! Form::text('slug', null, [
    'id' => 'slug',
    'class' => 'form-control form-control-sm',
    'placeholder' => 'Enter sub category Slug',
]) !!}
{!! Form::label('status', 'Status', ['class' => 'mt-4']) !!}
{!! Form::select('status', [1 => 'Active', 2 => 'Inactive'], null, [
    'class' => 'form-select form-select-sm',
    'placeholder' => 'Enter sub category status',
]) !!}
{!! Form::label('serial', 'Serial', ['class' => 'mt-4']) !!}
{!! form::number('serial', null, [
    'class' => 'form-control form-control-sm',
    'placeholder' => 'Enter sub category Serial',
]) !!}
@push('script')
    <script>
        $('#name').on('input', function() {
            let value = $(this).val()
            value = value.replaceAll(' ', '-').toLowerCase()
            $('#slug').val(value)
        })
    </script>
@endpush
