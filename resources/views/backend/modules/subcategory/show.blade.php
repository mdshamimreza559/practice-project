@extends('backend.layout.master')
@section('title', 'Sub Category Show')
@section('contant')
    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4>@yield('title')</h4>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover table-bordered table-sm">
                            <tbody>
                                <tr>
                                    <th> ID</th>
                                    <td> {{ $subCategory->id }}</td>
                                </tr>
                                <tr>
                                    <th> Sub Category Name</th>
                                    <td> {{ $subCategory->name }}</td>
                                </tr>
                                <tr>
                                    <th> Category Name</th>
                                    <td> {{ $subCategory->category->name }}</td>
                                </tr>
                                <tr>
                                    <th> Slug </th>
                                    <td> {{ $subCategory->slug }}</td>
                                </tr>
                                <tr>
                                    @php
                                        $status = $subCategory->status;
                                    @endphp
                                    <th> Status </th>
                                    @if ($status == 1)
                                        <td class="text-success"> Active </td>
                                    @else
                                        <td class="text-danger"> Inactive </td>
                                    @endif

                                </tr>
                                <tr>
                                    <th> Serial </th>
                                    <td> {{ $subCategory->order_by }}</td>
                                </tr>
                                <tr>
                                    <th> Created At </th>
                                    <td> {{ $subCategory->created_at->toDayDateTimeString() }}</td>
                                </tr>
                                <tr>
                                    <th> Updated At </th>
                                    <td> {{ $subCategory->updated_at->toDayDateTimeString() }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <a href="{{ route('sub-categories.index') }}">
                            <button class="btn btn-success btn-sm">Back</button>
                        </a>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection
