@extends('backend.layout.master')
@section('title', 'Sub Category List')
@section('contant')
    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h4> @yield('title')</h4>
                        <a href="{{ route('sub-categories.create') }}">
                            <button class="btn btn-sm btn-success"><i class="fa-solid fa-plus    "></i></button>
                        </a>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover table-striped table-bordered table-sm" id="my_table">
                            <thead>
                                <tr>
                                    <th> SL </th>
                                    <th> Name </th>
                                    <th> Category Name </th>
                                    <th> Slug </th>
                                    <th> Status </th>
                                    <th> Serial </th>
                                    <th> Time </th>
                                    <th> Action </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($models as $model)
                                    @php
                                        $stats = $model->status;
                                        $create = $model->created_at;
                                        $update = $model->updated_at;
                                    @endphp
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $model->name }}</td>
                                        <td>{{ $model->category->name }}</td>
                                        <td class="text-danger">{{ $model->slug }}</td>
                                        <td>
                                            <div class="form-check form-switch">
                                                <input class="form-check-input" type="checkbox" name="subStatus"
                                                    value="{{ $model->id }}" <?php echo $stats == 1 ? 'checked' : ''; ?> role="switch"
                                                    id="flexSwitchCheckDefault">
                                            </div>
                                        </td>
                                        <td>{{ $model->order_by }}</td>
                                        <td>
                                            <p class="mb-0 text-success">
                                                <small> {{ $create->toDayDateTimeString() }} </small>
                                            </p>
                                            <p class="mb-0">
                                                <small>
                                                    {{ $create == $model->update ? 'Not updated yet' : $update->toDayDateTimeString() }}
                                                </small>
                                            </p>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center">
                                                <a href="{{ route('sub-categories.show', $model->id) }}">
                                                    <button class="btn btn-success btn-sm me-1"><i
                                                            class="fa-solid fa-eye    "></i></button>
                                                </a>
                                                <a href="{{ route('sub-categories.edit', $model->id) }}">
                                                    <button class="btn btn-warning btn-sm me-1"><i
                                                            class="fa-sloid fa-edit    "></i></button>
                                                </a>

                                                {!! Form::open([
                                                    'method' => 'delete',
                                                    'route' => ['sub-categories.destroy', $model->id],
                                                    'id' => 'sub_categories_delete_form_' . $model->id,
                                                ]) !!}
                                                {!! Form::button('<i class="fa-solid fa-trash    "></i>', [
                                                    'class' => 'btn btn-danger btn-sm sub-category-delete-btn',
                                                    'data-id' => $model->id,
                                                    'id' => 'sub_category_btn_' . $model->id,
                                                ]) !!}
                                                {!! Form::close() !!}
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(document).ready(function() {
            $('#my_table').DataTable();
        });

        $('input[name=subStatus]').on('change', function() {
            let mode = $(this).prop('checked') == true ? 1 : 2;
            let id = $(this).val()
            $.ajax({
                url: "{{ route('subCategory.status') }}",
                type: "POST",
                data: {
                    _token: '{{ csrf_token() }}',
                    status: mode,
                    value: id,
                },
                success: function(response) {
                    if (response.status) {
                        Swal.fire({
                            position: 'top-end',
                            toast: true,
                            icon: 'success',
                            title: response.msg,
                            showConfirmButton: false,
                            timer: 2000
                        })
                    }
                }
            })
        })

        $('.sub-category-delete-btn').on('click', function() {
            let id = $(this).attr('data-id')
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#sub_categories_delete_form_' + id).submit()
                }
            })
        })
    </script>
@endpush

@if (Session::has('msg'))
    @push('script')
        <script>
            Swal.fire({
                position: 'top-end',
                toast: true,
                icon: '<?php echo session('cls'); ?>',
                title: '<?php echo session('msg'); ?>',
                showConfirmButton: false,
                timer: 2000
            })
        </script>
    @endpush
@endif
