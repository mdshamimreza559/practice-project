@extends('backend.layout.master')
@section('title', 'Sub Category Create')
@section('contant')
    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4> @yield('title')</h4>
                    </div>
                    <div class="card-body">
                        {!! Form::model($subCategory, ['method' => 'put', 'route' => ['sub-categories.update', $subCategory->id]]) !!}
                        @include('backend.modules.subcategory.form')
                        {!! Form::button('Update SubCategory', ['class' => 'btn btn-success btn-sm mt-4', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
