@extends('backend.layout.master')
@section('title', 'Order Details')
@section('contant')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-hover table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Name </th>
                                <th>Email </th>
                                <th>Payment Method </th>
                                <th>Payment Status </th>
                                <th> Total</th>
                                <th> Status</th>
                                <th> Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>{{ $data->first_name . ' ' . $data->last_name }} </td>
                                <td>{{ $data->email }}</td>
                                <td>{{ $data->payment_method == 'cod' ? 'Cash On Delivery' : '' }}</td>
                                <td>{{ $data->payment_status }}</td>
                                <td><strong>${{ $data->total_amount }}</strong></td>
                                <td class="text-success">{{ $data->condition }}</td>
                                <td class="align-middle text-center">
                                    <div class="d-inline-flex ">
                                        <a href="">
                                            <button class="download-btn me-2">
                                                <i class="fas fa-download    "></i>
                                            </button>
                                        </a>

                                        {{ Form::open(['route' => ['order.destroy', $data->id], 'method' => 'delete', 'id' => 'order_delete_form_' . $data->id]) }}
                                        {!! Form::button('<i class="fas fa-trash    "></i>', [
                                            'class' => 'order-delete-btn',
                                            'data-id' => $data->id,
                                            'id' => 'order_delete_btn_' . $data->id,
                                        ]) !!}
                                        {{ Form::close() }}
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-striped table-hover table-bordered table-sm">
                        <thead>
                            <tr>
                                <th>Product Image</th>
                                <th> Quantity</th>
                                <th>Product </th>
                                <th>Price </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data->orderProducts as $order)
                                <tr>
                                    <td>
                                        <img style="width:100px"
                                            src="{{ asset('image/product/thumbnail/' . $order->product?->photo) }}"
                                            alt="{{ $order->product->title }}">
                                    </td>
                                    <td><strong>{{ $order->quantity }}</strong></td>
                                    <td><strong>{{ ucfirst($order->product->title) }}</strong></td>
                                    <td>${{ $order->total }}</td>

                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-6"></div>
        <div class="col-lg-6">
            <div class="card me-3">
                <div class="card-body">

                    <form action="{{ route('order.status', $data->id) }}" method="post">
                        @csrf
                        <p> <strong>Sub Total: </strong>${{ $data->sub_total }} </p>
                        <p> <strong>Sub Total: </strong>${{ $data->sub_total }} </p>
                        <p> <Strong>Total: </Strong>${{ $data->total_amount }}</p>
                        @if ($data->delivery_charge)
                            <p> <Strong>Delivery charge:</Strong> ${{ $data->delivery_charge }}</p>
                        @endif
                        <p> <Strong>Status: </Strong>
                            <input type="hidden" name="order_id" value="{{ $data->id }}">
                            <select name="condition" id="" class="form form-select form-select-sm">
                                <option value="pending"
                                    {{ $data->condition == 'delivered' || $data->condition == 'cancelled' ? 'disabled' : '' }}
                                    {{ $data->condition == 'pending' ? 'selected' : '' }}>pending
                                </option>
                                <option value="processing"
                                    {{ $data->condition == 'delivered' || $data->condition == 'cancelled' ? 'disabled' : '' }}
                                    {{ $data->condition == 'processing' ? 'selected' : '' }}>
                                    processing</option>
                                <option value="delivered" {{ $data->condition == 'cancelled' ? 'disabled' : '' }}
                                    {{ $data->condition == 'delivered' ? 'selected' : '' }}>delivered
                                </option>
                                <option value="cancelled" {{ $data->condition == 'delivered' ? 'disabled' : '' }}
                                    {{ $data->condition == 'cancelled' ? 'selected' : '' }}>cancelled
                                </option>
                            </select>
                            <button class="order-submit-btn mt-3">submit</button>
                        </p>
                    </form>
                </div>
            </div>

        </div>
    </div>
    @if (Session::has('msg'))
        @push('script')
            <script>
                Swal.fire({
                    position: 'top-end',
                    toast: true,
                    icon: '<?php echo session('cls'); ?>',
                    title: '<?php echo session('msg'); ?>',
                    showConfirmButton: false,
                    timer: 2000
                })
            </script>
        @endpush
    @endif
@endsection
@push('script')
    <script>
        $('.order-delete-btn').on('click', function() {
            let id = $(this).attr('data-id')

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#order_delete_form_' + id).submit()
                }
            })
        })
    </script>
@endpush
