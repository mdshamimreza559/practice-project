@extends('backend.layout.master')
@section('title', 'Product Show')
@section('contant')
    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4>@yield('title')</h4>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover table-bordered table-sm">
                            <tbody>
                                <tr>
                                    <th class="text-danger"> ID</th>
                                    <td> {{ $product->id }}</td>
                                </tr>
                                <tr>
                                    <th class="text-danger"> Product Name </th>
                                    <td> {{ $product->title }}</td>
                                </tr>
                                <tr>
                                    <th class="text-danger"> Slug </th>
                                    <td> {{ $product->slug }}</td>
                                </tr>
                                <tr>
                                    @php
                                        $status = $product->status;
                                    @endphp
                                    <th class="text-danger"> Status </th>
                                    @if ($status == 1)
                                        <td class="text-success"> Active </td>
                                    @else
                                        <td class="text-warning"> Inactive </td>
                                    @endif
                                </tr>
                                <tr>
                                    <th class="text-danger"> Short Description </th>
                                    <td> {!! $product->short_des !!}</td>
                                </tr>
                                <tr>
                                    <th class="text-danger"> Description </th>
                                    <td> {!! $product->description !!}</td>
                                </tr>
                                <tr>
                                    <th class="text-danger"> Stock </th>
                                    <td class="text-primary"> {!! $product->stock !!}</td>
                                </tr>
                                <tr>
                                    <th class="text-danger"> Price </th>
                                    <td class="text-primary"> {!! $product->price !!} TK</td>
                                </tr>
                                <tr>
                                    <th class="text-danger"> Offer Price </th>
                                    <td class="text-primary"> {!! $product->offer_price !!} TK</td>
                                </tr>
                                <tr>
                                    <th class="text-danger"> Discount </th>
                                    <td class="text-primary"> {!! $product->discount !!}</td>
                                </tr>
                                <tr>
                                    <th class="text-danger"> Size </th>
                                    <td class="text-primary"> {!! $product->size !!}</td>
                                </tr>

                                <tr>
                                    <th class="text-danger"> Brand </th>
                                    <td> {!! $product->brand->name !!}</td>
                                </tr>
                                <tr>
                                    <th class="text-danger"> Condition </th>
                                    <td> {!! $product->condition !!}</td>
                                </tr>
                                <tr>
                                    <th class="text-danger"> Category Name </th>
                                    <td> {!! $product->category->name !!}</td>
                                </tr>
                                <tr>
                                    <th class="text-danger"> Sub Category Name </th>
                                    <td> {!! $product->subCategory->name !!}</td>
                                </tr>
                                <tr>
                                    <th class="text-danger"> Created By </th>
                                    <td> {!! $product->user->f_name . ' ' . $product->user->l_name !!}</td>
                                </tr>
                                <tr>
                                    <th class="text-danger"> Created At </th>
                                    <td> {{ $product->created_at->toDayDateTimeString() }}</td>
                                </tr>
                                <tr>
                                    <th class="text-danger"> Updated At </th>
                                    <td> {{ $product->updated_at->toDayDateTimeString() }}</td>
                                </tr>
                                <tr>
                                    <th class="text-danger"> Photo </th>
                                    <td> <img class="product_img"
                                            src="{{ asset('image/product/thumbnail/' . $product->photo) }}" alt="">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <a href="{{ route('products.index') }}">
                            <button class="btn btn-success">Back</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
