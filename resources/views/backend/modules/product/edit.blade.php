@extends('backend.layout.master')
@section('title', 'Product update')
@section('contant')
    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h4> @yield('title')</h4>
                        <a href="{{ route('products.index') }}">
                            <button class="btn btn-sm btn-success">Back</button>
                        </a>
                    </div>
                    <div class="card-body">
                        {!! Form::model($product, ['method' => 'put', 'route' => ['products.update', $product->id], 'files' => true]) !!}
                        @include('backend.modules.product.form')

                        <div class="row my-3">
                            <div class="col-lg-6">
                                <div class="old-img mb-3">
                                    <h6>old image</h6>
                                    <hr class="my-2 text-danger">
                                    <img src="{{ asset('image/product/thumbnail/' . $product->photo) }}"
                                        alt="{{ $product->photo }}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="update-img" style="display: none">
                                    <h6>update image</h6>
                                    <hr class="my-2 text-danger">
                                    <img id="feather_img" alt="">
                                </div>
                            </div>
                        </div>
                        {!! Form::button('Update Product', ['class' => 'btn btn-success btn-sm mt-4', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $('#photo').on('change', function(e) {
            let photo = e.target.files[0]
            photo = URL.createObjectURL(photo)
            $('#feather_img').attr('src', photo)
            $('.update-img').show()
        })




        let sub_category_id = "<?php echo $product->sub_category_id; ?>"
        const getsubCategories = (id) => {
            axios.get(window.location.origin + '/get-sub-categories/' + id).then(res => {
                let subCategories = res.data.data
                subCategories.map(subCategory => {
                    $('#select_sub_category').append(
                        `<option ${subCategory.id == sub_category_id ? 'selected' : ''} value="${subCategory.id}">${subCategory.name}</option>`
                    )
                })
            })
        }
        let category_id = '<?php echo $product->category_id; ?>'
        getsubCategories(category_id)
        $('#category_select').on('change', function() {
            let id = $(this).val()
            $('#select_sub_category').empty()
            $('#select_sub_category').append('<option value="">Select Sub Category</option>')
            getsubCategories(id)
        })
    </script>
@endpush
