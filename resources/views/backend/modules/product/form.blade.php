{!! Form::label('title', 'Name') !!}
{!! Form::text('title', null, [
    'id' => 'name',
    'class' => 'form-control form-control-sm',
    'placeholder' => 'Enter product Name',
]) !!}
{!! Form::label('slug', 'Slug', ['class' => 'mt-4']) !!}
{!! Form::text('slug', null, [
    'id' => 'slug',
    'class' => 'form-control form-control-sm',
    'placeholder' => 'Enter product Slug',
]) !!}
{!! Form::label('status', 'Status', ['class' => 'mt-4']) !!}
{!! Form::select('status', [1 => 'Active', 2 => 'Inactive'], null, [
    'class' => 'form-select form-select-sm',
    'placeholder' => 'Enter product status',
]) !!}

{{-- category  Sub Catgeory --}}
<div class="row mt-4">
    <div class="col-lg-6">
        {!! Form::label('category_id', 'Category Name') !!}
        {!! form::select('category_id', $category, null, [
            'id' => 'category_select',
            'class' => 'form-select form-select-sm',
            'placeholder' => 'Select Category Name',
        ]) !!}
    </div>
    <div class="col-lg-6">
        {!! Form::label('sub_category_id', 'Sub Category Name') !!}
        <select name="sub_category_id" id="select_sub_category" class="form-select form-select-sm">
            <option value="">select sub category</option>
        </select>
    </div>
</div>

{{-- brand Condition Size --}}
<div class="row mt-4">
    <div class="col-lg-4">
        {!! Form::label('brand_id', 'Brand') !!}
        {!! form::select('brand_id', $brand, null, [
            'class' => 'form-select form-select-sm',
            'placeholder' => 'Select brand name',
        ]) !!}
    </div>
    <div class="col-lg-4">
        {!! Form::label('condition', 'Condition') !!}
        {!! Form::select('stock', [1 => 'New', 2 => 'Popular', 3 => 'winter'], null, [
            'class' => 'form-select form-select-sm',
            'placeholder' => 'Select product condition',
        ]) !!}
    </div>
    <div class="col-lg-4">
        {!! Form::label('size', 'Size') !!}
        {!! Form::select('size', [1 => 'S', 2 => 'L', 3 => 'M', 4 => 'XL'], null, [
            'class' => 'form-select form-select-sm',
            'placeholder' => 'Select product condition',
        ]) !!}

    </div>
</div>
{{-- price Discount Stock --}}
<div class="row mt-4">
    <div class="col-lg-4">
        {!! Form::label('price', 'Price') !!}
        {!! Form::number('price', null, [
            'class' => 'form-control form-control-sm',
            'placeholder' => 'Ensert product price',
        ]) !!}
    </div>
    <div class="col-lg-4">
        {!! Form::label('discount', 'Discount') !!}
        {!! Form::number('discount', null, [
            'class' => 'form-control form-control-sm',
            'placeholder' => 'Ensert discount %',
        ]) !!}
    </div>
    <div class="col-lg-4">
        {!! Form::label('stock', 'Stock') !!}
        {!! Form::number('stock', null, [
            'class' => 'form-control form-select-sm',
            'placeholder' => 'Product Stock...',
        ]) !!}
    </div>
</div>

{{-- Featured & Tranding Product  --}}
<div class="row mt-4">
    <div class="col-lg-6">
        {!! Form::label('is_featured', 'Featured Product') !!}
        {!! Form::select('is_featured', ['1' => 'Feature product'], null, [
            'class' => 'form-select form-control-sm',
            'placeholder' => 'Featured Product...',
        ]) !!}
    </div>
    <div class="col-lg-6">
        {!! Form::label('trendding_products', 'Trading Product') !!}
        {!! Form::select('trendding_products', [1 => 'Trading Product'], null, [
            'class' => 'form-select form-select-sm',
            'placeholder' => 'Trading Product...',
        ]) !!}
    </div>
</div>



{!! Form::label('short_des', 'Short Description', ['class' => 'mt-4']) !!}
{!! Form::textarea('short_des', null, [
    'id' => 'description',
    'class' => 'form-control form-control-sm',
    'placeholder' => 'write product short description',
]) !!}
{!! Form::label('description', ' Description', ['class' => 'mt-4']) !!}
{!! Form::textarea('description', null, [
    'id' => 'editor',
    'class' => 'form-control form-control-sm',
    'placeholder' => 'write product description',
]) !!}

{!! Form::label('photo', 'Photo', ['class' => 'mt-4']) !!}
{!! Form::file('photo', ['id' => 'photo', 'class' => 'form-control form-control-sm']) !!}

@push('script')
    <script>
        $('#name').on('input', function() {
            let value = $(this).val()
            value = value.replaceAll(' ', '-').toLowerCase()
            $('#slug').val(value)
        })

        ClassicEditor
            .create(document.querySelector('#description'))
            .catch(error => {
                console.error(error);
            });
        ClassicEditor
            .create(document.querySelector('#editor'))
            .catch(error => {
                console.error(error);
            });
    </script>
@endpush
