@extends('backend.layout.master')
@section('title', 'Add Product')
@section('contant')
    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4> @yield('title')</h4>
                    </div>
                    <div class="card-body">
                        {!! Form::open(['method' => 'store', 'route' => 'products.store', 'files' => true]) !!}
                        @include('backend.modules.product.form')
                        <div class="update-img my-2" style="display: none">
                            <img id="feather_img" alt="">
                        </div>
                        {!! Form::button('Add Product', ['class' => 'btn btn-success btn-sm mt-3', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $('#category_select').on('change', function() {
            let id = $(this).val()
            axios.get(window.location.origin + '/get-sub-categories/' + id).then(res => {
                let subCategories = res.data.data
                $('#select_sub_category').empty()
                $('#select_sub_category').append('<option value="">Select Sub Category</option>')
                subCategories.map(subCategory => {
                    $('#select_sub_category').append(
                        `<option value="${subCategory.id}">${subCategory.name}</option>`)
                })
            })
        })
        $('#photo').on('change', function(e) {
            let photo = e.target.files[0]
            photo = URL.createObjectURL(photo)
            $('#feather_img').attr('src', photo)
            $('.update-img').show()
        })
    </script>
@endpush
