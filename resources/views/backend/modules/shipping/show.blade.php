@extends('backend.layout.master')
@section('title', 'Category Show')
@section('contant')
    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4>@yield('title')</h4>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover table-bordered table-sm">
                            <tbody>
                                <tr>
                                    <th> ID</th>
                                    <td> {{ $shipping->id }}</td>
                                </tr>
                                <tr>
                                    <th> Shpping Name</th>
                                    <td> {{ $shipping->name }}</td>
                                </tr>
                                <tr>
                                    <th> Delivery Time </th>
                                    <td> {{ $shipping->time }}</td>
                                </tr>
                                <tr>
                                    @php
                                        $status = $shipping->status;
                                    @endphp
                                    <th> Status </th>
                                    @if ($status == 1)
                                        <td class="text-success"> Active </td>
                                    @else
                                        <td class="text-warning"> Inactive </td>
                                    @endif

                                </tr>
                                <tr>
                                    <th> Delivery Charge </th>
                                    <td> {{ $shipping->price }}</td>
                                </tr>
                                <tr>
                                    <th> Created At </th>
                                    <td> {{ $shipping->created_at->toDayDateTimeString() }}</td>
                                </tr>
                                <tr>
                                    <th> Updated At </th>
                                    <td> {{ $shipping->created_at == $shipping->updated_at ? 'Not updated yet' : $shipping->updated_at->toDayDateTimeString() }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <a href="{{ route('shippings.index') }}">
                            <button class="btn btn-success btn-sm">
                                Back
                            </button>
                        </a>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection
