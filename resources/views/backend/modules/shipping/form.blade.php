{!! Form::label('name', 'Name') !!}
{!! Form::text('name', null, [
    'id' => 'name',
    'class' => 'form-control form-control-sm',
    'placeholder' => 'Enter Category Name',
]) !!}
@error('name')
    <p class="text-danger position-absolute">
        <small>{{ $message }}</small>
    </p>
@enderror
{!! Form::label('status', 'Status', ['class' => 'mt-4']) !!}
{!! Form::select('status', [1 => 'Active', 2 => 'Inactive'], null, [
    'class' => 'form-select form-select-sm',
    'placeholder' => 'Enter Category status',
]) !!}
@error('status')
    <p class="text-danger position-absolute">
        <small>{{ $message }}</small>
    </p>
@enderror
{!! Form::label('time', 'Delivery Time', ['class' => 'mt-4']) !!}
{!! form::text('time', null, [
    'class' => 'form-control form-control-sm',
    'placeholder' => 'Enter Category Serial',
]) !!}
@error('time')
    <p class="text-danger position-absolute">
        <small>{{ $message }}</small>
    </p>
@enderror
{!! Form::label('price', 'Charge', ['class' => 'mt-4']) !!}
{!! Form::text('price', null, [
    'class' => 'form-control form-control-sm',
    'placeholder' => 'Enter delivery price',
]) !!}
@error('price')
    <p class="text-danger position-absolute">
        <small>{{ $message }}</small>
    </p>
@enderror
