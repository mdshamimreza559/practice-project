@extends('backend.layout.master')
@section('title', 'Shipping List')
@section('contant')
    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h4> @yield('title')</h4>
                        <a href="{{ route('shippings.create') }}">
                            <button class="btn btn-sm btn-success"><i class="fa-solid fa-plus    "></i></button>
                        </a>
                    </div>
                    <div class="card-body">
                        <table class="table table-hover table-striped table-bordered table-sm pt-2" id="my_table">
                            <thead>
                                <tr>
                                    <th> SL </th>
                                    <th> Method </th>
                                    <th> Delivery Time </th>
                                    <th> Status </th>
                                    <th> Delivery Charge </th>
                                    <th> Time </th>
                                    <th> Action </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($shhipings as $model)
                                    @php
                                        $stats = $model->status;
                                        $create = $model->created_at;
                                        $update = $model->updated_at;
                                    @endphp
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $model->name }}</td>
                                        <td><strong>{{ $model->time }} </strong></td>
                                        <td>
                                            <div class="form-check form-switch">
                                                <input class="form-check-input" value="{{ $model->id }}" type="checkbox"
                                                    name="status" <?php echo $stats == 1 ? 'checked' : ''; ?> role="switch"
                                                    id="flexSwitchCheckDefault">
                                            </div>
                                        </td>
                                        <td>{{ $model->price }}</td>
                                        <td>
                                            <p class="mb-0 text-success">
                                                <small> {{ $create->toDayDateTimeString() }} </small>
                                            </p>
                                            <p class="mb-0">
                                                <small>
                                                    {{ $create == $update ? 'Not updated yet' : $update->toDayDateTimeString() }}
                                                </small>
                                            </p>
                                        </td>
                                        <td>
                                            <div class="d-flex justify-content-center">
                                                <a href="{{ route('shippings.show', $model->id) }}">
                                                    <button class="btn btn-success btn-sm me-1"><i
                                                            class="fa-solid fa-eye    "></i></button>
                                                </a>
                                                <a href="{{ route('shippings.edit', $model->id) }}">
                                                    <button class="btn btn-warning btn-sm me-1"><i
                                                            class="fa-sloid fa-edit    "></i></button>
                                                </a>
                                                {!! Form::open([
                                                    'method' => 'delete',
                                                    'id' => 'shipping_delete_form_' . $model->id,
                                                    'route' => ['shippings.destroy', $model->id],
                                                ]) !!}
                                                {!! Form::button('<i class="fa-solid fa-trash"></i>', [
                                                    'class' => 'btn btn-danger btn-sm shipping-select-btn',
                                                    'data-id' => $model->id,
                                                    'id' => 'shipping_delete_btn_' . $model->id,
                                                ]) !!}
                                                {!! Form::close() !!}
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        $(document).ready(function() {
            $('#my_table').DataTable();
        });
        $('input[name=status]').on('change', function() {
            let mode = $(this).prop('checked') == true ? 1 : 2;
            let id = $(this).val()
            $.ajax({
                url: "{{ route('shipping.status') }}",
                type: "POST",
                data: {
                    _token: '{{ csrf_token() }}',
                    mode: mode,
                    value: id,
                },
                success: function(response) {
                    if (response.status == 1) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            toast: true,
                            title: 'Category Status active Successfully',
                            showConfirmButton: false,
                            timer: 2000
                        })
                    } else {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            toast: true,
                            title: 'Category Status inactive Successfully',
                            showConfirmButton: false,
                            timer: 2000
                        })
                    }
                }
            })
        })
        $('.shipping-select-btn').on('click', function() {
            let id = $(this).attr('data-id')
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#shipping_delete_form_' + id).submit()
                }
            })
        })
    </script>
@endpush

@if (Session::has('msg'))
    @push('script')
        <script>
            Swal.fire({
                position: 'top-end',
                toast: true,
                icon: '<?php echo session('cls'); ?>',
                title: '<?php echo session('msg'); ?>',
                showConfirmButton: false,
                timer: 2000
            })
        </script>
    @endpush
@endif
