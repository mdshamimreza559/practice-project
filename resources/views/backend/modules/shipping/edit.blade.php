@extends('backend.layout.master')
@section('title', 'Shipping Update')
@section('contant')
    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4> @yield('title')</h4>
                    </div>
                    <div class="card-body">
                        {!! Form::model($shipping, ['method' => 'put', 'route' => ['shippings.update', $shipping->id]]) !!}
                        @include('backend.modules.shipping.form')
                        {!! Form::button('Update shipping', ['class' => 'btn btn-success btn-sm mt-4', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
