@extends('backend.layout.master')
@section('title', 'About us')
@section('contant')
    <div class="row justify-content-center">
        <div class="col-lg-7">
            <div class="card">
                <div class="card-header">
                    @yield('title')
                </div>
                <div class="card-body">
                    {!! Form::model($about, ['route' => 'aboutus.store', 'method' => 'post', 'files' => 'true']) !!}
                    {!! Form::label('title', 'Title') !!}
                    {!! Form::text('title', null, [
                        'class' => 'form-control form-control-sm',
                        'placeholder' => 'About us title...',
                    ]) !!}
                    {!! Form::label('content', 'Content', ['class' => 'mt-4']) !!}
                    {!! Form::textarea('content', null, [
                        'class' => 'form-control form-control-sm',
                        'placeholder' => 'About us content...',
                    ]) !!}

                    {!! Form::label('photo', 'Photo', ['class' => 'mt-4']) !!}
                    {!! Form::file('photo', ['class' => 'about-photo form-control form-control-sm']) !!}
                    <div class="row mt-2">
                        <div class="col-lg-6">
                            @if (!empty($about->photo))
                                <img src="{{ asset('image/about/' . $about->photo) }}" alt="">
                            @endif
                        </div>
                        <div class="col-lg-6">
                            <div id="about_photo" style="display: none">
                                <img id="show_about_photo" alt="">
                            </div>

                        </div>
                    </div>

                    {!! Form::label('happy_customer', 'HAPPY CUSTOMERS', ['class' => 'mt-4']) !!}
                    {!! Form::number('happy_customer', null, [
                        'class' => 'form-control form-control-sm',
                        'placeholder' => 'happy customer...',
                    ]) !!}

                    {!! Form::label('awards_winned', 'AWARDS WINNED', ['class' => 'mt-4']) !!}
                    {!! Form::number('awards_winned', null, [
                        'class' => 'form-control form-control-sm',
                        'placeholder' => 'awards winned...',
                    ]) !!}

                    {!! Form::label('hours_worked', 'HOURS WORKED', ['class' => 'mt-4']) !!}
                    {!! Form::number('hours_worked', null, [
                        'class' => 'form-control form-control-sm',
                        'placeholder' => 'hours worked...',
                    ]) !!}

                    {!! Form::label('complete_project', 'COMPLETE PROJECTS', ['class' => 'mt-4']) !!}
                    {!! Form::number('complete_project', null, [
                        'class' => 'form-control form-control-sm',
                        'placeholder' => 'complete project...',
                    ]) !!}

                    {!! Form::button('submit', ['class' => 'btn btn-success btn-sm mt-3', 'type' => 'submit']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $('.about-photo').on('change', function(e) {
            let files = e.target.files[0]
            files = URL.createObjectURL(files)
            $('#show_about_photo').attr('src', files)
            $('#about_photo').show()
        })
    </script>
@endpush
