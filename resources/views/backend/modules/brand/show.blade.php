@extends('backend.layout.master')
@section('title', 'Brand Show')
@section('contant')
    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4>@yield('title')</h4>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover table-bordered table-sm">
                            <tbody>
                                <tr>
                                    <th> ID</th>
                                    <td> {{ $brand->id }}</td>
                                </tr>
                                <tr>
                                    <th> brand Name</th>
                                    <td> {{ $brand->name }}</td>
                                </tr>
                                <tr>
                                    <th> Slug </th>
                                    <td> {{ $brand->slug }}</td>
                                </tr>
                                <tr>
                                    @php
                                        $status = $brand->status;
                                    @endphp
                                    <th> Status </th>
                                    @if ($status == 1)
                                        <td class="text-success"> Active </td>
                                    @else
                                        <td class="text-warning"> Inactive </td>
                                    @endif

                                </tr>
                                <tr>
                                    <th> Serial </th>
                                    <td> {{ $brand->order_by }}</td>
                                </tr>
                                </tr>
                                <tr>
                                    <th> Created At </th>
                                    <td> {{ $brand->created_at->toDayDateTimeString() }}</td>
                                </tr>
                                <tr>
                                    <th> Updated At </th>
                                    <td> {{ $brand->updated_at->toDayDateTimeString() }}</td>
                                </tr>

                                <tr>
                                    <th> Photo </th>
                                    <td>
                                        <img src="{{ asset('image/brand/' . $brand->photo) }}" alt="">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <a href="{{ route('brand.index') }}">
                            <button class="btn btn-success btn-sm"> Back</button>
                        </a>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection
