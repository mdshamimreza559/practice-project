{!! Form::label('name', 'Name') !!}
{!! Form::text('name', null, [
    'id' => 'name',
    'class' => 'form-control form-control-sm',
    'placeholder' => 'Enter brand Name',
]) !!}
@error('name')
    <p class="mb-0 text-danger position-absolute"><small>{{ $message }}</small></p>
@enderror
{!! Form::label('slug', 'Slug', ['class' => 'mt-4']) !!}
{!! Form::text('slug', null, [
    'id' => 'slug',
    'class' => 'form-control form-control-sm',
    'placeholder' => 'Enter brand Slug',
]) !!}
@error('slug')
    <p class="mb-0 text-danger position-absolute"><small>{{ $message }}</small></p>
@enderror
{!! Form::label('status', 'Status', ['class' => 'mt-4']) !!}
{!! Form::select('status', [1 => 'Active', 2 => 'Inactive'], null, [
    'class' => 'form-select form-select-sm',
    'placeholder' => 'Enter brand status',
]) !!}
@error('status')
    <p class="mb-0 text-danger position-absolute"><small>{{ $message }}</small></p>
@enderror
{!! Form::label('order_by', 'Serial', ['class' => 'mt-4']) !!}
{!! form::number('order_by', null, [
    'class' => 'form-control form-control-sm',
    'placeholder' => 'Enter brand Serial',
]) !!}
@error('order_by')
    <p class="mb-0 text-danger position-absolute"><small>{{ $message }}</small></p>
@enderror
{!! Form::label('photo', 'Photo', ['class' => 'mt-4']) !!}
{!! Form::file('photo', ['id' => 'photo', 'class' => 'form-control form-control-sm']) !!}



@push('script')
    <script>
        $('#photo').on('change', function(e) {
            let file = e.target.files[0];
            file = URL.createObjectURL(file)
            $('#brand_photo').attr('src', file)
            $('.brand-photo').show()
        })

        $('#name').on('input', function() {
            let value = $(this).val()
            value = value.replaceAll(' ', '-').toLowerCase()
            $('#slug').val(value)
        })
    </script>
@endpush
