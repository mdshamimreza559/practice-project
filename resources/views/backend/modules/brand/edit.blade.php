@extends('backend.layout.master')
@section('title', 'Update Brand')
@section('contant')
    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4> @yield('title')</h4>
                    </div>
                    <div class="card-body">
                        {!! Form::model($brand, ['method' => 'put', 'route' => ['brand.update', $brand->id], 'files' => true]) !!}
                        @include('backend.modules.brand.form')
                        <div class="row mt-3">
                            <div class="col-md-6">
                                <div class="old-img">
                                    <h6 class="text-danger">Old Image</h6>
                                    <hr class="my-2">
                                    <img src="{{ asset('image/brand/' . $brand->photo) }}" alt="">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="brand-photo " style="display: none">
                                    <h6 class="text-success">Updated Image</h6>
                                    <hr class="my-2">
                                    <img id="brand_photo" alt="">
                                </div>
                            </div>
                        </div>
                        {!! Form::button('Update brand', ['class' => 'btn btn-success btn-sm mt-4', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
