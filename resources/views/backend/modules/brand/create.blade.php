@extends('backend.layout.master')
@section('title', 'Add Brand')
@section('contant')
    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4> @yield('title')</h4>
                    </div>
                    <div class="card-body">
                        {!! Form::open(['method' => 'store', 'route' => 'brand.store', 'files' => true]) !!}
                        @include('backend.modules.brand.form')
                        <div class="brand-photo mt-2" style="display: none">
                            <img id="brand_photo" alt="">
                        </div>
                        {!! Form::button('Add Brand', ['class' => 'btn btn-success btn-sm mt-4', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
