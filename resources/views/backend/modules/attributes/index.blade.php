@extends('backend.layout.master')
@section('title', 'Product Attribute List')

@section('contant')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4>@yield('title')</h4>
                </div>
                <div class="card-body">
                    <table class="table  table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>S.N</th>
                                <th>Product Name</th>
                                <th>Size</th>
                                <th>Price</th>
                                <th>Offer Price</th>
                                <th> Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($attr as $model)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $model->product->title }}</td>
                                    <td>{{ $model->size }}</td>
                                    <td>{{ $model->price }}</td>
                                    <td>{{ $model->offer_price }}</td>
                                    <td>{{ $model->size }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
