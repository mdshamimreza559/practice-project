@extends('backend.layout.master')

@section('title', 'Add Product Attribute')

@section('contant')
    <div class="row my-4 ">
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h5>@yield('title')</h5>
                    <strong>{{ ucfirst($products->title) }}</strong>
                </div>
                <div class="card-body">
                    <form action="{{ route('attribute.store', $products->id) }}" method="post">
                        @csrf
                        <div id="product_attr" class="content"
                            data-mfield-options='{"section": ".group","btnAdd":"#btnAdd-1","btnRemove":".btnRemove"}'>
                            <div class="row">
                                <div class="col-md-12"><button type="button" id="btnAdd-1"
                                        class="btn btn-primary btn-sm ">
                                        <i class="fa-solid fa-plus    "></i>
                                    </button></div>
                            </div>

                            <div class="row group mt-3">

                                <div class="col-md-2">
                                    <label for="">Size</label>
                                    <input name="size[]" class="form-control form-control-sm" type="text">
                                </div>
                                <div class="col-md-3">
                                    <label for="">Price</label>
                                    <input name="price[]" class="form-control form-control-sm" type="number">
                                </div>
                                <div class="col-md-3">
                                    <label for="">Offer Price</label>
                                    <input name="offer_price[]" class="form-control form-control-sm" type="number">
                                </div>
                                <div class="col-md-2">
                                    <label for="">Stock</label>
                                    <input name="stock[]" class="form-control form-control-sm" type="number">
                                </div>
                                <div class="col-md-2 ">
                                    <button type="button" class="btn btn-danger btnRemove rmv_btn">
                                        <i class="fa-solid fa-traffic-light-slow    "></i>
                                    </button>
                                </div>
                            </div>

                        </div>
                        <button class="btn btn-success btn-sm mt-4" type="submit">submit</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <table class="table table-striped table-sm table-hover table-bordered">
                <thead>
                    <tr>
                        <th>S.N</th>
                        <th>Size</th>
                        <th>Price</th>
                        <th>Offer price</th>
                        <th>stock</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($product_attr as $attr)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $attr->size }}</td>
                            <td>{{ $attr->price }}</td>
                            <td>{{ $attr->offer_price }}</td>
                            <td>{{ $attr->stock }}</td>
                            <td>
                                {!! Form::open([
                                    'route' => ['attribute.delete', $attr->id],
                                    'method' => 'delete',
                                    'id' => 'attr_delete_form_' . $attr->id,
                                ]) !!}
                                {!! Form::button(' <i class="fa-solid fa-trash  "></i>', [
                                    'class' => 'attr_dlt_btn',
                                    'id' => 'attr_dlt_btn_' . $attr->id,
                                    'data-id' => $attr->id,
                                ]) !!}
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @endforeach


                </tbody>
            </table>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $('.attr_dlt_btn').on('click', function() {
            let val = $(this).attr('data-id')

            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#attr_delete_form_' + val).submit()
                }

            })
        })

        $('#product_attr').multifield();
    </script>
@endpush
