@extends('backend.layout.master');
@section('title', 'Setting')
@section('contant')
    <div class="row justify-content-center">
        <div class="col-lg-8">
            <div class="card mt-4">
                <div class="card-header">
                    <h4> @yield('title')</h4>
                </div>
                <div class="card-body">
                    {!! Form::model($setting, ['route' => 'setting.store', 'method' => 'post', 'files' => 'true']) !!}
                    <div class="row">
                        <div class="col-lg-6">
                            {!! Form::label('title', 'Title') !!}
                            {!! Form::text('title', null, ['class' => 'form-control form-control-sm', 'placeholder' => 'Insert Title']) !!}
                        </div>
                        <div class="col-lg-6">
                            {!! Form::label('meta_keyword', 'Meta Keyword') !!}
                            {!! Form::text('meta_keyword', null, [
                                'class' => 'form-control form-control-sm',
                                'placeholder' => 'Enter meta keyword',
                            ]) !!}
                        </div>
                    </div>
                    {!! Form::label('meta_description', 'Meta Description', ['class' => 'mt-4']) !!}
                    {!! Form::textarea('meta_description', null, [
                        'class' => 'form-control form-control-sm',
                        'placeholder' => 'Enter meta description',
                    ]) !!}

                    <div class="row mt-4">
                        <div class="col-lg-6">
                            {!! Form::label('phone', 'Phone') !!}
                            {!! Form::number('phone', null, [
                                'class' => 'form-control form-control-sm',
                                'placeholder' => 'Enter phone number',
                            ]) !!}
                        </div>
                        <div class="col-lg-6">
                            {!! Form::label('email', 'Email') !!}
                            {!! Form::email('email', null, [
                                'class' => 'form-control form-control-sm',
                                'placeholder' => 'Enter phone email',
                            ]) !!}
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-lg-6">
                            {!! Form::label('facebook_url', 'Facebook Url') !!}
                            {!! Form::text('facebook_url', null, [
                                'class' => 'form-control form-control-sm',
                                'placeholder' => 'insert facebook url',
                            ]) !!}
                        </div>
                        <div class="col-lg-6">
                            {!! Form::label('twitter_url', 'Twitter Url') !!}
                            {!! Form::text('twitter_url', null, [
                                'class' => 'form-control form-control-sm',
                                'placeholder' => 'insert twitter url',
                            ]) !!}
                        </div>
                        <div class="col-lg-6 mt-4">
                            {!! Form::label('linkdin_url', 'Linkdin Url') !!}
                            {!! Form::text('linkdin_url', null, [
                                'class' => 'form-control form-control-sm',
                                'placeholder' => 'insert linkdin url',
                            ]) !!}
                        </div>
                        <div class="col-lg-6 mt-4">
                            {!! Form::label('youtube_url', 'Youtube Url') !!}
                            {!! Form::text('youtube_url', null, [
                                'class' => 'form-control form-control-sm',
                                'placeholder' => 'insert youtube url',
                            ]) !!}
                        </div>
                        <div class="col-lg-6 mt-4">
                            {!! Form::label('instagram_url', 'Instagram Url') !!}
                            {!! Form::text('instagram_url', null, [
                                'class' => 'form-control form-control-sm',
                                'placeholder' => 'insert instagram url',
                            ]) !!}
                        </div>
                        <div class="col-lg-6 mt-4">
                            {!! Form::label('pinterest_url', 'pinterest Url') !!}
                            {!! Form::text('pinterest_url', null, [
                                'class' => 'form-control form-control-sm',
                                'placeholder' => 'insert pinterest url',
                            ]) !!}
                        </div>
                    </div>
                    <div class="row mt-4">
                        <div class="col-lg-6">
                            {!! Form::label('logo', 'Logo') !!}
                            {!! Form::file('logo', ['class' => 'form-control form-control-sm', 'id' => 'logo']) !!}
                            <div class="f_img d-flex">
                                @if (!empty($setting->logo))
                                    <div class="exit_img me-2 mt-2">
                                        <img src="{{ asset('image/settings/logo/' . $setting->logo) }}" alt="">
                                    </div>
                                @endif


                                <div class="show-logo display_img mt-2" style="display: none">
                                    <img id="show_logo" alt="">
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-6">
                            {!! Form::label('favicone', 'Favicone') !!}
                            {!! Form::file('favicone', ['class' => 'form-control form-control-sm', 'id' => 'favicone']) !!}
                            <div class="f_img d-flex">
                                @if (!empty($setting->favicone))
                                    <div class="exit_img me-2 mt-2">
                                        <img src="{{ asset('image/settings/favicone/' . $setting->favicone) }}"
                                            alt="">
                                    </div>
                                @endif


                                <div class="favicone  mt-2" style="display: none">
                                    <img id="fav_icone" alt="">
                                </div>
                            </div>



                        </div>
                    </div>

                    {!! Form::button('<i class="fas fa-edit    "></i> submit', [
                        'class' => 'btn btn-success btn-sm mt-4 mb-2',
                        'type' => 'submit',
                    ]) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>

    </div>
@endsection

@push('script')
    <script>
        $('#logo').on('change', function(e) {
            let logo = e.target.files[0]
            logo = URL.createObjectURL(logo)
            $('#show_logo').attr('src', logo)
            $('.show-logo').show()
        })
        $('#favicone').on('change', function(e) {
            let favicone = e.target.files[0]
            favicone = URL.createObjectURL(favicone)
            $('#fav_icone').attr('src', favicone)
            $('.favicone').show()
        })
    </script>
@endpush
