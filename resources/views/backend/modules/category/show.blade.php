@extends('backend.layout.master')
@section('title', 'Category Show')
@section('contant')
    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4>@yield('title')</h4>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover table-bordered table-sm">
                            <tbody>
                                <tr>
                                    <th> ID</th>
                                    <td> {{ $category->id }}</td>
                                </tr>
                                <tr>
                                    <th> Category Name</th>
                                    <td> {{ $category->name }}</td>
                                </tr>
                                <tr>
                                    <th> Slug </th>
                                    <td> {{ $category->slug }}</td>
                                </tr>
                                <tr>
                                    @php
                                        $status = $category->status;
                                    @endphp
                                    <th> Status </th>
                                    @if ($status == 1)
                                        <td class="text-success"> Active </td>
                                    @else
                                        <td class="text-warning"> Inactive </td>
                                    @endif

                                </tr>
                                <tr>
                                    <th> Serial </th>
                                    <td> {{ $category->order_by }}</td>
                                </tr>
                                <tr>
                                    <th> Created At </th>
                                    <td> {{ $category->created_at->toDayDateTimeString() }}</td>
                                </tr>
                                <tr>
                                    <th> Updated At </th>
                                    <td> {{ $category->updated_at->toDayDateTimeString() }}</td>
                                </tr>
                            </tbody>
                        </table>
                        <a href="{{ route('categories.index') }}">
                            <button class="btn btn-sm btn-success">Back</button>
                        </a>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection
