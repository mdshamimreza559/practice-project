{!! Form::label('name', 'Name') !!}
{!! Form::text('name', null, [
    'id' => 'name',
    'class' => 'form-control form-control-sm',
    'placeholder' => 'Enter Category Name',
]) !!}
{!! Form::label('slug', 'Slug', ['class' => 'mt-4']) !!}
{!! Form::text('slug', null, [
    'id' => 'slug',
    'class' => 'form-control form-control-sm',
    'placeholder' => 'Enter Category Slug',
]) !!}
{!! Form::label('status', 'Status', ['class' => 'mt-4']) !!}
{!! Form::select('status', [1 => 'Active', 2 => 'Inactive'], null, [
    'class' => 'form-select form-select-sm',
    'placeholder' => 'Enter Category status',
]) !!}
{!! Form::label('order_by', 'Serial', ['class' => 'mt-4']) !!}
{!! form::number('order_by', null, [
    'class' => 'form-control form-control-sm',
    'placeholder' => 'Enter Category Serial',
]) !!}
@push('script')
    <script>
        $('#name').on('input', function() {
            let value = $(this).val()
            value = value.replaceAll(' ', '-').toLowerCase()
            $('#slug').val(value)
        })
    </script>
@endpush
