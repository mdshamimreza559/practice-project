@extends('backend.layout.master')
@section('title', 'Category Create')
@section('contant')
    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4> @yield('title')</h4>
                    </div>
                    <div class="card-body">
                        {!! Form::model($category, ['method' => 'put', 'route' => ['categories.update', $category->id]]) !!}
                        @include('backend.modules.category.form')
                        {!! Form::button('Update Category', ['class' => 'btn btn-success btn-sm mt-4', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
