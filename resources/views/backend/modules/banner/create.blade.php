@extends('backend.layout.master')

@section('title', 'Banner Create')

@section('contant')
    <div class="container ">
        <div class="row justify-content-center py-5">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header d-flex justify-content-between">
                        <h4> @yield('title')</h4>
                        <a href="{{ route('banners.index') }}">
                            <button class="btn btn-sm btn-success">Back</button>
                        </a>
                    </div>
                    <div class="card-body">
                        {!! Form::open(['route' => 'banners.store', 'method' => 'post', 'files' => true]) !!}
                        @include('backend.modules.banner.form')
                        <div class="show-banner mt-2" style="display: none">
                            <img id="banner_img" alt="">
                        </div>
                        {!! Form::button('Create Banner', ['class' => 'btn btn-sm btn-success mt-4', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
