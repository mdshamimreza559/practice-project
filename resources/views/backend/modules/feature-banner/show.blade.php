@extends('backend.layout.master')

@section('title', 'Slider Show')

@section('contant')
    <div class="container">
        <div class="row justify-content-center py-5">
            <div class="col-lg-7">
                <div class="card">
                    <div class="card-header">
                        <h4>@yield('title')</h4>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-bordered table-hover table-sm">
                            <tbody>
                                @php
                                    $status = $fetureBanne->status;
                                    $create = $fetureBanne->created_at;
                                    $update = $fetureBanne->updated_at;
                                @endphp
                                <tr>
                                    <th>ID </th>
                                    <td>{{ $fetureBanne->id }}</td>
                                </tr>
                                <tr>
                                    <th> Title </th>
                                    <td>{{ $fetureBanne->name }}</td>
                                </tr>
                                <tr>
                                    <th> status </th>
                                    @if ($status == 1)
                                        <td class="text-success">Active</td>
                                    @else
                                        <td class="text-danger">Inactive</td>
                                    @endif
                                </tr>
                                {{-- <tr>
                                    <th> Created At </th>
                                    <td>{{ $create->toDayDateTimeString() }}</td>
                                </tr>
                                <tr>
                                    <th> Created At </th>
                                    @if ($create == $update)
                                        <td class="text-danger">Not updated yet</td>
                                    @else
                                        <td class="text-success">{{ $update->toDayDateTimeString() }}</td>
                                    @endif
                                </tr> --}}
                                <tr>
                                    <th>Photo</th>
                                    <td><img width="190px"
                                            src="{{ asset('image/banner/feature-banner/' . $fetureBanne->photo) }}"
                                            alt="{{ $fetureBanne->name }}">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <a href="{{ route('feature-banner.index') }}">
                            <button class="btn btn-success btn-sm">Back</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
