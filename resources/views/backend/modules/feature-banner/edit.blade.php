@extends('backend.layout.master')

@section('title', 'Update Feture Banner')

@section('contant')
    <div class="conatiner">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4> @yield('title')</h4>
                    </div>
                    <div class="card-body">
                        {!! Form::model($fetureBanne, [
                            'route' => ['feature-banner.update', $fetureBanne->id],
                            'method' => 'put',
                            'files' => true,
                        ]) !!}
                        @include('backend.modules.feature-banner.form')
                        <div class="row py-3 ">
                            <div class="col-lg-6">
                                <div class="old-img">
                                    <h6>Old Image</h6>
                                    <hr class="my-2 text-danger">
                                    <img src="{{ asset('image/banner/feature-banner/' . $fetureBanne->photo) }}"
                                        alt="">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="show-banner" style="display: none">
                                    <h6>new photo</h6>
                                    <hr class="my-2 me-5 text-danger">
                                    <img id="banner_img" alt="">
                                </div>
                            </div>
                        </div>
                        {!! Form::button('Update banner', ['class' => 'mt-4 mb-2 btn btn-warning btn-sm', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
