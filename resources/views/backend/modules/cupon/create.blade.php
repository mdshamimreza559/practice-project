@extends('backend.layout.master')
@section('title', 'Cupon Create')
@section('contant')
    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4> @yield('title')</h4>
                    </div>
                    <div class="card-body">
                        {!! Form::open(['method' => 'store', 'route' => 'cupon.store']) !!}
                        @include('backend.modules.cupon.form')
                        {!! Form::button('Create Cupon', ['class' => 'btn btn-success btn-sm mt-4', 'type' => 'submit']) !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
