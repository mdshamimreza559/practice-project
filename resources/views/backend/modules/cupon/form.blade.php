{!! Form::label('code', 'Code') !!}
{!! Form::text('code', null, [
    'id' => 'name',
    'class' => 'form-control form-control-sm',
    'placeholder' => 'eg. 714521',
]) !!}
@error('code')
    <p class="mb-0 text-danger position-absolute">
        <span>{{ $message }}</span>
    </p>
@enderror
{!! Form::label('status', 'Status', ['class' => 'mt-4']) !!}
{!! Form::select('status', [1 => 'Active', 2 => 'Inactive'], null, [
    'class' => 'form-select form-select-sm',
    'placeholder' => 'Enter Cupon status',
]) !!}
@error('status')
    <p class="mb-0 text-danger position-absolute">
        <span>{{ $message }}</span>
    </p>
@enderror
{!! Form::label('type', 'Type', ['class' => 'mt-4']) !!}
{!! Form::select('type', [1 => 'Fixed', 2 => 'Percent'], null, [
    'class' => 'form-select form-select-sm',
    'placeholder' => 'Enter Cupon type',
]) !!}
@error('type')
    <p class="mb-0 text-danger position-absolute">
        <span>{{ $message }}</span>
    </p>
@enderror
{!! Form::label('value', 'value', ['class' => 'mt-4']) !!}
{!! Form::text('value', null, [
    'class' => 'form-select form-select-sm',
    'placeholder' => 'eg. HAPPY',
]) !!}
@error('type')
    <p class="mb-0 text-danger position-absolute">
        <span>{{ $message }}</span>
    </p>
@enderror
