@extends('backend.layout.master')
@section('title', 'Coupob Show')
@section('contant')
    <div class="container py-5">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <h4>@yield('title')</h4>
                    </div>
                    <div class="card-body">
                        <table class="table table-striped table-hover table-bordered table-sm">
                            <tbody>
                                <tr>
                                    <th> ID</th>
                                    <td> {{ $coupon->id }}</td>
                                </tr>
                                <tr>
                                    <th> Code</th>
                                    <td> {{ $coupon->code }}</td>
                                </tr>
                                <tr>
                                    <th> Type </th>
                                    @if ($coupon->type == 1)
                                        <td class="text-success"> Fixed</td>
                                    @else
                                        <td class="text-info"> Percentage</td>
                                    @endif

                                </tr>
                                <tr>
                                    @php
                                        $status = $coupon->status;
                                    @endphp
                                    <th> Status </th>
                                    @if ($status == 1)
                                        <td class="text-success"> Active </td>
                                    @else
                                        <td class="text-warning"> Inactive </td>
                                    @endif

                                </tr>
                                <tr>
                                    <th> Vlaue </th>
                                    <td> {{ $coupon->value }}</td>
                                </tr>
                                <tr>
                                    <th> Created At </th>
                                    <td class="text-info"> {{ $coupon->created_at->toDayDateTimeString() }}</td>
                                </tr>
                                <tr>
                                    <th> Updated At </th>
                                    <td class="text-danger">
                                        {{ $coupon->created_at == $coupon->updated_at ? 'Not updated yet' : $coupon->updated_at->toDayDateTimeString() }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <a href="{{ route('cupon.index') }}">
                            <button class="btn btn-success">Back</button>
                        </a>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection
