<nav>
    <ul class="menu-aside">
        <li class="menu-item active">
            <a class="menu-link" href="{{ route('dashboard.index') }}">
                <i class="icon material-icons md-home"></i>
                <span class="text">Dashboard</span>
            </a>
        </li>


        {{-- Banner Menu --}}
        <li class="menu-item has-submenu">
            <a class="menu-link" href="page-products-list.html">
                <i class="icon material-icons md-shopping_bag"></i>
                <span class="text">Banner</span>
            </a>
            <div class="submenu">
                <a href="{{ route('banners.create') }}">Banner Create</a>
                <a href="{{ route('banners.index') }}">Banner List</a>
                <hr class="mb-2">
                <a href="{{ route('list-banner.create') }}">List Banner create </a>
                <a href="{{ route('list-banner.index') }}">List Banner Manage</a>
                <hr class="mb-2">
                <a href="{{ route('single-banner.create') }}">single Banner create</a>
                <a href="{{ route('single-banner.index') }}">single Banner List</a>
                <hr class="mb-2">
                <a href="{{ route('feature-banner.create') }}">Feature Banner create</a>
                <a href="{{ route('feature-banner.index') }}">Feature Banner List</a>
            </div>
        </li>
        {{-- category Menu --}}
        <li class="menu-item has-submenu">
            <a class="menu-link" href="page-products-list.html">
                <i class="icon material-icons md-shopping_bag"></i>
                <span class="text">Category</span>
            </a>
            <div class="submenu">
                <a href="{{ route('categories.create') }}">Category Create</a>
                <a href="{{ route('categories.index') }}">Category List</a>
            </div>
        </li>
        {{-- Sub Category Menu --}}
        <li class="menu-item has-submenu">
            <a class="menu-link" href="page-products-list.html">
                <i class="icon material-icons md-shopping_bag"></i>
                <span class="text">Sub Category</span>
            </a>
            <div class="submenu">
                <a href="{{ route('sub-categories.create') }}"> Create Sub Category</a>
                <a href="{{ route('sub-categories.index') }}">Sub Category List</a>
            </div>
        </li>
        {{-- Brand Menu --}}
        <li class="menu-item has-submenu">
            <a class="menu-link" href="page-products-list.html">
                <i class="icon material-icons md-shopping_bag"></i>
                <span class="text">Brand</span>
            </a>
            <div class="submenu">
                <a href="{{ route('brand.create') }}"> Add Brand</a>
                <a href="{{ route('brand.index') }}">Brand List</a>
            </div>
        </li>
        {{-- Products Menu --}}
        <li class="menu-item has-submenu">
            <a class="menu-link" href="page-products-list.html">
                <i class="icon material-icons md-shopping_bag"></i>
                <span class="text">Products</span>
            </a>
            <div class="submenu">
                <a href="{{ route('products.create') }}"> Add Product</a>
                <a href="{{ route('products.index') }}">Products List</a>
            </div>
        </li>

        {{-- Cupons Menu --}}
        <li class="menu-item has-submenu">
            <a class="menu-link" href="page-products-list.html">
                <i class="icon material-icons md-shopping_bag"></i>
                <span class="text">Cupon</span>
            </a>
            <div class="submenu">
                <a href="{{ route('cupon.create') }}"> Create Cupon</a>
                <a href="{{ route('cupon.index') }}">Cupon List</a>
            </div>
        </li>

        {{-- Shipping Menu --}}
        <li class="menu-item has-submenu">
            <a class="menu-link" href="page-products-list.html">
                <i class="icon material-icons md-shopping_bag"></i>
                <span class="text">Shiping</span>
            </a>
            <div class="submenu">
                <a href="{{ route('shippings.create') }}"> Add Shipping </a>
                <a href="{{ route('shippings.index') }}">Shipping List</a>
            </div>
        </li>

        {{-- user --}}
        <li class="menu-item has-submenu">
            <a class="menu-link" href="page-products-list.html">
                <i class="icon material-icons md-shopping_bag"></i>
                <span class="text">User</span>
            </a>
            <div class="submenu">
                <a href="{{ route('user.index') }}">User List</a>
            </div>
        </li>

        {{-- Team Menu --}}
        <li class="menu-item has-submenu">
            <a class="menu-link" href="page-products-list.html">
                <i class="icon material-icons md-shopping_bag"></i>
                <span class="text">Team</span>
            </a>
            <div class="submenu">
                <a href="{{ route('teams.create') }}"> Add Team Member </a>
                <a href="{{ route('teams.index') }}">Team Member List</a>
            </div>
        </li>

        {{-- About section --}}
        <li class="menu-item has-submenu">
            <a class="menu-link" href="page-products-list.html">
                <i class="icon material-icons md-shopping_bag"></i>
                <span class="text">About us</span>
            </a>
            <div class="submenu">
                <a href="{{ route('about.create') }}">About us </a>
            </div>
        </li>

    </ul>
    <hr />
    <ul class="menu-aside">
        <li class="menu-item has-submenu">
            <a class="menu-link" href="#">
                <i class="icon material-icons md-settings"></i>
                <span class="text">Settings</span>
            </a>
            <div class="submenu">
                <a href="{{ route('setting.edit') }}">Setting Edit</a>

            </div>
        </li>

    </ul>
    <br />
    <br />
</nav>
