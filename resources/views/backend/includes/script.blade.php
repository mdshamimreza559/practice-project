<script src="{{ asset('backend/assets/style/jquery.js') }}"></script>
<script src="{{ asset('backend/assets/js/vendors/jquery-3.6.0.min.js') }}"></script>
<script src="{{ asset('backend/assets/js/vendors/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('backend/assets/js/vendors/select2.min.js') }}"></script>
<script src="{{ asset('backend/assets/js/vendors/perfect-scrollbar.js') }}"></script>
<script src="{{ asset('backend/assets/js/vendors/jquery.fullscreen.min.js') }}"></script>


<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<!-- Main Script -->
<script src="{{ asset('backend/assets/style/js/jquery.multifield.min.js') }}"></script>
<script src="{{ asset('backend/assets/js/vendors/chart.js') }}"></script>
<!-- Main Script -->
<script src="{{ asset('backend/assets/style/js/sweetalert2@11.js') }}"></script>
<!-- axios Script -->
<script src="{{ asset('backend/assets/style/js/axios.min.js') }}"></script>
<!-- dataTables Script -->
<script src="{{ asset('backend/assets/style/js/jquery.dataTables.min.js') }}"></script>
{{-- ck editor --}}
<script src="https://cdn.ckeditor.com/ckeditor5/35.1.0/classic/ckeditor.js"></script>
<script src="{{ asset('backend/assets/js/main.js?v=1.1') }}" type="text/javascript"></script>
<script src="{{ asset('backend/assets/js/custom-chart.js') }}" type="text/javascript"></script>
