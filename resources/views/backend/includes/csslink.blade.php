<link rel="shortcut icon" type="image/x-icon" href="{{ asset('backend/assets/imgs/theme/favicon.svg') }}" />
<!-- Template CSS -->
<link href="{{ asset('backend/assets/css/main.css?v=1.1') }}" rel="stylesheet" type="text/css" />

{{-- fontawesome css --}}
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css"
    integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w=="
    crossorigin="anonymous" referrerpolicy="no-referrer" />

<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
{{-- Data Table css --}}
<link rel="stylesheet" href="{{ asset('backend/assets/style/css/jquery.dataTables.min.css') }}">

<link rel="stylesheet" href="{{ asset('backend/assets/style/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('backend/assets/style/css/style.css') }}">
