<x-guest-layout>
    @section('title', 'Register Form')
    <x-auth-card>
        <x-slot name="logo">
            <a href="/">
                <x-application-logo class="w-20 h-20 fill-current text-gray-500" />
            </a>
        </x-slot>

        <!-- Validation Errors -->
        <x-auth-validation-errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('register') }}">
            @csrf

            <!-- Name -->
            <div class="row">
                <div class="col-md-6">
                    <div>
                        <x-input-label for="f_name" :value="__('First Name')" />

                        <x-text-input id="f_name" class="block mt-1 w-full" type="text" name="f_name"
                            :value="old('f_name')" required autofocus />
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mt-4">
                        <x-input-label for="l_name" :value="__('Last Name')" />

                        <x-text-input id="l_name" class="block mt-1 w-full" type="text" name="l_name"
                            :value="old('l_name')" required autofocus />
                    </div>
                </div>
            </div>
            <!-- Phone  -->
            <div class="mt-4">
                <x-input-label for="phone" :value="__('Phone')" />

                <x-text-input id="phone" class="block mt-1 w-full" type="number" name="phone" :value="old('phone')"
                    required autofocus />
            </div>
            <!-- Email Address -->
            <div class="mt-4">
                <x-input-label for="email" :value="__('Email')" />

                <x-text-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')"
                    required />
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-input-label for="password" :value="__('Password')" />

                <x-text-input id="password" class="block mt-1 w-full" type="password" name="password" required
                    autocomplete="new-password" />
            </div>

            <!-- Confirm Password -->
            <div class="mt-4">
                <x-input-label for="password_confirmation" :value="__('Confirm Password')" />

                <x-text-input id="password_confirmation" class="block mt-1 w-full" type="password"
                    name="password_confirmation" required />
            </div>

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Already registered?') }}
                </a>

                <x-primary-button class="ml-4">
                    {{ __('Register') }}
                </x-primary-button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
