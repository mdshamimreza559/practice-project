@extends('frontend.layout.forntmaster')
@section('title', 'Address')
@section('content')
    <!-- Breadcumb Area -->
    <div class="breadcumb_area">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <h5>My Account</h5>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('front.index') }}">Home</a></li>
                        <li class="breadcrumb-item active">My Account</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcumb Area -->

    <!-- My Account Area -->
    <section class="my-account-area section_padding_100_50">
        <div class="container">
            <div class="row">
                @include('frontend.pages.user-sidebar.sidebar')
                <div class="col-12 col-lg-9">
                    <div class="my-account-content mb-50">
                        <h5 class="mb-3">Account Details</h5>
                        <?php $user_id = Auth::user(); ?>
                        <form action="{{ route('update.account', $user_id->id) }}" method="post"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="firstName">First Name *</label>
                                        <input type="text" name="f_name" class="form-control" id="firstName"
                                            placeholder="first name" value="{{ Auth::user()->f_name }}">
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="lastName">Last Name *</label>
                                        <input type="text" name="l_name" class="form-control" id="lastName"
                                            placeholder="last name" value="{{ Auth::user()->l_name }}">
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="emailAddress">Email Address *</label>
                                        <input type="email" name="email" class="form-control" id="emailAddress"
                                            placeholder="phone" value="{{ Auth::user()->email }}">
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="displayName">Phone</label>
                                        <input type="number" name="phone" class="form-control" id="displayName"
                                            placeholder="phone" value="{{ Auth::user()->phone }}">
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="newPass">Your Password</label>
                                        <input type="password" name="old_password" class="form-control" id="newPass"
                                            placeholder="enter new password">
                                    </div>
                                </div>
                                <div class="col-12 col-lg-6">
                                    <div class="form-group">
                                        <label for="confirmPass">Confirm Password</label>
                                        <input type="password" name="new_pass" class="form-control" id="confirmPass"
                                            placeholder="confirm password">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="confirmPass">Photo</label>
                                        <input name="photo" type="file" class="form-control" id="photo"
                                            placeholder="upload your photo">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary">update profile</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h4>Address</h4>
                        </div>
                        <div class="card-body ">
                            <div class=" text-center">
                                <button href="#" class="address-btn" data-toggle="modal" data-target="#editUser">
                                    Add New Address
                                </button>
                            </div>
                            <div class="modal fade" id="editUser" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="false"
                                style="background: rgba(0, 0, 0, .5)">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">Edit Address</h5>
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>

                                        <?php
                                        $user = Auth::user();
                                        ?>
                                        {!! Form::open(['method' => 'post', 'route' => ['userAddress.update', $user->id]]) !!}
                                        <div class="modal-body">
                                            {!! Form::label('f_name', 'First Name') !!}
                                            {!! Form::text('f_name', null, [
                                                'class' => 'form-control form-control-sm',
                                                'placeholder' => 'Name',
                                            ]) !!}
                                            {!! Form::label('l_name', 'Last Name') !!}
                                            {!! Form::text('l_name', null, [
                                                'class' => 'form-control form-control-sm',
                                                'placeholder' => 'Name',
                                            ]) !!}
                                            {!! Form::label('address', 'Address', ['class' => 'mt-4']) !!}
                                            {!! Form::text('address', null, [
                                                'class' => 'form-control form-control-sm',
                                                'placeholder' => 'Inser your address',
                                            ]) !!}

                                            {!! Form::label('phone', 'Phone', ['class' => 'mt-4']) !!}
                                            {!! Form::number('phone', null, [
                                                'class' => 'form-control form-control-sm',
                                                'placeholder' => '+880',
                                            ]) !!}

                                            {!! Form::label('country', 'Country', ['class' => 'mt-4']) !!}
                                            {!! Form::select('country', [1 => 'Bangladesh'], null, [
                                                'class' => 'form-select form-control-sm',
                                                'placeholder' => 'select country',
                                            ]) !!}


                                            {!! Form::label('city', 'City', ['class' => 'mt-4']) !!}
                                            {!! Form::select('city', [1 => 'Rajshahi', 2 => 'Dhaka', 3 => 'Rangpur', 4 => 'Khulna'], null, [
                                                'class' => 'form-select form-control-sm',
                                                'placeholder' => 'select city',
                                            ]) !!}

                                            {!! Form::label('gendar', 'Gender', ['class' => 'mt-4']) !!}
                                            {!! Form::select('gendar', [1 => 'Male', 2 => 'Female'], null, [
                                                'class' => 'form-select form-control-sm',
                                                'placeholder' => 'select city',
                                            ]) !!}

                                            {!! Form::label('post_code', 'Postal Code', ['class' => 'mt-4']) !!}
                                            {!! Form::number('post_code', null, [
                                                'class' => 'form-select form-control-sm',
                                                'placeholder' => 'postal code',
                                            ]) !!}


                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Update Address</button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- My Account Area -->
@endsection


{{-- modal --}}
{{-- <a href="#" class="btn btn-primary btn-sm" >Edit Address</a> --}}
