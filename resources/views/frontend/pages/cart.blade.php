@extends('frontend.layout.forntmaster')
@section('title', 'Cart')

@section('content')
    <!-- Breadcumb Area -->
    <div class="breadcumb_area">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <h5>Cart</h5>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item active">Cart</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcumb Area -->
    <!-- Cart Area -->

    <div class="cart_area section_padding_100_70 clearfix">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-12">
                    <div class="cart-table">
                        <div class="table-responsive" id="cart_list">
                            @include('frontend.includes._cart_lists')
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-6">
                    <div class="cart-apply-coupon mb-30">
                        <h6>Have a Coupon?</h6>
                        <p>Enter your coupon code here &amp; get awesome discounts!</p>
                        <!-- Form -->
                        <div class="coupon-form">

                            <form action="{{ route('coupon.addd') }}" method="post" id="coupon_form">
                                @csrf
                                <input type="number" name="code" class="form-control "
                                    placeholder="Enter Your Coupon Code">
                                <button type="submit" class="btn btn-primary cupon-btn">Apply Coupon</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-lg-5">
                    <div class="cart-total-area mb-30">
                        <h5 class="mb-3">Cart Totals</h5>
                        <div class="table-responsive">
                            <table class="table mb-3">
                                <tbody>
                                    <tr>
                                        <td>Sub Total</td>
                                        <td>${{ Gloudemans\Shoppingcart\Facades\Cart::instance('shopping')->subtotal() }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Saving amount</td>
                                        <td>
                                            @if (Session::has('coupon'))
                                                ${{ session::get('coupon')['value'] }}
                                            @else
                                                0
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Total</td>
                                        <td>
                                            @if (Session::has('coupon'))
                                                ${{ number_format((float) str_replace(',', '', Gloudemans\Shoppingcart\Facades\Cart::instance('shopping')->subtotal()) - session::get('coupon')['value'], 2) }}
                                            @else
                                                ${{ Gloudemans\Shoppingcart\Facades\Cart::instance('shopping')->subtotal() }}
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <a href="{{ route('chechkout1') }}" class="btn btn-primary d-block">Proceed To Checkout</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if (Session::has('msg'))
        @push('js')
            <script>
                Swal.fire({
                    position: 'top-end',
                    toast: true,
                    icon: '<?php echo session('cls'); ?>',
                    title: '<?php echo session('msg'); ?>',
                    showConfirmButton: false,
                    timer: 2000
                })
            </script>
        @endpush
    @endif
@endsection

@push('js')
    <script>
        $('.cupon-btn').on('click', function(e) {
            e.preventDefault();
            let code = $('input[name="code"]').val();

            $('.cupon-btn').html(' <i class="fa fa-spinner  fa-spin  "></i> Applying..')
            $('#coupon_form').submit()
        })
        $(document).on('click', '.qty-text', function() {
            let id = $(this).attr('data-id')
            let spinner = $(this),
                input = spinner.closest('div.quantity').find('input[type="number"]');


            if (input.val() == 1) {
                return false;
            }

            if (input.val() != 1) {
                let newVal = parseFloat(input.val())
                let value = $('#input_qty_' + id).val(newVal)
            }
            let update_qty = $('#update_cart_' + id).attr('product-quantity')
            // alert(update_qty)
            updateProductCart(id, update_qty)
        })

        function updateProductCart(id, update_qty) {
            let rowId = id

            let product_id = $('#input_qty_' + rowId).val()

            let path = "{{ route('cart.update') }}";
            let token = "{{ csrf_token() }}";
            $.ajax({
                url: path,
                type: "post",
                data: {
                    _token: token,
                    id: rowId,
                    product_id: product_id,
                    product_qty: update_qty,
                },
                success: function(data) {
                    $('body .header-ajax').html(data['header']);
                    $('body #cart_list').html(data['cart_lists']);
                    if (data['message']) {
                        Swal.fire({
                            title: data['message'],
                            icon: 'warning',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            confirmButtonText: 'OK'
                        })
                    }
                }
            })
        }

        $(document).on('click', '.cart-delete', function(e) {
            e.preventDefault()
            let cart_id = $(this).attr('id')
            let token = "{{ csrf_token() }}"
            let route = "{{ route('cart.delete') }}"
            $.ajax({
                url: route,
                type: 'post',
                dataType: 'json',
                data: {
                    _token: token,
                    cart_id: cart_id,
                },
                success: function(data) {
                    $('body #header-ajax').html(data['header']);
                    $('body #cart_list').html(data['cart_lists']);
                    if (data['status']) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'warning',
                            toast: true,
                            title: data['msg'],
                            showConfirmButton: false,
                            timer: 1500
                        })
                    }
                }
            })

        })
    </script>
@endpush
