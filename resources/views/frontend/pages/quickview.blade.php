<button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
</button>
<div class="modal-inner-area row">
    <div class="col-lg-5 col-md-6 col-sm-6">
        <!-- Product Details Left -->
        <div class="product-details-left">
            <div class="product-details-images slider-navigation-1">
                <div class="lg-image">
                    <img src="{{ asset('image/product/thumbnail/' . $product->photo) }}" alt="{{ $product->title }}">
                </div>
            </div>
        </div>
        <!--// Product Details Left -->
    </div>

    <div class="col-lg-7 col-md-6 col-sm-6">
        <div class="product-details-view-content pt-60">
            <div class="product-info">
                <h2>{{ $product->title }}</h2>
                <div class="price-box pt-20">
                    <span class="new-price new-price-2">${{ $product->price }}</span>
                </div>
                <div class="product-desc">
                    <p>
                        <span>{!! $product->short_des !!}
                        </span>
                    </p>
                </div>
                {{-- <div class="product-variants">
                    <div class="produt-variants-size">
                        <label>Dimension</label>
                        <select class="nice-select">
                            <option value="1" title="S" selected="selected">40x60cm</option>
                            <option value="2" title="M">60x90cm</option>
                            <option value="3" title="L">80x120cm</option>
                        </select>
                    </div>
                </div> --}}
                <div class="single-add-to-cart">
                    <form action="#" class="cart-quantity">
                        <div class="quantity">
                            <label>Quantity</label>
                            <div class="cart-plus-minus">
                                <input class="cart-plus-minus-box" value="1" type="text">
                                <div class="dec qtybutton"><i class="fa fa-angle-down"></i></div>
                                <div class="inc qtybutton"><i class="fa fa-angle-up"></i></div>
                            </div>
                        </div>
                        <a href="#" class="cart-id" data-id="{{ $product->id }}" quantity="1"
                            id="add_cart_id_{{ $product->id }}"> <button class="btn btn-outline-success btn-sm">Add to
                                cart</button></a>

                    </form>
                </div>
                <div class="product-additional-info pt-25">
                    <a class="links-details wish-list" href="javascript:void(0)" data-id="{{ $product->id }}"
                        quantity="1" id="move_to_cart_{{ $product->id }}"><i class="fa fa-heart-o"></i>Add to
                        wishlist
                    </a>

                    <div class="product-social-sharing pt-25">
                        <ul>
                            <li class="facebook"><a href="#"><i class="fa fa-facebook"></i>Facebook</a></li>
                            <li class="twitter"><a href="#"><i class="fa fa-twitter"></i>Twitter</a></li>
                            <li class="google-plus"><a href="#"><i class="fa fa-google-plus"></i>Google +</a>
                            </li>
                            <li class="instagram"><a href="#"><i class="fa fa-instagram"></i>Instagram</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
