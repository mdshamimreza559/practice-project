@extends('frontend.layout.forntmaster')
@section('title', 'categories')
@section('content')
    <div class="body-wrapper">
        @include('frontend.pages._single_categeories')
    </div>
    <div class="loader ajax-load text-center" style="display: none">
        <img src="{{ asset('frontend/images/Loading_icon.gif') }}" alt="">
    </div>
@endsection

@push('js')
    <script>
        // $('#sortBy').on('change', function() {
        //     let sort = $(this).val()
        //     window.location = "{{ url('' . $route . '') }}/{{ $categories->slug }}?sort=" + sort
        // })

        // function loadmoreData(page) {
        //     $.ajax({
        //             url: '?page=' + page,
        //             type: 'get',
        //             beforeSend: function() {
        //                 $('.ajax-load').show()
        //             }
        //         })
        //         .done(function(data) {
        //             if (data.html == '') {
        //                 $('.ajax-load').html('No more product found')
        //                 return;
        //             }
        //             $('.ajax-load').hide()
        //             $('.ajax-load').append(data.html)
        //         })
        //         .fail(function() {
        //             alert('Something went wrong! plz try again later')
        //         })
        // }
        // var page = 1
        // $(window).scroll(function() {
        //     if ($(window).scrollTop() + $(window).height() > 70 = $(document).height()) {
        //         page++;
        //         loadmoreData(page);
        //     }
        // })
    </script>
@endpush
