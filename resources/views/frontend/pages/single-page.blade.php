@extends('frontend.layout.forntmaster')
@section('title', 'Single Product')
@section('content')
    <div class="breadcrumb-area">
        <div class="container">
            <div class="breadcrumb-content">
                <ul>
                    <li><a href="index.html">Home</a></li>
                    <li class="active">Single Product</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Breadcrumb Area End Here -->

    <!-- content-wraper start -->
    <div class="content-wraper">
        <div class="container">
            <div class="row single-product-area">
                <div class="col-lg-5 col-md-6">
                    <!-- Product Details Left -->
                    <div class="product-details-left">
                        <div class="product-details-images slider-navigation-1">
                            {{-- @foreach ($single_products as $s_product) --}}
                            <div class="lg-image">
                                <a class="popup-img venobox vbox-item"
                                    href="{{ asset('image/product/thumbnail/' . $single_products->photo) }}"
                                    data-gall="myGallery">
                                    <img src="{{ asset('image/product/thumbnail/' . $single_products->photo) }}"
                                        alt="{{ $single_products->title }}">
                                </a>
                            </div>
                            {{-- @endforeach --}}
                        </div>

                    </div>
                    <!--// Product Details Left -->
                </div>

                <div class="col-lg-7 col-md-6">
                    <div class="product-details-view-content pt-5 pb-5" id="product_details">
                        <div class="product-info ">
                            <div class="attribute">
                                <h2>{{ $single_products->title }}</h2>

                                <div class="price-box pt-20">
                                    @if ($single_products->discount)
                                        <span
                                            class="new-price new-price-2">${{ number_format($single_products->offer_price, 2) }}</span>
                                    @else
                                        <span
                                            class="new-price new-price-2">${{ number_format($single_products->price, 2) }}</span>
                                    @endif

                                </div>

                            </div>

                            <div class="product-desc">
                                <p>
                                    <span>{{ $single_products->short_des }}
                                    </span>
                                </p>
                            </div>
                            <div class="product-variants">
                                <div class="produt-variants-size">
                                    <p>Size</p>

                                    @foreach ($single_products->productAttr as $productAttr)
                                        <button onclick="sizefilter({{ $productAttr->id }})" class="btn btn-sm btn-success">
                                            {{ $productAttr->size }}</button>
                                    @endforeach

                                </div>
                            </div>
                            <div class="single-add-to-cart">
                                <form action="#" class="cart-quantity">
                                    <div class="quantity">
                                        <label>Quantity</label>
                                        <div class="cart-plus-minus">
                                            <input class="cart-plus-minus-box" value="1" type="text">
                                            <div class="dec qtybutton"><i class="fa fa-angle-down"></i></div>
                                            <div class="inc qtybutton"><i class="fa fa-angle-up"></i></div>
                                        </div>
                                    </div>

                                    <button class="add-to-cart cart-id" data-id="{{ $single_products->id }}" quantity='1'
                                        id="add_to_cart_{{ $single_products->id }}" type="submit">Add to
                                        cart</button>
                                </form>
                            </div>
                            <div class="product-additional-info pt-25">
                                <a class="links-details wish-list" href="javascript:void(0)"
                                    data-id="{{ $single_products->id }}" quantity="1"
                                    id="move_to_cart_{{ $single_products->id }}">
                                    <i class="fa fa-heart-o me-1"></i> Add to wishlist
                                </a>
                                <div class="product-social-sharing pt-25">
                                    <ul>
                                        <li class="facebook"><a href="#"><i class="fa fa-facebook"></i>Facebook</a>
                                        </li>
                                        <li class="twitter"><a href="#"><i class="fa fa-twitter"></i>Twitter</a>
                                        </li>
                                        <li class="google-plus"><a href="#"><i class="fa fa-google-plus"></i>Google
                                                +</a></li>
                                        <li class="instagram"><a href="#"><i class="fa fa-instagram"></i>Instagram</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- content-wraper end -->

    <!-- Begin Product Area -->
    <div class="product-area pt-35">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="li-product-tab">

                        @php
                            $review = App\Models\ProductReview::with('user')
                                ->where('product_id', $single_products->id)
                                ->latest()
                                ->paginate(4);

                        @endphp
                        <ul class="nav li-product-menu">
                            <li><a data-toggle="tab" href="#description"><span>Description</span></a></li>
                            <li><a class="active" data-toggle="tab" href="#reviews"><span>Reviews</span>
                                    <small> ({{ count($review) }})</small></a></li>
                        </ul>
                    </div>
                    <!-- Begin Li's Tab Menu Content Area -->
                </div>
            </div>
            <div class="tab-content">
                <div id="description" class="tab-pane  show" role="tabpanel">
                    <div class="product-description">
                        <span>{{ $single_products->description }}</span>
                    </div>
                </div>

                <div id="reviews" class="tab-pane active" role="tabpanel">
                    <div class="product-reviews">
                        <div class="product-details-comment-block">
                            @if ($review->count() > 0)
                                @foreach ($review as $feadback)
                                    <div class="review_section mb-3 border p-2">
                                        <div class="comment-review">
                                            <span>{{ $feadback->reason }}</span>
                                            <ul class="rating">
                                                @for ($i = 0; $i < 5; $i++)
                                                    @if ($feadback->rate > $i)
                                                        <li><i class="fa fa-star"></i></li>
                                                    @else
                                                        <li class="no-star"><i class="fa fa-star"></i></li>
                                                    @endif
                                                @endfor
                                            </ul>
                                        </div>
                                        <div class="comment-author-infos mt-2">
                                            <span>{{ $feadback->review }}</span>
                                            <em class="mt-1">by
                                                <a href="javascript:void(0)" class="text-success">
                                                    {{ $feadback->user->f_name . ' ' . $feadback->user->l_name . ' ' }}</a>
                                                on
                                                {{ Carbon\Carbon::parse($feadback->created_at)->format('d-M-Y') }}</em>
                                        </div>
                                    </div>
                                @endforeach
                            @endif

                            {{ $review->links('vendor.pagination.custom') }}

                            @auth
                                <div class="review-btn">
                                    <a class="review-links" href="#" data-toggle="modal" data-target="#mymodal">Write
                                        Your Review!</a>
                                </div>
                            @else
                                <p>you need to login write your review. <a href="{{ route('user.login') }}">click here!</a>
                                </p>
                                @endif

                                {{-- {{ $review->links('vendor.pagination.custom') }} --}}

                                <!-- Begin Quick View | Modal Area -->
                                <div class="modal fade modal-wrapper" id="mymodal">
                                    <div class="modal-dialog modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                            <div class="modal-body">
                                                <h3 class="review-page-title">Write Your Review</h3>
                                                <div class="modal-inner-area row">
                                                    <div class="col-lg-6">
                                                        <div class="li-review-product">
                                                            <img src="{{ asset('image/product/thumbnail/' . $single_products->photo) }}"
                                                                alt="{{ $single_products->title }}">
                                                            <div class="li-review-product-desc">
                                                                <p class="li-product-name">{{ $single_products->title }}
                                                                </p>
                                                                <p>
                                                                    <span>{{ $single_products->description }} </span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="li-review-content">
                                                            <!-- Begin Feedback Area -->
                                                            <div class="submit_a_review_area mt-50">
                                                                <h4>Submit A Review</h4>
                                                                <form
                                                                    action="{{ route('review.store', $single_products->slug) }}"
                                                                    method="post">
                                                                    @csrf
                                                                    <div class="form-group">
                                                                        <span>Your Ratings</span>
                                                                        <div class="stars">
                                                                            <input type="radio" name="rate"
                                                                                value="1" class="star-1" id="star-1">
                                                                            <label class="star-1" for="star-1">1</label>
                                                                            <input type="radio" name="rate"
                                                                                value="2" class="star-2" id="star-2">
                                                                            <label class="star-2" for="star-2">2</label>
                                                                            <input type="radio" name="rate"
                                                                                value="3" class="star-3" id="star-3">
                                                                            <label class="star-3" for="star-3">3</label>
                                                                            <input type="radio" name="rate"
                                                                                value="4" class="star-4" id="star-4">
                                                                            <label class="star-4" for="star-4">4</label>
                                                                            <input type="radio" name="rate"
                                                                                value="5" class="star-5" id="star-5">
                                                                            <label class="star-5" for="star-5">5</label>
                                                                            <span></span>
                                                                        </div>
                                                                        @error('rate')
                                                                            <p class="position-absolute text-danger">
                                                                                <small>{{ $message }}</small>
                                                                            </p>
                                                                        @enderror
                                                                    </div>
                                                                    <input name="product_id" type="hidden"
                                                                        value="{{ $single_products->id }}">
                                                                    @if (Auth::user())
                                                                        <input name="user_id" type="hidden"
                                                                            value="{{ Auth::user()->id }}">
                                                                    @endif

                                                                    <div class="form-group">
                                                                        <label for="options">Reason for your rating</label>
                                                                        <select name="reason"
                                                                            class="form-control small right py-0 w-100"
                                                                            id="options">
                                                                            <option value="quality"
                                                                                {{ old('reason') == 'quality' ? 'selected' : '' }}>
                                                                                Quality
                                                                            </option>
                                                                            <option value="value"
                                                                                {{ old('reason') == 'value' ? 'selected' : '' }}>
                                                                                Value
                                                                            </option>
                                                                            <option value="design"
                                                                                {{ old('reason') == 'design' ? 'selected' : '' }}>
                                                                                Design
                                                                            </option>
                                                                            <option value="price"
                                                                                {{ old('reason') == 'price' ? 'selected' : '' }}>
                                                                                Price
                                                                            </option>
                                                                            <option value="others"
                                                                                {{ old('reason') == 'others' ? 'selected' : '' }}>
                                                                                Others
                                                                            </option>
                                                                        </select>
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <label for="comments">Comments</label>
                                                                        <textarea class="form-control" id="comments" name="review" rows="5" data-max-length="150"></textarea>
                                                                    </div>
                                                                    <div class="feedback-btn pb-15">
                                                                        <a href="#" class="close" data-dismiss="modal"
                                                                            aria-label="Close">Close</a>
                                                                        <button class="review_btn"
                                                                            type="submit">Submit</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                            <!-- Feedback Area End Here -->

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Quick View | Modal Area End Here -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Product Area End Here -->

        <!-- Related Product Area -->
        <section class="product-area li-laptop-product pt-30 pb-50">
            <div class="container">
                <div class="row">
                    <!-- Begin Li's Section Area -->
                    <div class="col-lg-12">
                        <div class="li-section-title">
                            <h2>
                                <span>Related Products</span>
                            </h2>
                        </div>
                        <div class="row">
                            <div class="product-active owl-carousel">
                                @foreach ($related_products as $item)
                                    <div class="col-lg-12">
                                        <!-- single-product-wrap start -->
                                        <div class="single-product-wrap">
                                            <div class="product-image">
                                                <a href="{{ route('single.product', $item->slug) }}">
                                                    <img src="{{ asset('image/product/thumbnail/' . $item->photo) }}"
                                                        alt="{{ $item->title }}">
                                                </a>
                                                <span class="sticker">New</span>
                                            </div>
                                            <div class="product_desc">
                                                <div class="product_desc_info">
                                                    <div class="product-review">
                                                        <h5 class="manufacturer">
                                                            <a
                                                                href="{{ route('front.categories', $item->category->slug) }}">{{ $item->category->name }}</a>
                                                        </h5>
                                                        <div class="rating-box">
                                                            <ul class="rating">
                                                                <li><i class="fa fa-star-o"></i></li>
                                                                <li><i class="fa fa-star-o"></i></li>
                                                                <li><i class="fa fa-star-o"></i></li>
                                                                <li class="no-star"><i class="fa fa-star-o"></i></li>
                                                                <li class="no-star"><i class="fa fa-star-o"></i></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                    <h4><a class="product_name"
                                                            href="{{ route('single.product', $item->slug) }}">{{ $item->title }}</a>
                                                    </h4>
                                                    <div class="price-box">
                                                        @if ($item->offer_price)
                                                            <span
                                                                class="new-price">${{ number_format($item->offer_price, 2) }}</span>

                                                            <del
                                                                class="me-2 text-danger">${{ number_format($item->price, 2) }}</del>
                                                        @endif

                                                    </div>
                                                </div>
                                                <div class="add-actions">
                                                    <ul class="add-actions-link">
                                                        <li class="add-cart active">
                                                            <a href="#" class="cart-id" data-id="{{ $item->id }}"
                                                                quantity="1" id="add_cart_id_{{ $item->id }}">Add
                                                                to cart
                                                            </a>
                                                        </li>
                                                        <li>
                                                            <a href="#" title="quick view" class="quick-view-btn"
                                                                data-toggle="modal" data-target="#quick_view_modal"
                                                                onclick="quickview({{ $item->id }})"><i
                                                                    class="fa fa-eye"></i>
                                                            </a>
                                                        </li>
                                                        <li> <a class="links-details wish-list" href="javascript:void(0)"
                                                                data-id="{{ $item->id }}" quantity="1"
                                                                id="move_to_cart_{{ $item->id }}"><i
                                                                    class="fa fa-heart-o"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- single-product-wrap end -->
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <!-- Li's Section Area End Here -->
                </div>
            </div>
        </section>
    @endsection
    @push('js')
        <script>
            function sizefilter(id) {
                $('#product_details').show()
                $('#product_details').html(null)
                $.post('{{ route('size.filter') }}', {
                    _token: '{{ csrf_token() }}',
                    id: id
                }, function(data) {
                    $('#product_details').html(data)
                })
            }
        </script>
    @endpush
