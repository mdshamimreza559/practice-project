@extends('frontend.layout.forntmaster')
@section('title', 'wishlists')
@section('content')
    <!-- Begin Li's Breadcrumb Area -->
    <div class="breadcrumb-area">
        <div class="container">
            <div class="breadcrumb-content">
                <ul>
                    <li><a href="{{ route('front.index') }}">Home</a></li>
                    <li class="active">Wishlist</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Li's Breadcrumb Area End Here -->
    <!--Wishlist Area Strat-->
    <div class="wishlist-area pt-60 pb-60">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <form action="#">
                        <div class="table-content table-responsive" id="wishlist_list">
                            @include('frontend.includes._wishlist_list')
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--Wishlist Area End-->
@endsection
@push('js')
    <script>
        $('.wishlist-delete').on('click', function(e) {
            e.preventDefault()
            let wishlist_id = $(this).attr('data-id')
            let path = "{{ route('wishlist.delete') }}";
            let token = "{{ csrf_token() }}";
            $.ajax({
                url: path,
                type: "POST",
                dataType: "JSON",
                data: {
                    _token: token,
                    wish_id: wishlist_id,
                },
                success: function(data) {
                    if (data) {
                        $('body .header-ajax').html(data['header']);
                        $('#wishlist_list').html(data['wishlist_list'])
                        Swal.fire({
                            position: 'top-end',
                            icon: 'warning',
                            toast: true,
                            title: data['msg'],
                            showConfirmButton: false,
                            timer: 2500
                        })
                    }
                }
            })
        })

        $(document).on('click', '.move-to-cart', function(e) {
            e.preventDefault();
            let product_data_id = $(this).attr('data-id')
            let token = "{{ csrf_token() }}"
            let path = "{{ route('move.cart') }}"
            $.ajax({
                url: path,
                type: "POST",
                data: {
                    product_id: product_data_id,
                    _token: token,
                },
                beforeSend: function() {
                    $('#add_to_cart_' + product_data_id).html(
                        '<i class="fa fa-spinner  fa-spin  "></i> move to cart... ')
                },
                success: function(data) {
                    if (data['status']) {
                        $('body #cart_count').html(data['cart_count']);
                        $('body .header-ajax').html(data['header']);
                        $('body #wishlist_list').html(data['wishlist']);

                        Swal.fire({
                            title: data['msg'],
                            icon: 'warning',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'ok'
                        })
                    }

                }
            })
        })
    </script>
@endpush
