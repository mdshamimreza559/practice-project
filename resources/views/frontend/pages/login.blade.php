@extends('frontend.layout.forntmaster')

@section('title', 'Login & Register')

@section('content')
    <!-- Begin Li's Breadcrumb Area -->
    <div class="breadcrumb-area">
        <div class="container">
            <div class="breadcrumb-content">
                <ul>
                    <li><a href="index.html">Home</a></li>
                    <li class="active">Login Register</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Li's Breadcrumb Area End Here -->
    <!-- Begin Login Content Area -->
    <div class="page-section mb-60">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-xs-12 col-lg-6 mb-30">
                    <!-- Login Form s-->
                    <form action="{{ route('login.submit') }}" method="POST">
                        @csrf
                        <div class="login-form">

                            <h4 class="login-title">Login</h4>
                            <div class="row">
                                <div class="col-md-12 col-12 mb-20">
                                    <label>Email Address*</label>
                                    <input name="email" class="mb-0" type="email" placeholder="Email Address">



                                </div>
                                <div class="col-12 mb-20">
                                    <label>Password</label>
                                    <input name="password" class="mb-0" type="password" placeholder="Password">
                                    <p class="mb-0 text-danger error-msg position-absolute">
                                        @if (Session::has('error'))
                                            <span>{{ Session::get('error') }} </span>
                                        @endif

                                    </p>
                                </div>
                                <div class="col-md-8">
                                    <div class="check-box d-inline-block ml-0 ml-md-2 mt-10">
                                        <input type="checkbox" id="remember_me">
                                        <label for="remember_me">Remember me</label>
                                    </div>
                                </div>
                                <div class="col-md-4 mt-10 mb-20 text-left text-md-right">
                                    <a href="#"> Forgotten pasward?</a>
                                </div>
                                <div class="col-md-12">
                                    <button class="register-button mt-0">Login</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6 col-xs-12">
                    <form action="{{ route('register.submit') }}" method="post">
                        @csrf
                        <div class="login-form">
                            <h4 class="login-title">Register</h4>
                            <div class="row">
                                <div class="col-md-6 col-12 mb-20">
                                    <label>First Name</label>
                                    <input name="f_name" class="mb-0" type="text" placeholder="First Name"
                                        value="{{ old('f_name') }}">
                                    @error('f_name')
                                        <p class="mb-0 error-msg text-danger position-absolute">
                                            <span>{{ $message }}</span>
                                        </p>
                                    @enderror
                                </div>
                                <div class="col-md-6 col-12 mb-20">
                                    <label>Last Name</label>
                                    <input name="l_name" class="mb-0" type="text" placeholder="Last Name"
                                        value="{{ old('l_name') }}">
                                    @error('l_name')
                                        <p class="mb-0 text-danger error-msg position-absolute">
                                            <span>{{ $message }}</span>
                                        </p>
                                    @enderror
                                </div>
                                <div class="col-md-6 col-12 mb-20">
                                    <label>Email Address*</label>
                                    <input name="reg_email" class="mb-0" type="email" placeholder="Email Address"
                                        value="{{ old('reg_email') }}">
                                    @error('reg_email')
                                        <p class="mb-0 text-danger error-msg position-absolute">
                                            <span>{{ $message }}</span>
                                        </p>
                                    @enderror
                                </div>
                                <div class="col-md-6 col-12 mb-20">
                                    <label>Phone</label>
                                    <input name="phone" class="mb-0" type="number" placeholder="phone number"
                                        value="{{ old('phone') }}">
                                    @error('phone')
                                        <p class="mb-0 text-danger error-msg position-absolute">
                                            <span>{{ $message }}</span>
                                        </p>
                                    @enderror
                                </div>
                                <div class="col-md-6 mb-20">
                                    <label>Password</label>
                                    <input name="reg_password" id="password" class="mb-0" type="password"
                                        placeholder="Password" required autocomplete="new-password">
                                    @error('reg_password')
                                        <p class="mb-0  text-danger error-msg position-absolute">
                                            <span>{{ $message }}</span>
                                        </p>
                                    @enderror

                                </div>
                                <div class="col-md-6 mb-20">
                                    <label>Confirm Password</label>
                                    <input id="password_confirmation" class="mb-0" type="password"
                                        name="reg_password_confirmation" required>


                                </div>
                                <div class="col-12">
                                    <button class="register-button mt-0">Register</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Login Content Area End Here -->
@endsection
