<div class="col-12 col-lg-3">
    <div class="my-account-navigation mb-50">
        <ul>
            <li class="{{ \Request::is('user/dashboard') ? 'active' : '' }}"><a id="account_sideBar"
                    href="{{ route('user.dashboard') }}">Dashboard</a></li>
            <li class="{{ \Request::is('user/orders') ? 'active' : '' }}"><a href="{{ route('user.orders') }}">Orders</a>
            </li>
            <li><a href="downloads.html">Downloads</a></li>
            <li class="{{ Request::is('user/wishlists') ? 'active' : '' }}"><a
                    href="{{ route('user.wishlist') }}">Wishlist</a></li>
            <li class="{{ \Request::is('user/account-deatils') ? 'active' : '' }}"><a
                    href="{{ route('user.deatils') }}">Manage Profile</a></li>
            <li><a href="{{ route('user.logout') }}">Logout</a></li>
        </ul>
    </div>
</div>
