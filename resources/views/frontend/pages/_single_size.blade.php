<div class="product-info ">
    <div class="attribute">
        <h2>{{ $product->title }} <span class="text-success ms-5">({{ $size->size }})</span></h2>

        <div class="price-box pt-20">
            @if ($size->offer_price)
                <span class="new-price new-price-2">${{ number_format($size->offer_price, 2) }}</span>
                <del>${{ number_format($size->price, 2) }}</del>
            @else
                <span class="new-price new-price-2">${{ number_format($size->price, 2) }}</span>
            @endif

        </div>

    </div>

    <div class="product-desc">
        <p>
            <span>{{ $product->short_des }}
            </span>
        </p>
    </div>
    <div class="product-variants">
        <div class="produt-variants-size">
            <p>Size</p>

            @foreach ($attr as $productAttr)
                <button onclick="sizefilter({{ $productAttr->id }})" class="btn btn-sm btn-success">
                    {{ $productAttr->size }}</button>
            @endforeach

        </div>
    </div>
    <div class="single-add-to-cart">
        <form action="#" class="cart-quantity">
            <div class="quantity">
                <label>Quantity</label>
                <div class="cart-plus-minus">
                    <input class="cart-plus-minus-box" value="1" type="text">
                    <div class="dec qtybutton"><i class="fa fa-angle-down"></i></div>
                    <div class="inc qtybutton"><i class="fa fa-angle-up"></i></div>
                </div>
            </div>

            <button class="add-to-cart cart-id" data-id="{{ $product->id }}" quantity='1'
                id="add_to_cart_{{ $product->id }}" type="submit">Add to
                cart</button>
        </form>
    </div>
    <div class="product-additional-info pt-25">
        <a class="links-details wish-list" href="javascript:void(0)" data-id="{{ $product->id }}" quantity="1"
            id="move_to_cart_{{ $product->id }}">
            <i class="fa fa-heart-o me-1"></i> Add to wishlist
        </a>
        <div class="product-social-sharing pt-25">
            <ul>
                <li class="facebook"><a href="#"><i class="fa fa-facebook"></i>Facebook</a>
                </li>
                <li class="twitter"><a href="#"><i class="fa fa-twitter"></i>Twitter</a>
                </li>
                <li class="google-plus"><a href="#"><i class="fa fa-google-plus"></i>Google
                        +</a></li>
                <li class="instagram"><a href="#"><i class="fa fa-instagram"></i>Instagram</a>
                </li>
            </ul>
        </div>
    </div>

</div>
