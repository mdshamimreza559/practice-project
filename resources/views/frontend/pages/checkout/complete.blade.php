@extends('frontend.layout.forntmaster')
@section('title', 'checkout complate')
@section('content')
    <!-- Checkout Area -->
    <div class="checkout_area section_padding_100">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-8 order_complated_area">
                    <div class=" clearfix text-center">
                        <h3>Thank You For Your Order.</h3>
                        {{-- <p>You will receive an email of your order details</p> --}}
                        <strong class="orderid mb-0 d-block">Your Order code #{{ $order_details->order_number }}</strong>
                        <small>A copy or your order summary has been sent to</small>
                    </div>
                    <div class="row p-3">
                        <div class="col-lg-12">
                            <h6>Order summary</h6>
                        </div>

                        <div class="col-lg-6 ">
                            <table class="table  table-sm">
                                <tbody>
                                    <tr>
                                        <th> Order Code</th>
                                        <td> #{{ $order_details->order_number }}</td>
                                    </tr>
                                    <tr>
                                        <th>Name</th>
                                        <td> {{ $order_details->first_name . ' ' . $order_details->last_name }}</td>
                                    </tr>
                                    <tr>
                                        <th> Email</th>
                                        <td> {{ $order_details->email }}</td>
                                    </tr>
                                    <tr>
                                        <th> Shipping Address</th>
                                        <td> {{ $order_details->saddress }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-6 ">
                            <table class="table  table-sm">
                                <tbody>
                                    <tr>
                                        <th> Order Date</th>
                                        <td> {{ Carbon\Carbon::parse($order_details->created_at)->format('d-M-Y g:ia') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Order Status</th>
                                        <td> {{ $order_details->condition }}</td>
                                    </tr>
                                    <tr>
                                        <th> Total order amount </th>
                                        <td> ${{ $order_details->total_amount }}</td>
                                    </tr>
                                    <tr>
                                        <th> Shipping </th>
                                        <td> {{ $order_details->sapartment_suite }}</td>
                                    </tr>
                                    <tr>
                                        <th> Payment method</th>
                                        @if ($order_details->payment_method == 'cod')
                                            <td>Cash on Delivery</td>
                                        @else
                                            <td> {{ $order_details->payment_method }}</td>
                                        @endif
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="row p-3">
                        <div class="col-lg-12">
                            <h4>Order details</h4>
                        </div>
                        <div class="col-lg-12">
                            <table class="table table-sm table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Product</th>
                                        <th>Delivery Type</th>
                                        <th>Quantity</th>
                                        <th>price </th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $order_details = \App\Models\Order::where('user_id', Auth::user()->id)
                                            ->latest()
                                            ->first();
                                    @endphp
                                    @foreach ($order_details->orderProducts as $ordrr)
                                        <tr>
                                            <td>{{ $loop->iteration }}</td>
                                            <td>{{ $ordrr->product->title }} </td>
                                            @if ($order_details->payment_method == 'cod')
                                                <td>Cash on Delivery</td>
                                            @else
                                                <td> {{ $order_details->payment_method }}</td>
                                            @endif
                                            <td>{{ $ordrr->quantity }} </td>
                                            <td>{{ $ordrr->price }} </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-lg-6"></div>
                        <div class="col-lg-6">
                            <table class="table table-sm ">
                                <tbody>
                                    <tr>
                                        <th>Subtotal</th>
                                        <td>${{ $order_details->sub_total }}</td>
                                    </tr>
                                    <tr>
                                        <th>Shipping</th>
                                        <td>${{ $order_details->delivery_charge }}</td>
                                    </tr>
                                    <tr>
                                        <th>Total</th>
                                        <td>${{ $order_details->total_amount }}</td>
                                    </tr>
                                    <tr>
                                        <th> Partial payment</th>
                                        @if ($order_details->payment_method == 'cod')
                                            <td>$0</td>
                                        @endif
                                    </tr>
                                    <tr>
                                        <th>Total Due</th>
                                        <th>${{ $order_details->total_amount }}</th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Checkout Area End -->

@endsection
