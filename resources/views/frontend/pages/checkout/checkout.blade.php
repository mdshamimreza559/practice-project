@extends('frontend.layout.forntmaster')
@section('title', 'Checkout')
@section('content')


    <!-- Breadcumb Area -->
    <div class="breadcumb_area">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <h5>Checkout</h5>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item active">Checkout</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcumb Area -->

    <!-- Checkout Step Area -->
    <div class="checkout_steps_area">
        <a class="active" href="checkout-2.html"><i class="icofont-check-circled"></i> Billing</a>
        <a href="checkout-3.html"><i class="icofont-check-circled"></i> Shipping</a>
        <a href="checkout-4.html"><i class="icofont-check-circled"></i> Payment</a>
        <a href="checkout-5.html"><i class="icofont-check-circled"></i> Review</a>
    </div>

    <!-- Checkout Area -->
    <div class="checkout_area section_padding_100">
        <div class="container">
            <form action="{{ route('checkout1.store') }}" method="post">
                @csrf
                <div class="row">
                    <div class="col-12">
                        <div class="checkout_details_area clearfix">
                            <h5 class="mb-4">Billing Details</h5>

                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="first_name">First Name</label>
                                    <input name="first_name" type="text" class="form-control" id="first_name"
                                        placeholder="First Name" value="{{ Auth::user()->f_name }}" required>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="last_name">Last Name</label>
                                    <input name="last_name" type="text" class="form-control" id="last_name"
                                        placeholder="Last Name" value="{{ Auth::user()->l_name }}" required>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="company">Company Name</label>
                                    <input name="company" type="text" class="form-control" id="company"
                                        placeholder="Company Name" value="">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="email_address">Email Address</label>
                                    <input name="email" type="email" class="form-control" id="email_address"
                                        placeholder="Email Address" value="{{ Auth::user()->email }}" readonly>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="phone_number">Phone Number</label>
                                    <input name="phone_number" type="number" class="form-control" id="phone_number"
                                        min="0" value="{{ Auth::user()->phone }}">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="country">Country</label>
                                    <select name="country" class="custom-select d-block w-100 form-control" id="country">
                                        <option value="usa">United States</option>
                                        <option value="uk">United Kingdom</option>
                                        <option value="ger">Germany</option>
                                        <option value="fra">France</option>
                                        <option value="ind">India</option>
                                        <option value="aus">Australia</option>
                                        <option value="bra">Brazil</option>
                                        <option value="cana">Canada</option>
                                    </select>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label for="street_address">Street address</label>
                                    <input name="address" type="text" class="form-control" id="street_address"
                                        placeholder="Street Address" value="">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="apartment_suite">Apartment/Suite/Unit</label>
                                    <input name="apartment_suite" type="text" class="form-control" id="apartment_suite"
                                        placeholder="Apartment, suite, unit etc" value="">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="city">Town/City</label>
                                    <input name="city" type="text" class="form-control" id="city"
                                        placeholder="Town/City" value="">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="state">State</label>
                                    <input name="state" type="text" class="form-control" id="state"
                                        placeholder="State" value="">
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="postcode">Postcode/Zip</label>
                                    <input name="postcode" type="number" class="form-control" id="postcode"
                                        placeholder="Postcode / Zip" value="">
                                </div>
                                <div class="col-md-12">
                                    <label for="order-notes">Order Notes</label>
                                    <textarea class="form-control" name="note" id="order-notes" cols="30" rows="10"
                                        placeholder="Notes about your order, e.g. special notes for delivery."></textarea>
                                </div>
                            </div>

                            <!-- Different Shipping Address -->
                            <div class="different-address mt-50">
                                <div class="ship-different-title mb-3">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1">
                                        <label class="custom-control-label" for="customCheck1">Ship to a same
                                            address?</label>
                                    </div>
                                </div>
                                <div class="row shipping_input_field">
                                    <div class="col-md-6 mb-3">
                                        <label for="first_name">First Name</label>
                                        <input name="sfirst_name" type="text" class="form-control" id="sfirst_name"
                                            placeholder="First Name" value="" required>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="last_name">Last Name</label>
                                        <input name="slast_name" type="text" class="form-control" id="slast_name"
                                            placeholder="Last Name" value="" required>
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="company">Company Name</label>
                                        <input name="scompany" type="text" class="form-control" id="scompany"
                                            placeholder="Company Name" value="">
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="email_address">Email Address</label>
                                        <input name="semail" type="email" class="form-control" id="semail"
                                            placeholder="Email Address" value="">
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="phone_number">Phone Number</label>
                                        <input name="sphone" type="number" class="form-control" id="sphone"
                                            min="0" value="">
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="country">Country</label>
                                        <select name="scountry" class="custom-select d-block w-100 form-control"
                                            id="scountry">

                                        </select>
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <label for="street_address">Street address</label>
                                        <input name="saddress" type="text" class="form-control" id="saddress"
                                            placeholder="Street Address" value="">
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="apartment_suite">Apartment/Suite/Unit</label>
                                        <input name="sapartment_suite" type="text" class="form-control"
                                            id="sapartment_suite" placeholder="Apartment, suite, unit etc"
                                            value="">
                                    </div>
                                    <div class="col-md-6 mb-3">
                                        <label for="city">Town/City</label>
                                        <input name="scity" type="text" class="form-control" id="scity"
                                            placeholder="Town/City" value="">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="state">State</label>
                                        <input name="sstate" type="text" class="form-control" id="sstate"
                                            placeholder="State" value="">
                                    </div>
                                    <div class="col-md-6">
                                        <label for="postcode">Postcode/Zip</label>
                                        <input name="spostcode" type="number" class="form-control" id="spostcode"
                                            placeholder="Postcode / Zip" value="">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                    <input type="hidden" name="sub_total" id=""
                        value="{{ Cart::instance('shopping')->subtotal() }}">

                    <input type="hidden" name="total_amount" value="{{ Cart::instance('shopping')->subtotal() }}">





                    <div class="col-12">
                        <div class="checkout_pagination d-flex justify-content-end mt-50">
                            <a href="{{ route('cart.details') }}" class="btn btn-primary mt-2 ml-2">Go Back</a>
                            <button type="submit" class="btn btn-primary mt-2 ml-2">Continue</button>
                        </div>
                    </div>
            </form>
        </div>
    </div>
    </div>
    <!-- Checkout Area -->
@endsection
@push('js')
    <script>
        $('#customCheck1').on('change', function(e) {
            e.preventDefault()
            if (this.checked) {
                $('#sfirst_name').val($('#first_name').val())
                $('#slast_name').val($('#last_name').val())
                $('#scompany').val($('#company').val())
                $('#semail').val($('#email_address').val())
                $('#scountry').val($('#country').val())
                $('#sphone').val($('#phone_number').val())
                $('#saddress').val($('#street_address').val())
                $('#sapartment_suite').val($('#apartment_suite').val())
                $('#scity').val($('#city').val())
                $('#sstate').val($('#street_address').val())
                $('#spostcode').val($('#postcode').val())
            }
        })
    </script>
@endpush
