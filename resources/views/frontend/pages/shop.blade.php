@extends('frontend.layout.forntmaster')
@section('title', 'shop')
@section('content')
    <!-- Begin Li's Breadcrumb Area -->
    <div class="breadcrumb-area">
        <div class="container">
            <div class="breadcrumb-content">
                <ul>
                    <li><a href="{{ route('front.index') }}">Home</a></li>
                    <li class="active">Shop Left Sidebar</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Li's Breadcrumb Area End Here -->
    <!-- Begin Li's Content Wraper Area -->
    <div class="content-wraper pt-60 pb-60 pt-sm-30">
        <div class="container">
            <form action="{{ route('shop.filter') }}" method="post" id="submit">
                @csrf
                <div class="row">
                    <div class="col-lg-9 order-1 order-lg-2">
                        <!-- Begin Li's Banner Area -->
                        @foreach ($banners as $banner)
                            <div class="single-banner shop-page-banner">
                                <a href="javascript:void(0)">
                                    <img src="{{ asset('image/banner/single-banner/' . $banner->photo) }}"
                                        alt="Li's Static Banner">
                                </a>
                            </div>
                        @endforeach
                        <!-- Li's Banner Area End Here -->
                        <!-- shop-top-bar start -->
                        <div class="shop-top-bar mt-30">
                            <div class="shop-bar-inner">


                            </div>
                            <!-- product-select-box start -->
                            <div class="product-select-box">
                                <div class="product-short">
                                    <p>Sort By:</p>
                                    <select class="nice-select" name="sortBy" id="sortBy" onchange="this.form.submit()">
                                        <option value="">Defult </option>
                                        <option value="high-price" @if (!empty($_GET['sortBy']) && $_GET['sortBy'] == 'high-price') selected @endif> Higer
                                            to
                                            Lower
                                        </option>
                                        <option value="low-high" @if (!empty($_GET['sortBy']) && $_GET['sortBy'] == 'low-high') selected @endif> Lower to
                                            Higer </option>
                                        <option value="new-to-old-product"
                                            @if (!empty($_GET['sortBy']) && $_GET['sortBy'] == 'new-to-old-product') selected @endif>New
                                            to Old</option>
                                        <option value="old-to-new-product"
                                            @if (!empty($_GET['sortBy']) && $_GET['sortBy'] == 'old-to-new-product') selected @endif>Old
                                            to New</option>
                                        <option value="alphabetical" @if (!empty($_GET['sortBy']) && $_GET['sortBy'] == 'alphabetical') selected @endif>
                                            Alphabetical</option>
                                    </select>
                                </div>
                            </div>
                            <!-- product-select-box end -->
                        </div>
                        <!-- shop-top-bar end -->
                        <!-- shop-products-wrapper start -->
                        <div class="shop-products-wrapper">
                            <div class="tab-content">
                                <div id="grid-view" class="tab-pane fade active show" role="tabpanel">
                                    <div class="product-area shop-product-area">
                                        <div class="row">
                                            @foreach ($products as $product)
                                                <div class="col-lg-4 col-md-4 col-sm-6 mt-40">
                                                    <!-- single-product-wrap start -->
                                                    <div class="single-product-wrap">
                                                        <div class="product-image">
                                                            <a href="{{ route('single.product', $product->slug) }}">
                                                                <img src="{{ asset('image/product/orginal/' . $product->photo) }}"
                                                                    alt="Li's Product Image">
                                                            </a>
                                                            <span class="sticker">New</span>
                                                        </div>
                                                        <div class="product_desc">
                                                            <div class="product_desc_info">
                                                                <div class="product-review">
                                                                    <h5 class="manufacturer">
                                                                        <a
                                                                            href="{{ route('product.brand', $product->brand->slug) }}">{{ $product->brand->name }}</a>
                                                                    </h5>
                                                                    <div class="rating-box">
                                                                        <ul class="rating">
                                                                            <li><i class="fa fa-star-o"></i></li>
                                                                            <li><i class="fa fa-star-o"></i></li>
                                                                            <li><i class="fa fa-star-o"></i></li>
                                                                            <li class="no-star"><i class="fa fa-star-o"></i>
                                                                            </li>
                                                                            <li class="no-star"><i class="fa fa-star-o"></i>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                <h4><a class="product_name"
                                                                        href="{{ route('single.product', $product->slug) }}">
                                                                        {{ $product->title }}
                                                                    </a>
                                                                </h4>
                                                                <div class="price-box">
                                                                    @if ($product->offer_price)
                                                                        <span
                                                                            class="new-price">${{ $product->offer_price }}</span>
                                                                        <span>
                                                                            <del class="text-danger">
                                                                                ${{ $product->price }}
                                                                            </del>
                                                                        </span>
                                                                    @else
                                                                        <span>
                                                                            ${{ $product->price }}
                                                                        </span>
                                                                    @endif

                                                                </div>
                                                            </div>
                                                            <div class="add-actions">
                                                                <ul class="add-actions-link">
                                                                    <li class="add-cart active">
                                                                        <a href="#" class="cart-id"
                                                                            data-id="{{ $product->id }}" quantity="1"
                                                                            id="add_to_cart_{{ $product->id }}">Add
                                                                            to
                                                                            cart</a>
                                                                    </li>
                                                                    <li>
                                                                        <a href="#" title="quick view"
                                                                            onclick="quickview({{ $product->id }})"
                                                                            class="quick-view-btn" data-toggle="modal"
                                                                            data-target="#quick_view_modal">
                                                                            <i class="fa fa-eye"></i></a>
                                                                    </li>
                                                                    <li>
                                                                        <a class="links-details wish-list"
                                                                            href="javascript:void(0)"
                                                                            data-id="{{ $product->id }}" quantity="1"
                                                                            id="move_to_cart_{{ $product->id }}"><i
                                                                                class="fa fa-heart-o"></i>
                                                                        </a>

                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- single-product-wrap end -->
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                @if (count($products) > 14)
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 pt-xs-15">
                                            <p> {{ $products }} </p>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            {{ $products->appends($_GET)->links('vendor.pagination.custom') }}
                                        </div>
                                    </div>
                                @endif

                            </div>
                        </div>
                        <!-- shop-products-wrapper end -->
                    </div>
                    <div class="col-lg-3 order-2 order-lg-1">
                        <!--sidebar-categores-box start  -->
                        <div class="sidebar-categores-box mt-sm-30 mt-xs-30">
                            <div class="sidebar-title">
                                <h2>Laptop</h2>
                            </div>
                            <!-- category-sub-menu start -->
                            <div class="category-sub-menu">
                                <ul>
                                    @foreach ($categories as $category)
                                        <li class="has-sub"><a href="# ">{{ $category->name }}</a>
                                            <ul>
                                                @foreach ($category->subcategory as $subcategory)
                                                    <li><a class="text-success" href="#">{{ $subcategory->name }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                            <!-- category-sub-menu end -->
                        </div>
                        <!--sidebar-categores-box end  -->
                        <!--sidebar-categores-box start  -->
                        <div class="sidebar-categores-box">
                            <div class="sidebar-title">
                                <h2>Filter By</h2>
                            </div>
                            <!-- btn-clear-all start -->
                            <button class="btn-clear-all mb-sm-30 mb-xs-30">Clear all</button>
                            <!-- btn-clear-all end -->
                            <!-- filter-sub-area start -->
                            <div class="filter-sub-area">
                                <h5 class="filter-sub-titel">Brand</h5>
                                <div class="categori-checkbox">
                                    <ul>
                                        @if (!empty($_GET['brands']))
                                            @php
                                                $brand_filter = explode(',', $_GET['brands']);
                                            @endphp
                                        @endif

                                        @foreach ($brands as $brand)
                                            <li>
                                                <input @if (!empty($brand_filter) && in_array($brand->slug, $brand_filter)) checked @endif name="brands[]"
                                                    onchange="this.form.submit()" value="{{ $brand->slug }}"
                                                    type="checkbox">
                                                <a href="#">{{ $brand->name }} ({{ count($brand->products) }})</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>

                            <!-- filter-sub-area end -->
                            <!-- filter-sub-area start -->
                            <div class="filter-sub-area pt-sm-10 pt-xs-10">
                                <h5 class="filter-sub-titel">Categories</h5>
                                <div class="categori-checkbox">
                                    <ul>
                                        @if (!empty($_GET['category']))
                                            @php
                                                $filter_cats = explode(',', $_GET['category']);
                                            @endphp
                                        @endif
                                        @foreach ($filter_category as $filterCategory)
                                            <li>
                                                <input type="checkbox" @if (!empty($filter_cats) && in_array($filterCategory->slug, $filter_cats)) checked @endif
                                                    id="{{ $filterCategory->slug }}" name="category[]"
                                                    onchange="this.form.submit()" value="{{ $filterCategory->slug }}">
                                                <a href="#">{{ ucfirst($filterCategory->name) }}
                                                    ({{ count($filterCategory->products) }})
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            <!-- filter-sub-area end -->
                            <!-- filter-sub-area start -->
                            <div class="filter-sub-area pt-sm-10 pt-xs-10">
                                <h5 class="filter-sub-titel">Size</h5>
                                <div class="size-checkbox">

                                    <ul>
                                        <input type="hidden" name="size_final" id="sizeFinal">
                                        <li>

                                            {{-- {{ dd($productSize) }} --}}
                                            <input @if (!empty($_GET['size']) && $_GET['size'] == 'S') checked @endif type="checkbox"
                                                onchange="problem('S') " name="size" value="S">
                                            <a href="#"> small
                                                (
                                                {{ App\Models\Product::where('status', 1)->where(['size' => 'S'])->count() }})
                                            </a>
                                        </li>
                                        <li> <input @if (!empty($_GET['size']) && $_GET['size'] == 'M') checked @endif type="checkbox"
                                                onchange="problem('M')" name="size" value="M"><a
                                                href="#">medium
                                                (
                                                {{ App\Models\Product::where('status', 1)->where(['size' => 'M'])->count() }})</a>
                                        </li>
                                        <li> <input @if (!empty($_GET['size']) && $_GET['size'] == 'L') checked @endif type="checkbox"
                                                onchange="problem('L')" name="size" value="L">
                                            <a href="#">large (
                                                {{ App\Models\Product::where('status', 1)->where(['size' => 'L'])->count() }})
                                            </a>
                                        </li>
                                        <li> <input @if (!empty($_GET['size']) && $_GET['size'] == 'XL') checked @endif type="checkbox"
                                                onchange="problem('XL')" name="size" value="XL"><a
                                                href="#">extra large (
                                                {{ App\Models\Product::where('status', 1)->where(['size' => 'XL'])->count() }})</a>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                            <!-- filter-sub-area end -->

                        </div>
                        <!--sidebar-categores-box end  -->


                    </div>
                </div>
            </form>
        </div>
    </div>

    <!-- Content Wraper Area End Here -->
@endsection
@push('js')
    <script>
        function problem(size) {
            $('#sizeFinal').val(size);
            $('#submit').submit();
        }
        $(document).on('click', '.cart-id', function(e) {
            e.preventDefault()
            let product_id = $(this).attr('data-id')
            let quantity = $(this).attr('quantity')
            let token = "{{ csrf_token() }}"
            let path = "{{ route('cart.store') }}"
            $.ajax({
                type: "POST",
                dataType: "JSON",
                url: path,
                data: {
                    product_id: product_id,
                    product_qty: quantity,
                    _token: token,
                },
                beforeSend: function() {
                    $('#add_to_cart_' + product_id).html(
                        ' <i class="fa fa-spinner fa-spin "></i> loading...')
                },
                complete: function() {
                    $('#add_to_cart_' + product_id).html('Add to Cart')
                },
                success: function(data) {
                    $('body #cart_count').html(data['cart_count']);
                    $('body .header-ajax').html(data['header']);

                    if (data.status == 'false') {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'warning',
                            toast: true,
                            title: data.msg,
                            showConfirmButton: false,
                            timer: 2000,
                        })
                        if (data.url) {
                            setTimeout(() => {
                                window.location.href = data.url;
                            }, 2500);
                        }
                    } else if (data) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            toast: true,
                            title: data['msg'],
                            showConfirmButton: false,
                            timer: 2000
                        })
                    }
                }
            })
        })
    </script>
@endpush
