<div class="content-wraper pt-60 pb-60 pt-sm-30">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 order-1 order-lg-2">
                <!-- Begin Li's Banner Area -->
                @foreach ($banners as $banner)
                    <div class="single-banner shop-page-banner">
                        <a href="#">
                            <img src="{{ asset('image/banner/single-banner/' . $banner->photo) }}"
                                alt="Li's Static Banner">
                        </a>
                    </div>
                @endforeach

                <!-- Li's Banner Area End Here -->
                <!-- shop-top-bar start -->
                <div class="shop-top-bar mt-30">
                    <div class="shop-bar-inner">
                        <div class="product-view-mode">
                            <!-- shop-item-filter-list start -->
                            <ul class="nav shop-item-filter-list" role="tablist">
                                <li class="active" role="presentation"><a aria-selected="true" class="active show"
                                        data-toggle="tab" role="tab" aria-controls="grid-view" href="#grid-view"><i
                                            class="fa fa-th"></i></a>
                                </li>
                                <li role="presentation"><a data-toggle="tab" role="tab" aria-controls="list-view"
                                        href="#list-view"><i class="fa fa-th-list"></i></a></li>
                            </ul>
                            <!-- shop-item-filter-list end -->
                        </div>
                        <div class="toolbar-amount">
                            <span>Showing 1 to 9 of 15</span>
                        </div>
                    </div>
                    <!-- product-select-box start -->
                    <div class="product-select-box">
                        <div class="product-short">
                            <p>Sort By:</p>
                            <select class="nice-select" id="sortBy">
                                <option value="trending">Defult</option>
                                <option value="high-price" @if (!empty($_GET['sort']) && $_GET['sort'] == 'high-price') selected @endif> Higer to
                                    Lower
                                </option>

                                <option value="low-high" @if (!empty($_GET['sort']) && $_GET['sort'] == 'low-high') selected @endif> Lower to
                                    Higer </option>
                                <option value="new-to-old-product" @if (!empty($_GET['sort']) && $_GET['sort'] == 'new-to-old-product') selected @endif>New
                                    to Old</option>
                                <option value="old-to-new-product" @if (!empty($_GET['sort']) && $_GET['sort'] == 'old-to-new-product') selected @endif>Old
                                    to New</option>
                                <option value="alphabetical" @if (!empty($_GET['sort']) && $_GET['sort'] == 'alphabetical') selected @endif>
                                    Alphabetical</option>
                            </select>
                        </div>
                    </div>
                    <!-- product-select-box end -->
                </div>
                <!-- shop-top-bar end -->
                <!-- shop-products-wrapper start -->
                <div class="shop-products-wrapper">
                    <div class="tab-content">
                        <div id="grid-view" class="tab-pane fade active show" role="tabpanel">
                            <div class="product-area shop-product-area">
                                <div class="row">
                                    @foreach ($products as $c_product)
                                        <div class="col-lg-4 col-md-4 col-sm-6 mt-40">
                                            <!-- single-product-wrap start -->
                                            <div class="single-product-wrap">
                                                <div class="product-image">
                                                    <a href="single-product.html">
                                                        <img width="100"
                                                            src="{{ asset('image/product/thumbnail/' . $c_product->photo) }}"
                                                            alt="{{ $c_product->title }}">
                                                    </a>
                                                    <span class="sticker">New</span>
                                                </div>
                                                <div class="product_desc">
                                                    <div class="product_desc_info">
                                                        <div class="product-review">
                                                            <h5 class="manufacturer">
                                                                <a
                                                                    href="product-details.html">{{ $c_product->category->name }}</a>
                                                            </h5>
                                                            <div class="rating-box">
                                                                <ul class="rating">
                                                                    <li><i class="fa fa-star-o"></i></li>
                                                                    <li><i class="fa fa-star-o"></i></li>
                                                                    <li><i class="fa fa-star-o"></i></li>
                                                                    <li class="no-star"><i class="fa fa-star-o"></i>
                                                                    </li>
                                                                    <li class="no-star"><i class="fa fa-star-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <h4><a class="product_name"
                                                                href="single-product.html">{{ $c_product->title }}</a>
                                                        </h4>
                                                        <div class="price-box">
                                                            @if ($c_product->discount)
                                                                <p class="new-price mb-1">
                                                                    <strong>Offer Price: </strong>
                                                                    ${{ $c_product->offer_price }}
                                                                </p>
                                                                <span class="new-price text-danger text-end">
                                                                    <strong>Discount : </strong>
                                                                    {{ $c_product->discount }}%
                                                                </span>
                                                            @else
                                                                <span class="new-price">
                                                                    ${{ $c_product->price }}
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                    <div class="add-actions">
                                                        <ul class="add-actions-link">
                                                            <li class="add-cart active">
                                                                <a href="/" data-id="{{ $c_product->id }}"
                                                                    data-quantity="1" class="cart-id"
                                                                    id="cart_id_{{ $c_product->id }}">Add
                                                                    to cart</a>
                                                            </li>
                                                            <li><a href="javascript:void(0)" title="quick view"
                                                                    class="quick-view-btn" onclick="quick_view()"><i
                                                                        class="fa fa-eye"></i></a></li>
                                                            <li><a class="links-details" href="wishlist.html"><i
                                                                        class="fa fa-heart-o"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- single-product-wrap end -->
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        {{ $products->links('vendor.pagination.custom') }}
                    </div>
                </div>
                <!-- shop-products-wrapper end -->
            </div>


            <div class="col-lg-3 order-2 order-lg-1">
                <!--sidebar-categores-box start  -->
                <div class="sidebar-categores-box mt-sm-30 mt-xs-30">
                    <div class="sidebar-title">
                        <h2>Laptop</h2>
                    </div>
                    <!-- category-sub-menu start -->
                    <div class="category-sub-menu">
                        <ul>
                            <li class="has-sub"><a href="# ">Prime Video</a>
                                <ul>
                                    <li><a href="#">All Videos</a></li>
                                    <li><a href="#">Blouses</a></li>

                                </ul>
                            </li>
                            <li class="has-sub"><a href="#">Computer</a>
                                <ul>
                                    <li><a href="#">TV & Video</a></li>
                                    <li><a href="#">Audio & Theater</a></li>
                                    <li><a href="#">Camera, Photo</a></li>

                                </ul>
                            </li>
                            <li class="has-sub"><a href="#">Electronics</a>
                                <ul>
                                    <li><a href="#">Amazon Home</a></li>
                                    <li><a href="#">Kitchen & Dining</a></li>
                                    <li><a href="#">Bed & Bath</a></li>
                                    <li><a href="#">Appliances</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- category-sub-menu end -->
                </div>
                <!--sidebar-categores-box end  -->
                <!--sidebar-categores-box start  -->
                <div class="sidebar-categores-box">
                    <div class="sidebar-title">
                        <h2>Filter By</h2>
                    </div>
                    <!-- btn-clear-all start -->
                    <form action="{{ route('shop') }}" method="GET">
                        <button class="btn-clear-all mb-sm-30 mb-xs-30" type="submit">Clear all</button>
                    </form>
                    <!-- btn-clear-all end -->
                    <!-- filter-sub-area start -->
                    <div class="filter-sub-area">
                        <h5 class="filter-sub-titel">Brand</h5>
                        <div class="categori-checkbox">
                            <form action="#">
                                <ul>
                                    @foreach ($brands as $brand)
                                        <li><input type="checkbox" name="product-categori"><a
                                                href="#">{{ $brand->name }}(13)</a></li>
                                    @endforeach

                                </ul>
                            </form>
                        </div>
                    </div>
                    <!-- filter-sub-area end -->
                    <!-- filter-sub-area start -->
                    {{-- <div class="filter-sub-area pt-sm-10 pt-xs-10">
                            <h5 class="filter-sub-titel">Categories</h5>
                            <div class="categori-checkbox">
                                <form action="#">
                                    <ul>
                                        @foreach ($side_categories as $category)
                                            <li>
                                                <input type="checkbox" name="product-categori"><a
                                                    href="#">{{ $category->name }}</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </form>
                            </div>
                        </div> --}}
                    <!-- filter-sub-area end -->
                    <!-- filter-sub-area start -->
                    <div class="filter-sub-area pt-sm-10 pt-xs-10">
                        <h5 class="filter-sub-titel">Size</h5>
                        <div class="size-checkbox">
                            <form action="#">
                                <ul>
                                    <li><input type="checkbox" name="product-size"><a href="#">S
                                            (3)</a>
                                    </li>
                                    <li><input type="checkbox" name="product-size"><a href="#">M
                                            (3)</a>
                                    </li>
                                    <li><input type="checkbox" name="product-size"><a href="#">L
                                            (3)</a>
                                    </li>
                                    <li><input type="checkbox" name="product-size"><a href="#">XL
                                            (3)</a></li>
                                </ul>
                            </form>
                        </div>
                    </div>
                    <!-- filter-sub-area end -->

                </div>
                <!--sidebar-categores-box end  -->


            </div>
        </div>
    </div>
</div>

@push('js')
    <script>
        $('#sortBy').on('change', function() {
            let sort = $(this).val()
            window.location = "{{ url('' . $route . '') }}/{{ $categories->slug }}?sort=" + sort
        })

        $(document).on('click', '.cart-id', function(e) {
            e.preventDefault();

            let product_quantity = $(this).attr('data-quantity')
            let product_data_id = $(this).attr('data-id')
            let token = "{{ csrf_token() }}"
            let path = "{{ route('cart.store') }}"
            $.ajax({
                type: 'POST',
                url: path,
                dataType: "JSON",
                data: {
                    _token: token,
                    product_id: product_data_id,
                    qty: product_quantity,
                },
                beforeSend: function() {
                    $('#cart_id_' + product_data_id).html(
                        ' <i class="fa fa-spinner  fa-spin  "></i> loading...')
                },
                complete: function() {
                    $('#cart_id_' + product_data_id).html(
                        'Add to cart')
                },

                success: function(data) {
                    console.log(data)
                    $('body .header-ajax').html(data['header']);
                }
            })

        })
    </script>
@endpush
