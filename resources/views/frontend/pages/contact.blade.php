@extends('frontend.layout.forntmaster')
@section('title', 'Limupa | Contact us')
@section('content')
    <div class="breadcrumb-area">
        <div class="container">
            <div class="breadcrumb-content">
                <ul>
                    <li><a href="{{ route('front.index') }}">Home</a></li>
                    <li class="active">Contact</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Li's Breadcrumb Area End Here -->
    <!-- Begin Contact Main Page Area -->
    <div class="container mt-5 pt-5">
        <div class="contact-main-page  mb-40 mb-md-40 mb-sm-40 mb-xs-40">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 offset-lg-1 col-md-12 order-1 order-lg-2">
                        <div class="contact-page-side-content">
                            <h3 class="contact-page-title">Contact Us</h3>
                            <p class="contact-page-message mb-25">Claritas est etiam processus dynamicus, qui sequitur
                                mutationem consuetudium lectorum. Mirum est notare quam littera gothica, quam nunc putamus
                                parum
                                claram anteposuerit litterarum formas human.</p>
                            <div class="single-contact-block">
                                <h4><i class="fa fa-fax"></i> Address</h4>
                                <p>123 Main Street, Anytown, CA 12345 – USA</p>
                            </div>
                            <div class="single-contact-block">
                                <h4><i class="fa fa-phone"></i> Phone</h4>
                                <p>Mobile: (08){{ $setting->phone }}</p>
                            </div>
                            <div class="single-contact-block last-child">
                                <h4><i class="fa fa-envelope-o"></i> Email</h4>
                                <p>{{ $setting->email }}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-12 order-2 order-lg-1">
                        <div class="contact-form-content pt-sm-55 pt-xs-55">
                            <h3 class="contact-page-title">Tell Us Your Message</h3>

                            @if (Session::has('msg'))
                                <div class="alert alert-primary" role="alert">
                                    <strong>{{ session::get('msg') }}</strong>
                                </div>
                            @endif


                            <div class="contact-form">
                                <form action="{{ route('contact.store') }}" method="post">
                                    @csrf
                                    <div class="form-group">
                                        <label>Your Name <span class="required">*</span></label>
                                        <input type="text" value="{{ old('name') }}" name="name" id="customername"
                                            required>
                                        @error('name')
                                            <p class="text-danger position-absolute">
                                                <small>{{ $message }}</small>
                                            </p>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label>Your Email <span class="required">*</span></label>
                                        <input type="email" name="email" value="{{ old('email') }}" id="customerEmail"
                                            required>
                                        @error('email')
                                            <p class="text-danger position-absolute">
                                                <small>{{ $message }}</small>
                                            </p>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <label>Subject</label>
                                        <input type="text" name="subject" value="{{ old('subject') }}"
                                            id="contactSubject">
                                        @error('subject')
                                            <p class="text-danger position-absolute">
                                                <small>{{ $message }}</small>
                                            </p>
                                        @enderror
                                    </div>
                                    <div class="form-group mb-30">
                                        <label>Your Message</label>
                                        <textarea name="message" value="{{ old('message') }}" id="contactMessage"></textarea>
                                        @error('message')
                                            <p class="text-danger position-absolute">
                                                <small>{{ $message }}</small>
                                            </p>
                                        @enderror

                                    </div>
                                    <button type="submit" value="submit" id="submit" class="li-btn-3"
                                        type="submit">send</button>
                                </form>
                            </div>
                            <p class="form-messege"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Contact Main Page Area End Here -->
@endsection
