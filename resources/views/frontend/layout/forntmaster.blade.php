<!doctype html>
<html class="no-js" lang="zxx">

<!-- index-331:38-->

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title> @yield('title') </title>
    @include('frontend.includes.head')
</head>

<body>
    <!--[if lt IE 8]>
  <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
 <![endif]-->
    <!-- Begin Body Wrapper -->
    <div class="body-wrapper">
        <!-- Begin Header Area -->
        <!--  Header  Area -->
        <header class="header-ajax" id="header_ajax">
            @include('frontend.includes.topbar')
            <!-- Header  Area End Here -->
            <!-- Begin Mobile Menu Area -->
            {{-- @include('frontend.includes.mobilemenu') --}}
            <!-- Mobile Menu Area End Here -->
        </header>
        <!-- Header Area End Here -->
        <!-- Begin Slider With Category Menu Area -->
        @yield('content')

        @include('frontend.modal')
        <!--  Footer Area -->
        <div class="footer">
            @include('frontend.includes.footer')
        </div>
        <!-- Footer Area End Here -->


    </div>
    <!-- Body Wrapper End Here -->
    <!-- jQuery-V1.12.4 -->
    @include('frontend.includes.script')
    @stack('js')
    <script>
        // Quick view
        function quickview(id) {
            $('#quick_view_modal').show()
            $('#modal_body').html(null)
            $.post("{{ route('quick.view') }}", {
                _token: '{{ csrf_token() }}',
                id: id
            }, function(data) {
                $('#modal_body').html(data)
            })

        }
        //cart Store section
        $(document).on('click', '.cart-id', function(e) {
            e.preventDefault();
            let product_data_id = $(this).attr('data-id')
            let product_quantity = $(this).attr('quantity')

            let token = "{{ csrf_token() }}"
            let path = "{{ route('cart.store') }}"
            $.ajax({
                url: path,
                type: "POST",
                dataType: "JSON",
                data: {
                    product_id: product_data_id,
                    product_qty: product_quantity,
                    _token: token,
                },
                beforeSend: function() {
                    $('#add_cart_id_' + product_data_id).html(
                        ' <i class="fa fa-spinner  fa-spin  "></i>  loading...')
                },
                complete: function() {
                    $('#add_cart_id_' + product_data_id).html('Add to Cart')
                },
                success: function(data) {
                    $('body #cart_count').html(data['cart_count']);
                    $('body .header-ajax').html(data['header']);

                    if (data.status == 'false') {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'warning',
                            toast: true,
                            title: data.msg,
                            showConfirmButton: false,
                            timer: 2000,
                        })
                        if (data.url) {
                            setTimeout(() => {
                                window.location.href = data.url;
                            }, 2500);
                        }
                    } else if (data) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            toast: true,
                            title: data['msg'],
                            showConfirmButton: false,
                            timer: 2000
                        })
                    }

                }
            })
        })
        // Cart Delete
        $(document).on('click', '.cart-delete', function(e) {
            e.preventDefault()
            let cart_id = $(this).attr('id')
            let token = "{{ csrf_token() }}"
            let route = "{{ route('cart.delete') }}"
            $.ajax({
                url: route,
                type: 'post',
                dataType: 'json',
                data: {
                    _token: token,
                    cart_id: cart_id,
                },
                success: function(data) {
                    $('body .header-ajax').html(data['header']);
                    if (data['status']) {
                        Swal.fire({
                            position: 'top-end',
                            icon: 'warning',
                            toast: true,
                            title: data['msg'],
                            showConfirmButton: false,
                            timer: 1500
                        })
                    }
                }
            })

        })

        // Search Section
        $(document).ready(function() {
            let path = "{{ route('auto.search') }}";
            $("#search_text").autocomplete({
                source: function(request, response) {
                    $.ajax({
                        url: path,
                        dataType: "JSON",
                        data: {
                            term: request.term
                        },
                        success: function(data) {
                            response(data);
                        }
                    });
                }
            });
        })

        //Wishlist Section
        $(document).on('click', '.wish-list', function(e) {
            e.preventDefault();
            let product_id = $(this).attr('data-id')

            let product_quantity = $(this).attr('quantity')

            let token = "{{ csrf_token() }}"
            let path = "{{ route('wish.store') }}"
            $.ajax({
                url: path,
                type: "POST",
                data: {
                    product_id: product_id,
                    product_qty: product_quantity,
                    _token: token,
                },
                beforeSend: function() {
                    $('#move_to_cart_' + product_id).html(
                        ' <i class="fa fa-spinner  fa-spin  "></i>')
                },
                complete: function() {
                    $('#move_to_cart_' + product_id).html(' <i class="fa fa-heart-o "></i>')
                },
                success: function(data) {
                    if (data.status) {
                        $('body #cart_count').html(data['cart_count']);
                        $('body #wishlist_counter').html(data['wishlist_count']);
                        $('body .header-ajax').html(data['header']);

                        Swal.fire({
                            title: data.msg,
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'OK!'
                        })
                    } else if (data['present']) {
                        $('body #cart_count').html(data['cart_count']);
                        $('body .header-ajax').html(data['header']);
                        Swal.fire({
                            title: data['msg'],
                            icon: 'warning',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'OK'
                        })
                    } else {
                        $('body #cart_count').html(data['cart_count']);
                        $('body .header-ajax').html(data['header']);
                        Swal.fire({
                            title: data['msg'],
                            icon: 'success',
                            showCancelButton: false,
                            confirmButtonColor: '#3085d6',
                            cancelButtonColor: '#d33',
                            confirmButtonText: 'OK!'
                        })
                    }
                }
            })
        })
    </script>
</body>

<!-- index-331:41-->

</html>
