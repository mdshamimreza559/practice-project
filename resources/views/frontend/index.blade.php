@extends('frontend.layout.forntmaster')
@section('content')
    <div class="slider-with-banner">
        <div class="container">
            <div class="row">
                <!-- Begin Category Menu Area -->
                <div class="col-lg-3">
                    <!--Category Menu Start-->
                    <div class="category-menu category-menu-2">
                        <div class="category-heading">
                            <h2 class="categories-toggle"><span>categories</span></h2>
                        </div>
                        <div id="cate-toggle" class="category-menu-list">
                            <ul>

                                @foreach ($categories as $category)
                                    <li class="rx-child"><a
                                            href="{{ route('front.categories', $category->slug) }}">{{ $category->name }}
                                            @if ($category->subcategory->count() > 0)
                                                <i class="fas fa-angle-right    "></i>
                                            @endif

                                        </a>
                                        @if (count($category->subcategory) > 0)
                                            <ul class="cat-mega-menu">
                                                <li class="right-menu cat-mega-title">
                                                    @foreach ($category->subcategory as $subcategory)
                                                        {{-- <a href="shop-left-sidebar.html">Prime Video</a> --}}
                                                        <ul>
                                                            <li><a href="#">{{ $subcategory->name }}</a></li>
                                                        </ul>
                                                    @endforeach
                                                </li>
                                            </ul>
                                        @endif


                                    </li>
                                @endforeach
                                <li class="rx-parent">
                                    <a class="rx-default">More Categories</a>
                                    <a class="rx-show">Less Categories</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!--Category Menu End-->
                </div>
                <!-- Category Menu Area End Here -->
                <!-- Begin Slider Area -->
                <div class="col-lg-6 col-md-8">
                    <div class="slider-area slider-area-3 pt-sm-30 pt-xs-30 pb-xs-30">
                        <div class="slider-active owl-carousel">
                            <!-- Begin Single Slide Area -->
                            @foreach ($banners as $banner)
                                <div style="background-image: url('{{ asset('image/banner/' . $banner->photo) }}')"
                                    class="single-slide align-center-left animation-style-01 bg-7">

                                    <div class="slider-progress"></div>
                                    <div class="slider-content">
                                        <h5>{{ $banner->short_description }}</h5>
                                        <h2>{{ $banner->name }}</h2>
                                        <h3>{{ $banner->description }} <span>${{ $banner->price }}</span></h3>
                                        <div class="default-btn slide-btn">
                                            <a class="links" href="shop-left-sidebar.html">Shopping Now</a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            <!-- Single Slide Area End Here -->
                        </div>
                    </div>
                </div>
                <!-- Slider Area End Here -->
                <!-- Begin Li Banner Area -->

                <div class="col-lg-3 col-md-4 text-center pt-sm-30">
                    @foreach ($list_banner as $l_banner)
                        <div class="li-banner mb-3 ">
                            <a href="">
                                <img src="{{ asset('image/banner/list-banner/' . $l_banner->photo) }}"
                                    alt="{{ $l_banner->title }}">
                            </a>
                        </div>
                    @endforeach
                </div>
                <!-- Li Banner Area End Here -->
            </div>
        </div>
    </div>
    <!-- Slider With Category Menu Area End Here -->
    <!-- Begin Li's Static Banner Area -->
    <div class="li-static-banner pt-20 pt-sm-30">
        <div class="container">
            <div class="row">
                <!-- Begin Single Banner Area -->
                @foreach ($s_list_banner as $s_banner)
                    <div class="col-lg-4 col-md-4 text-center">
                        <div class="single-banner pb-xs-30">
                            <a href="#">
                                <img src="{{ asset('image/banner/list-banner/' . $s_banner->photo) }}"
                                    alt="{{ $s_banner->name }}">
                            </a>
                        </div>
                    </div>
                @endforeach

                <!-- Single Banner Area End Here -->

            </div>
        </div>
    </div>
    <!-- Li's Static Banner Area End Here -->

    <!-- New Product Product Area -->
    <section class="product-area li-laptop-product Special-product pt-60 pb-45">
        <div class="container">
            <div class="row">
                <!-- Begin Li's Section Area -->
                <div class="col-lg-12">
                    <div class="li-section-title">
                        <h2>
                            <span>NEW PRODUCTS</span>
                        </h2>
                    </div>
                    <div class="row">
                        <div class="special-product-active owl-carousel">
                            @foreach ($new_products as $new_product)
                                <div class="col-lg-12">
                                    <!-- single-product-wrap start -->
                                    <div class="single-product-wrap">
                                        <div class="product-image">
                                            <a href="single-product.html">
                                                <img src="{{ asset('image/product/thumbnail/' . $new_product->photo) }}"
                                                    alt="{{ $new_product->title }}">
                                            </a>
                                            <span class="sticker">New</span>
                                        </div>
                                        <div class="product_desc">
                                            <div class="product_desc_info">
                                                <div class="product-review">
                                                    <h5 class="manufacturer">
                                                        <a
                                                            href="{{ route('front.categories', $new_product->category->slug) }}">{{ $new_product->category->name }}</a>
                                                    </h5>
                                                </div>
                                                <h4><a class="product_name"
                                                        href="{{ route('single.product', $new_product->slug) }}">{{ $new_product->title }}</a>
                                                </h4>
                                                <div class="price-box">
                                                    <span class="new-price">${{ $new_product->offer_price }}</span>
                                                </div>

                                            </div>
                                            <div class="add-actions">
                                                <ul class="add-actions-link">
                                                    <li class="add-cart active"><a href="#" class="cart-id"
                                                            data-id="{{ $new_product->id }}" quantity="1"
                                                            id="add_cart_id_{{ $new_product->id }}">Add to cart</a>
                                                    </li>

                                                    <li>
                                                        <a class="links-details wish-list" href="javascript:void(0)"
                                                            data-id="{{ $new_product->id }}" quantity="1"
                                                            id="move_to_cart_{{ $new_product->id }}"><i
                                                                class="fa fa-heart-o"></i>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a class="quick-view" data-toggle="modal"
                                                            onclick="quickview({{ $new_product->id }})"
                                                            data-target="#quick_view_modal" href="#"><i
                                                                class="fa fa-eye"></i></a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- single-product-wrap end -->
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <!-- Li's Section Area End Here -->
            </div>
        </div>
    </section>
    <!-- Li's Special Product Area End Here -->

    <!-- Begin Featured Product With Banner Area -->
    <div class="featured-pro-with-banner mt-sm-5 pb-sm-10 mt-xs-5 pb-xs-10">
        <div class="container">
            <div class="row">
                <!-- Begin Li's Featured Banner Area -->
                <div class="col-lg-3 text-center">
                    @foreach ($feature_product_banner as $f_banner)
                        <div class="single-banner featured-banner">
                            <a href="#">
                                <img class="img-thumbnail" height="100"
                                    src="{{ asset('image/banner/feature-banner/' . $f_banner->photo) }}"
                                    alt="{{ $f_banner->title }}">
                            </a>
                        </div>
                    @endforeach

                </div>
                <!-- Li's Featured Banner Area End Here -->
                <!-- Begin Featured Product Area -->
                <div class="col-lg-9">
                    <div class="featured-product pt-sm-30 pt-xs-30">
                        <div class="li-section-title">
                            <h2>
                                <span>Featured Products</span>
                            </h2>
                        </div>
                        <div class="row">
                            <div class="featured-product-bundle">
                                <div class="row">
                                    @foreach ($feature_product as $f_product)
                                        <div class="col-lg-6">
                                            <div class="featured-pro-wrapper mb-30 mb-sm-25">
                                                <div class="product-img">
                                                    <a href="{{ route('single.product', $f_product->slug) }}">
                                                        <img width="100" alt="{{ $f_product->title }}"
                                                            src="{{ asset('image/product/thumbnail/' . $f_product->photo) }}">
                                                    </a>
                                                </div>
                                                <div class="featured-pro-content">
                                                    <div class="product-review">
                                                        <h5 class="manufacturer">

                                                            <a
                                                                href="{{ route('front.categories', $f_product->category->slug) }}">
                                                                {{ $f_product->category->name }}
                                                            </a>
                                                        </h5>
                                                    </div>
                                                    <div class="rating-box">
                                                        <ul class="rating">
                                                            <li><i class="fa fa-star-o"></i></li>
                                                            <li><i class="fa fa-star-o"></i></li>
                                                            <li><i class="fa fa-star-o"></i></li>
                                                            <li class="no-star"><i class="fa fa-star-o"></i></li>
                                                            <li class="no-star"><i class="fa fa-star-o"></i></li>
                                                        </ul>
                                                    </div>
                                                    <h4><a class="featured-product-name"
                                                            href="{{ route('single.product', $f_product->slug) }}">{{ $f_product->title }}</a>
                                                    </h4>
                                                    <div class="featured-price-box">
                                                        @if ($f_product->discount)
                                                            <span
                                                                class="new-price new-price-2">${{ $f_product->offer_price }}
                                                            </span>
                                                            <span
                                                                class="old-price text-warning"><del>${{ $f_product->price }}</del>
                                                            </span>
                                                            <span class="discount-percentage">{{ $f_product->discount }}%
                                                            </span>
                                                        @else
                                                            <span class="old-price">${{ $f_product->price }}</span>
                                                        @endif
                                                    </div>
                                                    <div class="featured-product-action">
                                                        <ul class="add-actions-link">
                                                            <li class="add-cart active"><a href="#" class="cart-id"
                                                                    data-id="{{ $f_product->id }}" quantity="1"
                                                                    id="add_cart_id_{{ $f_product->id }}">Add to cart</a>
                                                            </li>
                                                            <li>

                                                                <a class="links-details wish-list"
                                                                    href="javascript:void(0)"
                                                                    data-id="{{ $f_product->id }}" quantity="1"
                                                                    id="move_to_cart_{{ $f_product->id }}"><i
                                                                        class="fa fa-heart-o"></i>
                                                                </a>

                                                            </li>
                                                            <li> <a class="quick-view" data-toggle="modal"
                                                                    onclick="quickview({{ $f_product->id }})"
                                                                    data-target="#quick_view_modal" href="#"><i
                                                                        class="fa fa-eye"></i></a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Featured Product Area End Here -->
            </div>
        </div>
    </div>
    <!-- Featured Product With Banner Area End Here -->

    <!-- Begin Li's Trending Product Area -->
    <section class="product-area li-trending-product pt-60 pb-45 pt-xs-50">
        <div class="container">
            <div class="row">
                <!-- Begin Li's Tab Menu Area -->
                <div class="col-lg-12">
                    <div class="li-product-tab li-trending-product-tab">
                        <ul class="nav li-product-menu li-trending-product-menu">
                            <li><a class="active" href="javascript:void(0)">
                                    <h2>Trendding Products</h2>
                                </a></li>
                        </ul>
                    </div>
                    <!-- Begin Li's Tab Menu Content Area -->
                    <div class="tab-content li-tab-content li-trending-product-content">
                        <div id="home1" class="tab-pane show fade in active">
                            <div class="row">
                                <div class="product-active owl-carousel">
                                    @foreach ($trendding_products as $t_product)
                                        <div class="col-lg-12">
                                            <!-- single-product-wrap start -->
                                            <div class="single-product-wrap">
                                                <div class="product-image">
                                                    <a href="single-product.html">
                                                        <img src="{{ asset('image/product/thumbnail/' . $t_product->photo) }}"
                                                            alt="{{ $t_product->title }}">
                                                    </a>
                                                    <span class="sticker">New</span>
                                                </div>
                                                <div class="product_desc">
                                                    <div class="product_desc_info">
                                                        <div class="product-review">
                                                            <h5 class="manufacturer">
                                                                <a
                                                                    href="{{ route('front.categories', $t_product->category->slug) }}">{{ $t_product->category->name }}</a>
                                                            </h5>
                                                            <div class="rating-box">
                                                                <ul class="rating">
                                                                    <li><i class="fa fa-star-o"></i></li>
                                                                    <li><i class="fa fa-star-o"></i></li>
                                                                    <li><i class="fa fa-star-o"></i></li>
                                                                    <li class="no-star"><i class="fa fa-star-o"></i>
                                                                    </li>
                                                                    <li class="no-star"><i class="fa fa-star-o"></i>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <h4><a class="product_name"
                                                                href="{{ route('single.product', $t_product->slug) }}">{{ $t_product->title }}</a>
                                                        </h4>
                                                        <div class="price-box">
                                                            @if ($t_product->discount)
                                                                <span
                                                                    class="new-price">${{ $t_product->offer_price }}</span>
                                                            @else
                                                                <span class="new-price">${{ $t_product->price }}</span>
                                                            @endif

                                                        </div>
                                                    </div>
                                                    <div class="add-actions">
                                                        <ul class="add-actions-link">
                                                            <li class="add-cart active">
                                                                <a href="#" class="cart-id"
                                                                    data-id="{{ $t_product->id }}" quantity="1"
                                                                    id="add_cart_id_{{ $t_product->id }}">Add
                                                                    to cart
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a class="links-details wish-list"
                                                                    href="javascript:void(0)"
                                                                    data-id="{{ $t_product->id }}" quantity="1"
                                                                    id="move_to_cart_{{ $t_product->id }}"><i
                                                                        class="fa fa-heart-o"></i>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a class="quick-view" data-toggle="modal"
                                                                    onclick="quickview({{ $t_product->id }})"
                                                                    data-target="#quick_view_modal" href="#"><i
                                                                        class="fa fa-eye"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- single-product-wrap end -->
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- Tab Menu Content Area End Here -->
                </div>
                <!-- Tab Menu Area End Here -->
            </div>
        </div>
    </section>
    <!-- Li's Trending Product Area End Here -->

    {{--  best selling products --}}
    <section class="product-area li-trending-product li-trending-product-2 pt-60 pb-45">
        <div class="container">
            <div class="row">
                <!-- Begin Li's Tab Menu Area -->
                <div class="col-lg-12">
                    <div class="li-product-tab li-trending-product-tab">
                        <ul class="nav li-product-menu li-trending-product-menu">
                            <li><a class="active" data-toggle="tab" href="#home1"><span>Best Selling</span></a></li>
                            <li><a data-toggle="tab" href="#home2"><span>Top Reated</span></a></li>

                        </ul>
                    </div>
                    <!-- Begin Li's Tab Menu Content Area -->
                    <div class="tab-content li-tab-content li-trending-product-content">
                        <div id="home1" class="tab-pane show fade in active">
                            <div class="row">
                                <div class="product-active owl-carousel">
                                    @foreach ($best_selling as $best)
                                        <div class="col-lg-12">
                                            <!-- single-product-wrap start -->
                                            <div class="single-product-wrap">
                                                <div class="product-image">
                                                    <a href="{{ route('single.product', $best->slug) }}">
                                                        <img src="{{ asset('image/product/thumbnail/' . $best->photo) }}"
                                                            alt="{{ $best->title }}">
                                                    </a>
                                                    <span class="sticker">New</span>
                                                </div>
                                                <div class="product_desc">
                                                    <div class="product_desc_info">
                                                        <div class="product-review">
                                                            <h5 class="manufacturer">
                                                                <a
                                                                    href="{{ route('front.categories', $best->category->slug) }}">{{ $best->category->name }}</a>
                                                            </h5>
                                                            <div class="rating-box">
                                                                <ul class="rating">
                                                                    <li><i class="fa fa-star-o"></i></li>
                                                                    <li><i class="fa fa-star-o"></i></li>
                                                                    <li><i class="fa fa-star-o"></i></li>
                                                                    <li class="no-star"><i class="fa fa-star-o"></i></li>
                                                                    <li class="no-star"><i class="fa fa-star-o"></i></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <h4><a class="product_name"
                                                                href="{{ route('single.product', $best->slug) }}">{{ $best->title }}</a>
                                                        </h4>
                                                        <div class="price-box">
                                                            <span
                                                                class="new-price">${{ number_format($best->price, 2) }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="add-actions">
                                                        <ul class="add-actions-link">
                                                            <li class="add-cart active">
                                                                <a href="#" class="cart-id"
                                                                    data-id="{{ $best->id }}" quantity="1"
                                                                    id="add_cart_id_{{ $best->id }}">Add
                                                                    to cart
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a class="links-details wish-list"
                                                                    href="javascript:void(0)"
                                                                    data-id="{{ $best->id }}" quantity="1"
                                                                    id="move_to_cart_{{ $best->id }}"><i
                                                                        class="fa fa-heart-o"></i>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a class="quick-view" data-toggle="modal"
                                                                    onclick="quickview({{ $best->id }})"
                                                                    data-target="#quick_view_modal" href="#"><i
                                                                        class="fa fa-eye"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- single-product-wrap end -->
                                        </div>
                                    @endforeach


                                </div>
                            </div>
                        </div>
                        <div id="home2" class="tab-pane fade">
                            <div class="row">
                                <div class="product-active owl-carousel">
                                    @foreach ($top_rates as $top_product)
                                        <div class="col-lg-12">
                                            <!-- single-product-wrap start -->
                                            <div class="single-product-wrap">
                                                <div class="product-image">
                                                    <a href="{{ route('single.product', $top_product->slug) }}">
                                                        <img src="{{ asset('image/product/thumbnail/' . $top_product->photo) }}"
                                                            alt="Li's Product Image">
                                                    </a>
                                                    <span class="sticker">New</span>
                                                </div>
                                                <div class="product_desc">
                                                    <div class="product_desc_info">
                                                        <div class="product-review">
                                                            <h5 class="manufacturer">
                                                                <a
                                                                    href="{{ route('front.categories', $top_product->category->slug) }}">{{ $top_product->category->name }}</a>
                                                            </h5>
                                                            <div class="rating-box">
                                                                <ul class="rating">
                                                                    <li><i class="fa fa-star-o"></i></li>
                                                                    <li><i class="fa fa-star-o"></i></li>
                                                                    <li><i class="fa fa-star-o"></i></li>
                                                                    <li class="no-star"><i class="fa fa-star-o"></i></li>
                                                                    <li class="no-star"><i class="fa fa-star-o"></i></li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                        <h4><a class="product_name"
                                                                href="{{ route('single.product', $top_product->slug) }}">{{ $top_product->title }}</a>
                                                        </h4>
                                                        <div class="price-box">
                                                            <span
                                                                class="new-price">${{ number_format($top_product->price, 2) }}</span>
                                                        </div>
                                                    </div>
                                                    <div class="add-actions">
                                                        <ul class="add-actions-link">
                                                            <li class="add-cart active">
                                                                <a href="#" class="cart-id"
                                                                    data-id="{{ $top_product->id }}" quantity="1"
                                                                    id="add_cart_id_{{ $top_product->id }}">Add
                                                                    to cart
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a class="links-details wish-list"
                                                                    href="javascript:void(0)"
                                                                    data-id="{{ $top_product->id }}" quantity="1"
                                                                    id="move_to_cart_{{ $top_product->id }}"><i
                                                                        class="fa fa-heart-o"></i>
                                                                </a>
                                                            </li>
                                                            <li>
                                                                <a class="quick-view" data-toggle="modal"
                                                                    onclick="quickview({{ $top_product->id }})"
                                                                    data-target="#quick_view_modal" href="#"><i
                                                                        class="fa fa-eye"></i></a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- single-product-wrap end -->
                                        </div>
                                    @endforeach


                                </div>
                            </div>
                        </div>

                    </div>
                    <!-- Tab Menu Content Area End Here -->
                </div>
                <!-- Tab Menu Area End Here -->
            </div>
        </div>
    </section>
@endsection


@if (Session::has('msg'))
    @push('js')
        <script>
            Swal.fire({
                position: 'top-end',
                toast: true,
                icon: '<?php echo session('cls'); ?>',
                title: '<?php echo session('msg'); ?>',
                showConfirmButton: false,
                timer: 2000
            })
        </script>
    @endpush
@endif
@push('js')
    <script>
        function myfunction(a, b, c) {
            let value = [a, b, c]
            console.log(value)
            // for (let index = 0; index < value.length; index++) {
            //     console.log(index)
            // }
        }

        myfunction('a', 2, 3.5)
    </script>
@endpush
