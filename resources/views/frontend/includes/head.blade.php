<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Favicon -->
<link rel="shortcut icon" type="image/x-icon" href="{{ 'image/settings/favicone/' . $setting->favicone }}">
<!-- Material Design Iconic Font-V2.2.0 -->
<link rel="stylesheet" href="{{ asset('frontend/css/material-design-iconic-font.min.css') }}">
<!-- Font Awesome -->
<link rel="stylesheet" href="{{ asset('frontend/css/font-awesome.min.css') }}">
<!-- Font Awesome Stars-->
<link rel="stylesheet" href="{{ asset('frontend/css/fontawesome-stars.css') }}">
<!-- Meanmenu CSS -->
<link rel="stylesheet" href="{{ asset('frontend/css/meanmenu.css') }}">
<!-- owl carousel CSS -->
<link rel="stylesheet" href="{{ asset('frontend/css/owl.carousel.min.css') }}">
<!-- Slick Carousel CSS -->
<link rel="stylesheet" href="{{ asset('frontend/css/slick.css') }}">
<!-- Animate CSS -->
<link rel="stylesheet" href="{{ asset('frontend/css/animate.css') }}">
<!-- Jquery-ui CSS -->
<link rel="stylesheet" href="{{ asset('frontend/css/jquery-ui.min.css') }}">
<!-- Venobox CSS -->
<link rel="stylesheet" href="{{ asset('frontend/css/venobox.css') }}">
<!-- Nice Select CSS -->
<link rel="stylesheet" href="{{ asset('frontend/css/nice-select.css') }}">
<!-- Magnific Popup CSS -->
<link rel="stylesheet" href="{{ asset('frontend/css/magnific-popup.css') }}">
<!-- Bootstrap V4.1.3 Fremwork CSS -->
<link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.min.css') }}">
<!-- Helper CSS -->
<link rel="stylesheet" href="{{ asset('frontend/css/helper.css') }}">
<!-- Main Style CSS -->
<link rel="stylesheet" href="{{ asset('frontend/css/style.css') }}">
<!-- Responsive CSS -->
<link rel="stylesheet" href="{{ asset('frontend/css/responsive.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/style/front.css') }}">

{{-- Auto Search  css --}}
<link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
{{-- user style --}}
<link rel="stylesheet" href="{{ asset('frontend/user-style/css/classy-nav.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/user-style/css/icofont.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/user-style/css/jquery-ui.min.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/user-style/css/magnific-popup.css') }}">
<link rel="stylesheet" href="{{ asset('frontend/user-style/css/style.css') }}">
<!-- Modernizr js -->
<script src="js/vendor/modernizr-2.8.3.min.js"></script>
