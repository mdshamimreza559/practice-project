<div class="header-top">
    <div class="container">
        <div class="row">
            <!-- Begin Header Top Left Area -->
            <div class="col-lg-3 col-md-4">
                <div class="header-top-left">
                    <ul class="phone-wrap">
                        <li><span>Telephone Enquiry:</span><a href="#">{{ $setting->phone }}</a></li>
                    </ul>
                </div>
            </div>
            <!-- Header Top Left Area End Here -->
            <!-- Begin Header Top Right Area -->
            <div class="col-lg-9 col-md-8">
                <div class="header-top-right">
                    <ul class="ht-menu">
                        <!-- Begin Setting Area -->
                        @if (Auth::user())
                            <li>
                                <div class="ht-setting-trigger"><span>Setting</span></div>
                                <div class="setting ht-setting">
                                    <ul class="ht-setting-list">
                                        <li><a href="{{ route('user.dashboard') }}">My Account</a></li>
                                        <li><a href="checkout.html">Checkout</a></li>
                                        <li><a href="{{ route('user.logout') }}">Logout</a></li>

                                        {{-- {!! Form::open(['method' => 'post', 'route' => 'logout']) !!}
                                        {!! Form::button('logout', ['class' => 'btn d-block btn-success btn-sm ', 'type' => 'submit']) !!}
                                        {!! Form::close() !!} --}}

                                    </ul>
                                </div>
                            </li>
                        @else
                            <div class="log-in">
                                <a href="{{ route('user.login') }}"> Login & Register </a>
                            </div>
                        @endif

                        <!-- Setting Area End Here -->
                        <!-- Begin Currency Area -->
                        <li>
                            <span class="currency-selector-wrapper">Currency :</span>
                            <div class="ht-currency-trigger"><span>USD $</span></div>
                            <div class="currency ht-currency">
                                <ul class="ht-setting-list">
                                    <li><a href="#">EUR €</a></li>
                                    <li class="active"><a href="#">USD $</a></li>
                                </ul>
                            </div>
                        </li>
                        <!-- Currency Area End Here -->
                        <!-- Begin Language Area -->
                        <li>
                            <span class="language-selector-wrapper">Language :</span>
                            <div class="ht-language-trigger"><span>English</span></div>
                            <div class="language ht-language">
                                <ul class="ht-setting-list">
                                    <li class="active"><a href="#"><img src="images/menu/flag-icon/1.jpg"
                                                alt="">English</a>
                                    </li>
                                    <li><a href="#"><img src="images/menu/flag-icon/2.jpg"
                                                alt="">Français</a></li>
                                </ul>
                            </div>
                        </li>
                        <!-- Language Area End Here -->
                    </ul>
                </div>
            </div>
            <!-- Header Top Right Area End Here -->
        </div>
    </div>
</div>
<!-- Header middle  Area start Here -->
<div class="header-middle pl-sm-0 pr-sm-0 pl-xs-0 pr-xs-0" id="cart_count">
    <div class="container">
        <div class="row">
            <!-- Begin Header Logo Area -->
            <div class="col-lg-3">
                <div class="logo pb-sm-30 pb-xs-30">
                    <a href="{{ route('front.index') }}">
                        <img src="{{ 'image/settings/logo/' . $setting->logo }}" alt="">
                    </a>
                </div>
            </div>
            <!-- Header Logo Area End Here -->
            <!-- Begin Header Middle Right Area -->
            <div class="col-lg-9">
                <!-- Begin Header Middle Searchbox Area -->
                <form action="{{ route('search') }}" method="POST" class="hm-searchbox">
                    @csrf
                    <select class="nice-select select-search-category">
                        <option value="0">All</option>
                        <option value="10">Laptops</option>
                        <option value="17">- - Prime Video</option>

                        <option value="30"> TV &amp; Video</option>
                    </select>
                    <input type="text" id="search_text" name="search" placeholder="Enter your search key ...">
                    <button class="li-btn" type="submit"><i class="fa fa-search"></i></button>
                </form>
                <!-- Header Middle Searchbox Area End Here -->
                <!-- Begin Header Middle Right Area -->
                <div class="header-middle-right">
                    <ul class="hm-menu">
                        <!-- Begin Header Middle Wishlist Area -->
                        <li class="hm-wishlist">
                            <a href="{{ route('wishlist') }}" id="wishlist_counter">
                                <span class="cart-item-count wishlist-item-count">
                                    {{ \Gloudemans\Shoppingcart\Facades\Cart::instance('wishlist')->count() }}
                                </span><i class="fa fa-heart-o"></i>

                            </a>
                        </li>
                        <!-- Header Middle Wishlist Area End Here -->
                        <!-- Begin Header Mini Cart Area -->
                        <li class="hm-minicart">
                            <div class="hm-minicart-trigger" id="cart_count">
                                <span class="item-icon"></span>
                                <span class="item-text">Cart
                                    <span
                                        class="cart-item-count">{{ \Gloudemans\Shoppingcart\Facades\Cart::instance('shopping')->count() }}</span>
                                </span>
                            </div>
                            <span></span>
                            <div class="minicart">
                                <ul class="minicart-product-list">
                                    @foreach (\Gloudemans\Shoppingcart\Facades\Cart::instance('shopping')->content() as $item)
                                        <li>
                                            <a href="single-product.html" class="minicart-product-image">
                                                <img src="{{ asset('image/product/thumbnail/' . $item->model->photo) }}"
                                                    alt="{{ $item->name }}">
                                            </a>
                                            <div class="minicart-product-details">
                                                <h6><a
                                                        href="{{ route('single.product', $item->model->slug) }}">{{ $item->name }}</a>
                                                </h6>
                                                <span class="me-2">{{ $item->qty }}x </span>
                                                <span>${{ number_format($item->price, 2) }}</span>
                                            </div>
                                            <button class="close" id="cart_delete">
                                                <i class="fa fa-close cart-delete" id="{{ $item->rowId }}"></i>
                                            </button>
                                        </li>
                                    @endforeach
                                </ul>
                                <p class="minicart-total">Total:
                                    @if (session()->has('coupon'))
                                        <span>${{ filter_var(\Gloudemans\Shoppingcart\Facades\Cart::subtotal(), FILTER_SANITIZE_NUMBER_INT) - session('coupon')['value'] }}</span>
                                    @endif


                                </p>
                                <div class="minicart-button">
                                    <a href="{{ route('cart.details') }}"
                                        class="li-button li-button-dark li-button-fullwidth li-button-sm">
                                        <span>View Full Cart</span>
                                    </a>
                                    <a href="{{ route('chechkout1') }}"
                                        class="li-button li-button-fullwidth li-button-sm">
                                        <span>Checkout</span>
                                    </a>
                                </div>
                            </div>

                        </li>
                        <!-- Header Mini Cart Area End Here -->
                    </ul>
                </div>
                <!-- Header Middle Right Area End Here -->
            </div>
            <!-- Header Middle Right Area End Here -->
        </div>
    </div>
</div>

<!-- Header bottom  Area start Here -->
<div class="header-bottom header-sticky stick d-none d-lg-block d-xl-block">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- Begin Header Bottom Menu Area -->
                <div class="hb-menu hb-menu-2">
                    <nav>
                        <ul>
                            <li>
                                <a href="{{ route('front.index') }}">Home</a>
                            </li>
                            <li class="megamenu-holder">
                                <a href="{{ route('shop') }}">Shop</a>
                            </li>
                            <li><a href="{{ route('about.us') }}">About Us</a></li>
                            <li><a href="{{ route('contactUs') }}">Contact</a></li>
                            <!-- Begin Header Bottom Menu Information Area -->
                            <li class="hb-info f-right p-0 d-sm-none d-lg-block">
                                <span>6688 London, Greater London BAS 23JK, UK</span>
                            </li>
                            <!-- Header Bottom Menu Information Area End Here -->
                        </ul>
                    </nav>
                </div>
                <!-- Header Bottom Menu Area End Here -->
            </div>
        </div>
    </div>
</div>
