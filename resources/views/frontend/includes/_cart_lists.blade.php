@if (\Gloudemans\Shoppingcart\Facades\Cart::instance('shopping')->content()->count() > 0)
    <table class="table table-bordered mb-30">
        <thead>
            <tr>
                <th scope="col"> <i class="fa fa-trash" aria-hidden="true"></i></th>
                <th scope="col">Image</th>
                <th scope="col">Product</th>
                <th scope="col">Price</th>
                <th scope="col">Quantity</th>
                <th scope="col">Total</th>
            </tr>
        </thead>
        <tbody>
            @foreach (\Gloudemans\Shoppingcart\Facades\Cart::instance('shopping')->content() as $item)
                <tr>
                    <th scope="row">
                        <a href="" id="cart_delete">
                            <i class="fa fa-times  cart-delete " id="{{ $item->rowId }}"></i>
                        </a>
                    </th>
                    <td>
                        <img src="{{ asset('image/product/thumbnail/' . $item->model->photo) }}"
                            alt="{{ $item->name }}">
                    </td>
                    <td>
                        <a href="{{ route('single.product', $item->model->slug) }}">{{ $item->name }}</a>
                    </td>
                    <td>${{ $item->price }}</td>
                    <td>
                        <div class="quantity">
                            <input type="number" data-id="{{ $item->rowId }}" class="qty-text"
                                id="input_qty_{{ $item->rowId }}" step="1" min="1" max="99"
                                name="quantity" value="{{ $item->qty }}">



                            <input type="hidden" data-id="{{ $item->rowId }}"
                                product-quantity="{{ $item->model->stock }}" id="update_cart_{{ $item->rowId }}">
                        </div>
                    </td>
                    <td>${{ $item->subtotal() }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@else
    <p class="text-danger "><strong>You have not selected a product for the cart</strong></p>
@endif
