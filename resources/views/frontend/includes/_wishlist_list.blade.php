<table class="table">
    <thead>
        <tr>
            <th class="li-product-remove">remove</th>
            <th class="li-product-thumbnail">images</th>
            <th class="cart-product-name">Product</th>
            <th class="li-product-price">Unit Price</th>
            <th class="li-product-add-cart">add to cart</th>
        </tr>
    </thead>
    <tbody>
        @foreach (Gloudemans\Shoppingcart\Facades\Cart::instance('wishlist')->content() as $wishlist)
            <tr>
                <td class="li-product-remove">
                    <a href="" class="wishlist-delete" data-id="{{ $wishlist->rowId }}">
                        <i class="fa fa-times"></i>
                    </a>
                </td>
                <td class="li-product-thumbnail"><a href="#"><img style="width: 60px"
                            src="{{ asset('image/product/thumbnail/' . $wishlist->model->photo) }}" alt=""></a>
                </td>
                <td class="li-product-name"><a href="#">{{ $wishlist->name }}</a></td>
                <td class="li-product-price"><span class="amount">${{ number_format($wishlist->price, 2) }}</span>
                </td>

                <td class="li-product-add-cart">
                    <a class="move-to-cart" quantity="1" data-id="{{ $wishlist->rowId }}"
                        id="add_to_cart_{{ $wishlist->rowId }}" href="javascript:void(0);"> add
                        to
                        cart
                    </a>
                </td>

            </tr>
        @endforeach
    </tbody>
</table>
