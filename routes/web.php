<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BannerController;
use App\Http\Controllers\CouponController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\WishlistController;
use App\Http\Controllers\ListBannerController;
use App\Http\Controllers\FetureBanneController;
use App\Http\Controllers\Backend\UserController;
use App\Http\Controllers\SingleBannerController;
use App\Http\Controllers\Backend\BrandController;
use App\Http\Controllers\Backend\OrderController;
use App\Http\Controllers\Frontend\CartController;
use App\Http\Controllers\Backend\AboutUsController;
use App\Http\Controllers\Backend\BackendController;
use App\Http\Controllers\Backend\OurTeamController;
use App\Http\Controllers\Backend\ProductController;
use App\Http\Controllers\Backend\SettingController;
use App\Http\Controllers\Backend\CategoryController;
use App\Http\Controllers\Backend\ShippingController;
use App\Http\Controllers\Frontend\CheckOutController;
use App\Http\Controllers\Frontend\FrontendController;
use App\Http\Controllers\Backend\SubCategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [FrontendController::class, 'index'])->name('front.index');
Route::get('/categories/{slug}', [FrontendController::class, 'categories'])->name('front.categories');
Route::get('/single-product/{slug}', [FrontendController::class, 'singleProduct'])->name('single.product');
Route::get('/product-brand/{slug}', [FrontendController::class, 'productBrand'])->name('product.brand');
Route::post('/quick-view', [FrontendController::class, 'quickView'])->name('quick.view');

// review section
Route::post('/product-review/{slug}', [FrontendController::class, 'productriview'])->name('review.store');

Route::get('/user/login', [FrontendController::class, 'userLogin'])->name('user.login');
Route::post('/user/submit', [FrontendController::class, 'loginSubmit'])->name('login.submit');
Route::post('/register/submit', [UserController::class, 'store'])->name('register.submit');
Route::get('/user/logout', [FrontendController::class, 'userLogout'])->name('user.logout');

// Auto Search Section
Route::get('auto-search', [FrontendController::class, 'autoSearch'])->name('auto.search');
Route::post('search', [FrontendController::class, 'search'])->name('search');

//WishList section
Route::get('wishlists', [WishlistController::class, 'create'])->name('wishlist');
Route::post('wishlist/store', [WishlistController::class, 'store'])->name('wish.store');
Route::post('wishlist/delete', [WishlistController::class, 'delete'])->name('wishlist.delete');
Route::post('move-to-cart', [WishlistController::class, 'moveToCart'])->name('move.cart');

// CheckOut Section Start
Route::get('check-out', [CheckOutController::class, 'checkOut1'])->name('chechkout1');
Route::post('checkout-one', [CheckOutController::class, 'checkOut1Store'])->name('checkout1.store');
Route::post('checkout-two', [CheckOutController::class, 'checkOut2Store'])->name('checkout2.store');
Route::post('checkout-three', [CheckOutController::class, 'checkOut3Store'])->name('checkout3.store');
Route::post('checkout-four', [CheckOutController::class, 'checkOut4Store'])->name('checkout4.store');
Route::get('checkout-complete/{order}', [CheckOutController::class, 'checkOutComplete'])->name('checkout.complete');

//cart Section
Route::post('user/cart/store', [CartController::class, 'cartStore'])->name('cart.store');

Route::post('product-details', [FrontendController::class, 'sizeFilter'])->name('size.filter');

// About us
Route::get('about-us', [FrontendController::class, 'aboutUs'])->name('about.us');
// Contact Section
Route::get('contact-us', [FrontendController::class, 'contactUs'])->name('contactUs');
Route::post('contact', [ContactController::class, 'contactStore'])->name('contact.store');
// Shop Section
Route::get('shop', [FrontendController::class, 'shop'])->name('shop');
Route::post('shop-filter', [FrontendController::class, 'shopFilter'])->name('shop.filter');

Route::group(['prefix' => 'user', 'middleware' => 'auth'], function () {
    Route::get('dashboard', [UserController::class, 'userAccount'])->name('user.dashboard');


    //cart Section
    Route::post('cart/delete', [CartController::class, 'cartDelete'])->name('cart.delete');
    Route::get('cart/details', [CartController::class, 'cartDetails'])->name('cart.details');
    Route::post('cart-update', [CartController::class, 'cartUpdate'])->name('cart.update');

    // coupon section
    Route::post('coupon-add', [CartController::class, 'couponAdd'])->name('coupon.addd');



    //update user Section
    Route::get('orders', [UserController::class, 'userOrder'])->name('user.orders');
    Route::get('account-deatils', [UserController::class, 'userDeatils'])->name('user.deatils');
    Route::post('update-user-address/{id}', [UserController::class, 'updateAdress'])->name('userAddress.update');
    Route::post('update-account/{id}', [UserController::class, 'updateAccount'])->name('update.account');
    Route::get('wishlists', [UserController::class, 'userWishlist'])->name('user.wishlist');

    // coupon Controller
    Route::resource('cupon', CouponController::class);
    Route::post('coupon-status', [CouponController::class, 'couponStatus'])->name('coupon.status');
});


// Backend Section
Route::group(['prefix' => 'dashboard', 'middleware' => ['auth', 'admin']], function () {
    Route::get('/', [BackendController::class, 'index'])->name('dashboard.index');

    // Category Route
    Route::resource('categories', CategoryController::class);
    Route::post('categories-status', [CategoryController::class, 'categoryStatus'])->name('category.status');


    //Sub Category Route
    Route::resource('sub-categories', SubCategoryController::class);
    Route::post('sub-categories-status', [SubCategoryController::class, 'subCategoryStatus'])->name('subCategory.status');
    // Route::get('get-sub-categories/{id}', [SubCategoryController::class, 'subCategory']);

    //Brand Route
    Route::resource('brand', BrandController::class);
    Route::post('brand-status', [BrandController::class, 'brandStatus'])->name('brand.status');

    //Product Route
    Route::resource('products', ProductController::class);
    Route::post('products-status', [ProductController::class, 'productStatus'])->name('product.status');

    // Product Attribute section
    Route::get('add-product-attribute/{id}', [ProductController::class, 'productAttribute'])->name('product.attribute');
    Route::post('product-attribute-store/{id}', [ProductController::class, 'attributeStore'])->name('attribute.store');
    Route::get('product-attributes', [ProductController::class, 'attributeList'])->name('productAttr.index');
    Route::delete('product-attribute-delete/{id}', [ProductController::class, 'attributeDelete'])->name('attribute.delete');

    //User Route
    Route::resource('user', UserController::class);
    Route::post('user-type', [UserController::class, 'userType'])->name('user.type');

    //Banner Route
    Route::resource('banners', BannerController::class);
    Route::post('banners-status', [BannerController::class, 'bannerStatus'])->name('banner.status');

    // Single Banner
    Route::resource('single-banner', SingleBannerController::class);
    Route::post('single-banners-status', [SingleBannerController::class, 'singleStatus'])->name('single.status');

    // List Banner Route
    Route::resource('list-banner', ListBannerController::class);
    Route::post('list-banner-status', [ListBannerController::class, 'listBannerStatus'])->name('list-banner.status');

    // Feature product banner

    // Route::resource('feature-banner', FetureBanneController::class);
    Route::get('feature-banner/create', [FetureBanneController::class, 'create'])->name('feature-banner.create');
    Route::post('feature-banner', [FetureBanneController::class, 'store'])->name('feature-banner.store');
    Route::get('feature-banner', [FetureBanneController::class, 'index'])->name('feature-banner.index');
    Route::get('feature-banner/{id}', [FetureBanneController::class, 'show'])->name('feature-banner.show');
    Route::get('feature-banner/{id}/edit', [FetureBanneController::class, 'edit'])->name('feature-banner.edit');
    Route::put('feature-banner/{id}', [FetureBanneController::class, 'update'])->name('feature-banner.update');
    Route::delete('feature-banner/{id}', [FetureBanneController::class, 'destroy'])->name('feature-banner.destroy');

    Route::post('feature-status', [FetureBanneController::class, 'featureStatus'])->name('feature.status');

    //Order Section
    Route::resource('order', OrderController::class);
    Route::post('order-status', [OrderController::class, 'orderStatus'])->name('order.status');
    Route::get('order-search', [OrderController::class, 'orderSearch'])->name('order.search');

    //Setting Section
    Route::get('setting', [SettingController::class, 'settingEdit'])->name('setting.edit');
    Route::post('setting', [SettingController::class, 'settingStore'])->name('setting.store');

    // Shippiong Route
    Route::resource('shippings', ShippingController::class);
    Route::post('shipping-status', [ShippingController::class, 'shippingStatus'])->name('shipping.status');

    // About us Section
    Route::get('about-us-create', [AboutUsController::class, 'edit'])->name('about.create');
    Route::post('about-us', [AboutUsController::class, 'aboutUpdate'])->name('aboutus.store');

    // Team Membar Secction
    Route::resource('teams', OurTeamController::class);
    Route::get('member-details/{id}', [OurTeamController::class, 'show'])->name('member.show');
    Route::get('member-edit/{id}', [OurTeamController::class, 'edit'])->name('member.edit');
    Route::put('member-update/{id}', [OurTeamController::class, 'update'])->name('member.update');
    Route::get('member-show/{id}', [OurTeamController::class, 'show'])->name('member.show');
    Route::delete('member-delete/{id}', [OurTeamController::class, 'destroy'])->name('member.destroy');


    Route::post('member-status', [OurTeamController::class, 'memberStatus'])->name('member.status');
});

Route::get('get-sub-categories/{id}', [SubCategoryController::class, 'subCategory']);

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

require __DIR__ . '/auth.php';
