<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Support\Str;
use App\Models\Backend\Brand;
use Illuminate\Foundation\Auth\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = Product::class;
    public function definition()
    {
        $product_name = $this->faker->unique()->word(4, true);
        $slug = Str::slug($product_name);
        return [
            'title' => $product_name,
            'slug' => $slug,
            'short_des' => $this->faker->text(200),
            'description' => $this->faker->text(500),
            'stock' => $this->faker->numberBetween(1, 20),
            'brand_id' => $this->faker->randomElement(Brand::pluck('id')->toArray()),
            'category_id' => $this->faker->randomElement(Category::pluck('id')),
            'sub_category_id' => $this->faker->randomElement(SubCategory::pluck('id')),
            'user_id' => 1,
            'price' => $this->faker->numberBetween(100, 10000),
            'offer_price' => $this->faker->numberBetween(50, 3000),
            'discount' => $this->faker->numberBetween(5, 10),
            'size' => $this->faker->randomElement(['S', 'M', 'L', 'XL']),
            'status' => 1,
            'condition' => $this->faker->randomElement(['New', 'Popular', 'Winter']),
            'photo' => $this->faker->imageUrl(480, 280),
        ];
    }
}
