<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\SubCategory>
 */
class SubCategoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = SubCategory::class;
    public function definition()
    {
        return [
            'category_id' => $this->faker->randomElement(Category::pluck('id')),
            'name' => $this->faker->word,
            'slug' => $this->faker->unique()->slug,
            'status' => 1,
            'serial' => $this->faker->numberBetween(1, 20),
            'user_id' => 1,
        ];
    }
}
