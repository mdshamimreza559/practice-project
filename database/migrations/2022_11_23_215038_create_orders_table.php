<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('order_number', 10)->unique();
            $table->bigInteger('sub_total')->nullable();
            $table->bigInteger('total_amount')->nullable();
            $table->float('delivery_charge')->default(0);
            $table->string('payment_method')->default('cod');
            $table->enum('payment_status', ['paid', 'unpaid'])->default('unpaid');
            $table->enum('condition', ['processing', 'pending', 'delivered', 'cancelled'])->nullable();
            $table->float('coupon')->default(0);
            $table->integer('quantity')->default(0);


            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('country')->nullable();
            $table->string('company_name')->nullable();
            $table->string('address')->nullable();
            $table->string('apartment_suite')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->integer('postcode')->nullable();
            $table->mediumText('note')->nullable();


            $table->string('sfirst_name')->nullable();
            $table->string('slast_name')->nullable();
            $table->string('semail')->nullable();
            $table->string('sphone')->nullable();
            $table->string('scountry')->nullable();
            $table->string('scompany_name')->nullable();
            $table->string('saddress')->nullable();
            $table->string('sapartment_suite')->nullable();
            $table->string('scity')->nullable();
            $table->string('sstate')->nullable();
            $table->integer('spostcode')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
