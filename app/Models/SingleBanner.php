<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class SingleBanner extends Model
{
    use HasFactory;

    /**
     * Summary of
     * @var mixed
     */
    protected $guarded = [];
}
