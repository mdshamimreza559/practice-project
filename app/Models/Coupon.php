<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Coupon extends Model
{
    use HasFactory;

    protected $guarded = [];


    public function discount($total)
    {
        // dd($this->type);
        if ($this->type == 1) {
            return $this->value;
        } elseif ($this->type == 2) {
            return ($this->value / 100) * $total;
        } else {
            return 0;
        }
    }
}
