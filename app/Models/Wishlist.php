<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Wishlist extends Model
{
    use HasFactory;
    protected $guarded = [];


    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public static function getCartProduct($id)
    {
        return self::where('id', $id)->get()->toArray();
    }
}
