<?php

namespace App\Helper;

use App\Models\Product;
use Intervention\Image\Facades\Image;

class Helper
{

    /**
     * Summary of uploadImage
     * @param mixed $photo
     * @param mixed $width
     * @param mixed $height
     * @param mixed $path
     * @param mixed $name
     * @return void
     */
    public static function uploadImage($photo, $width, $height, $path, $name)
    {
        Image::make($photo)->fit($width, $height)->save(public_path($path) . $name, 50);
    }
    /**
     * Summary of unlinkImage
     * @param mixed $path
     * @param mixed $name
     * @return void
     */
    public static function unlinkImage($path, $name)
    {
        $img_path = $path . $name;
        if (file_exists($img_path)) {
            unlink($img_path);
        }
    }

    public static function  topRated($product)
    {

        $placeholders = implode(',', array_fill(0, count($product), '?'));

        $best_selling = Product::whereIn('id', $product)
            ->orderByRaw("field(id,{$placeholders})", $product)->take(6)->get();

        return $best_selling;
    }
}
