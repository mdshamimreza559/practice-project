<?php

namespace App\Http\Controllers\Backend;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::orderBy('id', 'desc')->get();
        return view('backend.modules.user.index', compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function userType(Request $request)
    {
        $mode = $request->mode;
        if ($mode == 1) {
            User::where('id', $request->value)->update(['role' => 1]);
        } else {
            User::where('id', $request->value)->update(['role' => 0]);
        }
        return response()->json(['msg' => 'User type updated successfully', 'role' => $mode]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'f_name' => 'required|min:2',
            'l_name' => 'required|min:3',
            'reg_email' => 'required|unique:users,email,',
            'phone' => 'required|digits:11|unique:users,phone,',
            'reg_password' => 'required|min:6|confirmed'
        ]);
        $user = User::create([
            'f_name' => $request->f_name,
            'l_name' => $request->l_name,
            'phone' => $request->phone,
            'email' => $request->reg_email,
            'address' => $request->address,
            'country' => $request->country,
            'city' => $request->city,
            'post_code' => $request->post_code,
            'gendar' => $request->gendar,
            'role' => 0,
            'password' => Hash::make($request->reg_password),
        ]);

        // $user_data = $request->all();
        // dd($user_data);

        // $user_data['password'] = Hash::make($request->reg_password);
        // $user_data['role'] = 0;
        // User::create($user_data);
        session()->flash('msg', 'Successfully Register');
        session()->flash('cls', 'success');
        return redirect()->route('front.index');
    }

    /**
     * Summary of userAccount
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function userAccount()
    {
        return view('frontend.pages.user.account');
    }

    /**
     * Summary of userAddress
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function userWishlist()
    {
        return view('frontend.pages.user.wishlist');
    }
    /**
     * Summary of userOrder
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function userOrder()
    {
        return view('frontend.pages.user.orders');
    }
    /**
     * Summary of deatils
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function userDeatils()
    {
        return view('frontend.pages.user.account-details');
    }

    /**
     * Summary of updateAdress
     * @param Request $request
     * @param mixed $id
     * @return mixed
     */
    public function updateAdress(Request $request, $id)
    {
        $user =  User::where('id', $id)->update(['f_name' => $request->f_name, 'l_name' => $request->l_name, 'address' => $request->address, 'phone' => $request->phone, 'country' => $request->country, 'city' => $request->city, 'gendar' => $request->gendar, 'post_code' => $request->post_code]);
        if ($user) {
            session()->flash('msg', 'Address updated successfuly');
            session()->flash('cls', 'success');
            return redirect()->back();
        } else {
            session()->flash('msg', 'Something went wrong');
            session()->flash('cls', 'success');
            return redirect()->back();
        }
    }

    public function updateAccount(Request $request, $id)
    {
        $user_pass = Auth::user()->password;
        $h_old_pass = Hash::make($request->old_password);
        $h_new_pass = Hash::make($request->new_pass);


        $old_pass = $request->old_password;
        $new_pass = $request->new_pass;
        if ($old_pass == null && $new_pass == null) {
            User::where('id', $id)->update(['f_name' => $request->f_name, 'l_name' => $request->l_name, 'email' => $request->email, 'phone' => $request->phone]);
            return redirect()->back();
        } else {
            if ($user_pass == $h_old_pass && $user_pass != $h_new_pass) {
                User::where('id', $id)->update(['f_name' => $request->f_name, 'l_name' => $request->l_name, 'email' => $request->email, 'phone' => $request->phone, 'password' => $request->new_pass]);
            } else {
                session()->flash('msg', 'Error old password does\'t match');
                session()->flash('cls', 'warning');
                return redirect()->back();
            }
        }
    }
}
