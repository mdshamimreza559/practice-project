<?php

namespace App\Http\Controllers\Backend;

use App\Helper\Helper;
use App\Models\OurTeam;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreOurTeamRequest;
use App\Http\Requests\UpdateOurTeamRequest;

class OurTeamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teamMember = OurTeam::orderBy('id', 'desc')->get();
        return view('backend.modules.team.index', compact('teamMember'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.modules.team.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreOurTeamRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOurTeamRequest $request)
    {
        $data = $request->all();

        $data['name'] = strtolower($request->input('name'));
        $data['skills'] = strtolower($request->input('skills'));

        if ($request->file('photo')) {
            $photo = $request->file('photo');
            $width = 270;
            $height = 317;
            $path = 'image/team/';
            $name = str_replace(' ', '-', $request->input('name')) . '-' . rand(11, 99) . '.webp';
            $data['photo'] = $name;
            Helper::uploadImage($photo, $width, $height, $path, $name);
        }

        OurTeam::create($data);
        session()->flash('msg', 'Team member add succcessfully');
        session()->flash('cls', 'success');
        return redirect()->route('teams.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OurTeam  $ourTeam
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ourTeam = OurTeam::findOrFail($id);
        // return $ourTeam;
        return view('backend.modules.team.show', compact('ourTeam'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OurTeam  $ourTeam
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ourTeam = OurTeam::findOrFail($id);
        return view('backend.modules.team.edit', compact('ourTeam'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateOurTeamRequest  $request
     * @param  \App\Models\OurTeam  $ourTeam
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateOurTeamRequest $request,  $id)
    {
        $ourTeam = OurTeam::findOrFail($id);

        $data = $request->all();

        $data['name'] = strtolower($request->input('name'));
        $data['skills'] = strtolower($request->input('skills'));

        if ($request->file('photo')) {
            $photo = $request->file('photo');
            $width = 270;
            $height = 317;
            $path = 'image/team/';
            $name = str_replace(' ', '-', $request->input('name')) . '-' . rand(11, 99) . '.webp';
            $data['photo'] = $name;
            if ($ourTeam->photo != null) {
                Helper::unlinkImage($path, $ourTeam->photo);
            }
            Helper::uploadImage($photo, $width, $height, $path, $name);
        }

        $ourTeam->update($data);
        session()->flash('msg', 'Team member add succcessfully');
        session()->flash('cls', 'success');
        return redirect()->route('teams.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OurTeam  $ourTeam
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ourTeam = OurTeam::findOrFail($id);
        $ourTeam->delete();
        session()->flash('msg', 'Team Member deleted successfully');
        session()->flash('cls', 'warning');
        return redirect()->back();
    }

    /**
     * Summary of memberStatus
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function memberStatus(Request $request)
    {
        $mode = $request->mode;
        if ($mode == 1) {
            OurTeam::where('id', $request->value)->update(['status' => 1]);
        } else {
            OurTeam::where('id', $request->value)->update(['status' => 2]);
        }

        return response()->json(['status' => $mode]);
    }
}
