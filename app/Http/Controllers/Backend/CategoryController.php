<?php

namespace App\Http\Controllers\Backend;


use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = Category::orderBy('order_by', 'desc')->get();
        return view('backend.modules.category.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.modules.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCategoryRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCategoryRequest $request)
    {
        $cat_data = $request->all();
        $cat_data['slug'] = Str::slug($request->input('slug'));
        Category::create($cat_data);
        session()->flash('msg', 'Category Created Successfully');
        session()->flash('cls', 'success');
        return redirect()->route('categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return view('backend.modules.category.show', compact('category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view('backend.modules.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCategoryRequest  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCategoryRequest $request, Category $category)
    {
        $category->update($request->all());
        session()->flash('msg', 'Category updated successfully');
        session()->flash('cls', 'success');
        return redirect()->route('categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
        session()->flash('msg', 'Category Deleted Successfully');
        session()->flash('cls', 'warning');
        return redirect()->back();
    }

    /**
     * Summary of categoryStatus
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function categoryStatus(Request $request)
    {
        $mode = $request->mode;
        if ($mode == 1) {
            Category::where('id', $request->value)->update(['status' => 1]);
        } else {
            Category::where('id', $request->value)->update(['status' => 2]);
        }
        return response()->json(['status' => $mode]);
    }
}