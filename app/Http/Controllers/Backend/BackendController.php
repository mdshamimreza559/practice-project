<?php

namespace App\Http\Controllers\Backend;

use App\Models\User;
use App\Models\Order;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;


class BackendController extends Controller
{
    public function index()
    {
        $orders = Order::with('orderProducts', 'user')->orderBy('id', 'DESC')->paginate(10);
        $order = Order::with('orderProducts', 'user')->orderBy('id', 'DESC')->get();
        $products = Product::with('category')->where('status', 1)->get();
        $categories = Category::where('status', 1)->take(8)->get();
        $user = User::where('role', 0)->get();

        // dd($order);
        return view('backend.index', compact('orders', 'order', 'products', 'categories', 'user'));
    }
}
