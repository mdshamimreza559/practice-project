<?php

namespace App\Http\Controllers\Backend;

use App\Models\User;
use App\Models\Order;
use App\Models\Product;
use App\Models\Category;
use App\Models\OrderProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function orderSearch(Request $request)
    {
        // dd($request->all());
        $orders = Order::with('orderProducts', 'user')->orderBy('id', 'DESC')->paginate(10);
        $products = Product::with('category')->where('status', 1)->get();
        $categories = Category::where('status', 1)->take(8)->get();
        $user = User::where('role', 0)->get();



        $order = Order::with('orderProducts', 'user')->orderBy('id', 'DESC')->get();
        $data = $request->input('order_search');

        $orders = Order::with('orderProducts');

        if ($request->order_search != null) {
            $orders = $orders->where('email', 'LIKE', '%' . $request->order_search . '%')->orWhere('order_number', 'LIKE', '%' . $request->order_search . '%');
        }

        if ($request->condition != null) {
            $orders = $orders->where('condition', $request->condition);
        }

        if ($request->date != null) {
            $s_date = date('Y-m-d', strtotime(explode(' - ', $request->date)[0]));
            $e_date = date('Y-m-d', strtotime(explode(' - ', $request->date)[1]));
            $orders = $orders->whereDate('created_at', '>=',  $s_date)->whereDate('created_at', '<=',  $e_date);
        }


        $orders = $orders->get();
        // $product = Order::where('email', 'LIKE', '%' . $data . '%')->orwhere('order_number', 'LIKE', '%' . $data . '%')->get();

        // dd($product);
        return view('backend.modules.search.search', compact('order', 'user', 'categories', 'products', 'orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $route = 'dashboard';
        $data = Order::findOrFail($id);
        $product = Order::with('orderProducts.product')->where('id', $id)->first();
        // dd($product);
        return view('backend.modules.order.order', compact('data', 'product', 'route'));
    }


    public function orderStatus(Request $request)
    {
        $order = Order::findOrFail($request->input('order_id'));
        if ($order) {
            if ($request->input('condition') == 'delivered') {
                foreach ($order->orderProducts as $item) {
                    $product = Product::where('id', $item->product_id)->first();
                    $stock = $product->stock;
                    $stock -= $item->quantity;
                    $product->update(['stock' => $stock]);
                    Order::where('id', $request->input('order_id'))->update(['payment_status' => 'paid']);
                }
            }
            Order::where('id', $order->id)->update(['condition' => $request->condition]);
        }
        session()->flash('msg', 'Order condition updated successfully');
        session()->flash('cls', 'success');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $order = Order::findOrFail($id);
        // $order->delete();
        // session()->flash('msg', 'Order deleted successfully');
        // session()->flash('cls', 'success');
        // return redirect()->route('dashboard.index');
    }
}
