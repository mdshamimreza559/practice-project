<?php

namespace App\Http\Controllers\Backend;

use App\Models\Category;
use App\Models\SubCategory;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\SubCategoryResource;
use App\Http\Requests\StoreSubCategoryRequest;
use App\Http\Requests\UpdateSubCategoryRequest;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = SubCategory::with('category')->orderBy('serial', 'desc')->get();
        return view('backend.modules.subcategory.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::pluck('name', 'id');
        return view('backend.modules.subcategory.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSubCategoryRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSubCategoryRequest $request)
    {

        $data = $request->all();
        $data['slug'] = Str::slug($request->input('slug'));
        $data['user_id'] = Auth::user()->id;
        SubCategory::create($data);
        session()->flash('msg', 'SubCategory Create Successfully');
        session()->flash('cls', 'success');
        return redirect()->route('sub-categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function show(SubCategory $subCategory)
    {
        $category = SubCategory::with('category')->orderBy('serial', 'desc')->get();
        return view('backend.modules.subcategory.show', compact('subCategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(SubCategory $subCategory)
    {
        $category = Category::pluck('name', 'id');
        return view('backend.modules.subcategory.edit', compact('subCategory', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateSubCategoryRequest  $request
     * @param  \App\Models\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSubCategoryRequest $request, SubCategory $subCategory)
    {
        $data = $request->all();
        $data['slug'] = Str::slug($request->input('slug'));
        $subCategory->update($data);
        session()->flash('msg', 'Sub Category updated successfully');
        session()->flash('cls', 'success');
        return redirect()->route('sub-categories.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubCategory $subCategory)
    {
        $subCategory->delete();
        session()->flash('msg', 'Sub category deleted successfully');
        session()->flash('cls', 'warning');
        return redirect()->back();
    }

    /**
     * Summary of subCategoryStatus
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function subCategoryStatus(Request $request)
    {
        $mode = $request->status;
        if ($mode == 1) {
            SubCategory::where('id', $request->value)->update(['status' => 1]);
        } else {
            SubCategory::where('id', $request->value)->update(['status' => 2]);
        }
        return response()->json(['msg' => 'sub category status updated successfully', 'status' => $mode]);
    }

    public function subCategory($id)
    {
        $subcategory = SubCategory::where('status', 1)->where('category_id', $id)->get();
        return SubCategoryResource::collection($subcategory);
    }
}
