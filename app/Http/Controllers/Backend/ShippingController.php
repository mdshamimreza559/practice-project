<?php

namespace App\Http\Controllers\Backend;

use App\Models\Shipping;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreShippingRequest;
use App\Http\Requests\UpdateShippingRequest;

class ShippingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shhipings = Shipping::orderBy('id', 'desc')->get();
        return view('backend.modules.shipping.index', compact('shhipings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.modules.shipping.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreShippingRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreShippingRequest $request)
    {
        Shipping::create($request->all());
        session()->flash('msg', 'Shipping Add successfully');
        session()->flash('cls', 'success');
        return redirect()->route('shippings.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Shipping  $shipping
     * @return \Illuminate\Http\Response
     */
    public function show(Shipping $shipping)
    {
        return view('backend.modules.shipping.show', compact('shipping'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Shipping  $shipping
     * @return \Illuminate\Http\Response
     */
    public function edit(Shipping $shipping)
    {
        return view('backend.modules.shipping.edit', compact('shipping'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateShippingRequest  $request
     * @param  \App\Models\Shipping  $shipping
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateShippingRequest $request, Shipping $shipping)
    {
        // $request->validate(
        //     [
        //         'time' => ['required', 'min:3', 'max:255', Rule::unique('shippings')->ignore($shipping)],
        //     ]
        // );
        $shipping->update($request->all());
        session()->flash('msg', 'Shipping updated successfully');
        session()->flash('cls', 'success');
        return redirect()->route('shippings.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Shipping  $shipping
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shipping $shipping)
    {
        $shipping->delete();
        session()->flash('msg', 'Shipping deleted successfully');
        session()->flash('cls', 'success');
        return redirect()->back();
    }

    /**
     * Summary of shippingStatus
     * @param Request $request
     * @return void
     */
    public function shippingStatus(Request $request)
    {
        $mode = $request->mode;

        if ($mode == 1) {
            Shipping::where('id', $request->value)->update(['status' => 1]);
        } else {
            Shipping::where('id', $request->value)->update(['status' => 2]);
        }
        return response()->json(['status' => $mode]);
    }
}
