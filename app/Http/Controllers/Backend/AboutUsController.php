<?php

namespace App\Http\Controllers\Backend;

use App\Helper\Helper;
use Illuminate\Http\Request;
use App\Models\Backend\AboutUs;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAboutUsRequest;
use App\Http\Requests\UpdateAboutUsRequest;
use App\Models\Backend\AboutUs as BackendAboutUs;

class AboutUsController extends Controller
{

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\AboutUs  $aboutUs
     * @return \Illuminate\Http\Response
     */
    public function edit(AboutUs $aboutUs)
    {
        $about = AboutUs::orderBy('id', 'desc')->first();
        return view('backend.modules.aboutus.edit', compact('about'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateAboutUsRequest  $request
     * @param  \App\Models\AboutUs  $aboutUs
     * @return \Illuminate\Http\Response
     */
    public function aboutUpdate(Request $request, AboutUs $aboutUs)
    {
        $value =  AboutUs::all();

        $data = $request->all();
        $about = AboutUs::first();
        $data['title'] = $request->input('title');
        $data['content'] = $request->input('content');
        $data['content'] = $request->input('content');
        $data['awards_winned'] = $request->input('awards_winned');
        $data['hours_worked'] = $request->input('hours_worked');
        $data['complete_project'] = $request->input('complete_project');
        if ($request->file('photo')) {
            $photo = $request->file('photo');
            $width = 440;
            $height = 275;
            $path = 'image/about/';
            $name = strtolower($request->input('title')) . '-' . rand(1, 50) . '.webp';
            $data['photo'] = $name;
            if (!empty($value[0]['photo'])) {
                Helper::unlinkImage($path, $value[0]['photo']);
                Helper::uploadImage($photo, $width, $height, $path, $name);
            } else {
                Helper::uploadImage($photo, $width, $height, $path, $name);
            }
        }

        if (!empty($value[0]['id'])) {
            $about->update($data);
        } else {
            AboutUs::create($data);
        }
        session()->flash('msg', 'Aboutus content updated succcessfully ');
        session()->flash('class', 'success');
        return redirect()->back();
    }
}
