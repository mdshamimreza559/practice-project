<?php

namespace App\Http\Controllers\Backend;

use App\Helper\Helper;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Backend\Brand;
use App\Models\ProductAttribute;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = DB::table('products')->orderBy('id', 'DESC')->get();
        return view('backend.modules.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::pluck('name', 'id');
        $brand = Brand::pluck('name', 'id');
        return view('backend.modules.product.create', compact('category', 'brand'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreProductRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreProductRequest $request)
    {
        $product_data = $request->all();
        $product_data['slug'] = Str::slug($request->input('slug'));
        $price = $request->input('price');
        $discount = $request->input('discount');
        if ($discount) {
            $product_data['offer_price'] = (float) str_replace(',', '', ceil($price - ($price / 100) * $discount));
        } else {
            $product_data['offer_price'] = 00;
        }

        $product_data['user_id'] = Auth::user()->id;
        if ($request->file('photo')) {
            $photo = $request->file('photo');
            $width = 520;
            $thambWidth = 320;
            $height = 520;
            $thumbheight = 320;
            $path = 'image/product/orginal/';
            $thumPath = 'image/product/thumbnail/';
            $name = Str::slug($request->input('slug') . '-' . rand(1111, 9999)) . '.webp';
            $product_data['photo'] = $name;
            Helper::uploadImage($photo, $width, $height, $path, $name);
            Helper::uploadImage($photo, $thambWidth, $thumbheight, $thumPath, $name);
        }
        Product::create($product_data);
        session()->flash('msg', 'Product Created successfully');
        session()->flash('cls', 'success');
        return redirect()->route('products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $product->load('category', 'subCategory', 'user', 'brand');
        return view('backend.modules.product.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $category = Category::pluck('name', 'id');
        $brand = Brand::pluck('name', 'id');
        return view('backend.modules.product.edit', compact('category', 'brand', 'product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateProductRequest  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
        $product_data = $request->all();
        $product_data['slug'] = Str::slug($request->input('slug'));
        $price = $request->input('price');
        $discount = $request->input('discount');
        if ($discount) {
            $product_data['offer_price'] = ceil($price - ($price / 100) * $discount);
        } else {
            $product_data['offer_price'] = '';
        }

        $product_data['user_id'] = Auth::user()->id;
        if ($request->file('photo')) {
            $photo = $request->file('photo');
            $width = 720;
            $height = 680;
            $thambWidth = 300;
            $thumbheight = 300;
            $path = 'image/product/orginal/';
            $thumPath = 'image/product/thumbnail/';
            $name = Str::slug($request->input('slug') . '-' . rand(1111, 9999)) . '.webp';
            $product_data['photo'] = $name;

            if ($product->photo != null) {
                Helper::unlinkImage($path, $product->photo);
                Helper::unlinkImage($thumPath, $product->photo);
            }

            Helper::uploadImage($photo, $width, $height, $path, $name);
            Helper::uploadImage($photo, $thambWidth, $thumbheight, $thumPath, $name);
        }
        $product->update($product_data);
        session()->flash('msg', 'Product Created successfully');
        session()->flash('cls', 'success');
        return redirect()->route('products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        if ($product->photo != null) {
            $path = 'image/product/orginal/';
            $thumPath = 'image/product/thumbnail/';
            Helper::unlinkImage($path, $product->photo);
            Helper::unlinkImage($thumPath, $product->photo);
        }
        $product->delete();
        session()->flash('msg', 'Product delete successfully');
        session()->flash('cls', 'warning');
        return redirect()->back();
    }

    /**
     * Summary of productStatus
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function productStatus(Request $request)
    {
        $mode = $request->mode;
        if ($mode == 1) {
            Product::where('id', $request->id)->update(['status' => 1]);
        } else {
            Product::where('id', $request->id)->update(['status' => 2]);
        }
        return response()->json(['msg' => 'product status update successfully', 'status' => $mode]);
    }

    /**
     * Summary of productAttribute
     * @param Request $request
     * @param mixed $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function productAttribute(Request $request, $id)
    {
        $products = Product::findOrFail($id);
        $product_attr = ProductAttribute::with('product')->where('product_id', $id)->get();
        return  view('backend.modules.attributes.create', compact('products', 'product_attr'));
    }

    /**
     * Summary of attributeStore
     * @param Request $request
     * @param mixed $id
     * @return void
     */
    public function attributeStore(Request $request, $id)
    {
        $this->validate($request, [
            'size' => 'required',
            'price' => 'required',
            'stock' => 'required',
        ], [
            'size.required' => 'Atrribute Size is required',
        ]);

        $size = $request->input('size');
        $req = ProductAttribute::where('size', $size)->where('product_id', $id)->get();

        if (count($req) > 0) {
            return back();
        }
        $products = $request->all();
        foreach ($products['price'] as $key => $data) {
            if (!empty($data)) {
                $value = new ProductAttribute;
                $value['product_id'] = $id;
                $value['size'] = strtoupper($products['size'][$key]);
                $value['offer_price'] = $products['offer_price'][$key];
                $value['stock'] = $products['stock'][$key];
                $value['price'] = $data;
                $value->save();
            }
        }
        session()->flash('msg', 'product attribute create successfully');
        session()->flash('cls', 'success');
        return redirect()->back();
    }

    /**
     * Summary of attributeDelete
     * @param mixed $id
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function attributeDelete($id)
    {
        $product = ProductAttribute::findOrFail($id);
        $product->delete();
        return redirect()->back();
    }

    /**
     * Summary of attributeList
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function attributeList()
    {
        $attr = ProductAttribute::with('product')->get();
        return view('backend.modules.attributes.index', compact('attr'));
    }
}
