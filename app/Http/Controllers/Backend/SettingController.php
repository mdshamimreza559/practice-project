<?php

namespace App\Http\Controllers\Backend;

use App\Helper\Helper;
use App\Models\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingController extends Controller
{
    /**
     * Summary of settingEdit
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function settingEdit()
    {
        $setting = Setting::orderBy('id', 'DESC')->first();
        return view('backend.modules.setting.setting', compact('setting'));
    }

    public function settingStore(Request $request, Setting $setting)
    {
        $setting_data = $request->all();
        $setting = Setting::first();
        $setting_data['title'] = $request->input('title');
        $setting_data['meta_keyword'] = $request->input('meta_keyword');
        $setting_data['meta_description'] = $request->input('meta_description');
        $setting_data['phone'] = $request->input('phone');
        $setting_data['email'] = $request->input('email');
        $setting_data['facebook_url'] = $request->input('facebook_url');
        $setting_data['twitter_url'] = $request->input('twitter_url');
        $setting_data['linkdin_url'] = $request->input('linkdin_url');

        $setting_data['youtube_url'] = $request->input('youtube_url');
        $setting_data['instagram_url'] = $request->input('instagram_url');
        $setting_data['linkdin_url'] = $request->input('linkdin_url');
        $setting_data['pinterest_url'] = $request->input('pinterest_url');

        if ($request->file('logo')) {
            $photo =   $request->file('logo');
            $width = 190;
            $height = 37;
            $path = 'image/settings/logo/';
            $name = $request->input('title') . '-' . rand(1111, 9999) . '.webp';
            $setting_data['logo'] = $name;
            if (!empty($setting->logo)) {
                Helper::unlinkImage($path, $setting->logo);
            }
            Helper::uploadImage($photo, $width, $height, $path, $name);
        }
        if ($request->file('favicone')) {
            $photo =   $request->file('favicone');
            $width = 37;
            $height = 40;
            $path = 'image/settings/favicone/';
            $name = $request->input('title') . '-' . rand(1111, 9999) . '.webp';
            $setting_data['favicone'] = $name;
            if (!empty($setting->favicone)) {
                Helper::unlinkImage($path, $setting->favicone);
            }
            Helper::uploadImage($photo, $width, $height, $path, $name);
        }


        if (!empty($setting->id)) {
            $setting->update($setting_data);
        } else {
            Setting::create($setting_data);
        }


        return redirect()->back();
    }
}
