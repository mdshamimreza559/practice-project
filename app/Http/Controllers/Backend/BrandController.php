<?php

namespace App\Http\Controllers\Backend;

use App\Helper\Helper;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Backend\Brand;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBrandRequest;
use App\Http\Requests\UpdateBrandRequest;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = Brand::orderBy('order_by', 'desc')->get();
        return view('backend.modules.brand.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.modules.brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBrandRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBrandRequest $request)
    {
        $data = $request->all();
        $data['slug'] = Str::slug($request->input('slug'));
        if ($request->file('photo')) {
            $photo = $request->file('photo');
            $width = 130;
            $height = 60;
            $path = 'image/brand/';
            $name =  Str::slug($request->input('slug') . '-' . rand(1111, 9999)) . '.webp';
            $data['photo'] = $name;
            Helper::uploadImage($photo, $width, $height, $path, $name);
        }
        Brand::create($data);
        session()->flash('msg', 'Brand Created successfully');
        session()->flash('cls', 'success');
        return redirect()->route('brand.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Backend\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        return view('backend.modules.brand.show', compact('brand'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Backend\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        return view('backend.modules.brand.edit', compact('brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBrandRequest  $request
     * @param  \App\Models\Backend\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateBrandRequest $request, Brand $brand)
    {
        $data = $request->all();
        $data['slug'] = Str::slug($request->input('slug'));
        if ($request->file('photo')) {
            $photo = $request->file('photo');
            $width = 130;
            $height = 60;
            $path = 'image/brand/';
            $name = Str::slug($request->input('slug') . '-' . rand(1111, 9999)) . '.webp';
            $data['photo'] = $name;

            if ($brand->photo != null) {
                Helper::unlinkImage($path, $brand->photo);
            }
            Helper::uploadImage($photo, $width, $height, $path, $name);
        }
        $brand->update($data);
        session()->flash('msg', 'Brand updated successfully');
        session()->flash('cls', 'success');
        return redirect()->route('brand.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Backend\Brand  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        //
    }

    /**
     * Summary of brandStatus
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function brandStatus(Request $request)
    {
        $mode = $request->status;

        if ($mode == 1) {
            Brand::where('id', $request->value)->update(['status' => 1]);
        } else {
            Brand::where('id', $request->value)->update(['status' => 2]);
        }
        return response()->json(['msg' => 'Brand status update successfully', 'status' => $mode]);
    }
}
