<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Gloudemans\Shoppingcart\Facades\Cart;

class WishlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function moveToCart(Request $request)
    {
        $item = Cart::instance('wishlist')->get($request->input('product_id'));
        // dd($item->price);
        Cart::instance('wishlist')->remove($request->input('product_id'));

        $results = Cart::instance('shopping')->add($item->id, $item->name, 1, $item->price)->associate('App\Models\Product');

        if ($results) {
            $response['status'] = true;
            $response['msg'] = "Item move to cart successfully";
            $response['cart_count'] = Cart::instance('shopping')->count();
        }

        if ($request->ajax()) {
            $header = view('frontend.includes.topbar')->render();
            $wish_list = view('frontend.includes._wishlist_list')->render();
            $response['header'] = $header;
            $response['wishlist'] = $wish_list;
        }


        return $response;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check()) {
            // $count = ;
            $wishlists = Wishlist::with('product')->where('user_id', Auth::user()->id)->orderBy('id', 'desc')->get();
            // dd($wishlists);
            return view('frontend.pages.wishlists.wishlists', compact('wishlists'));
        } else {
            return redirect()->route('user.login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product_id = $request->input('product_id');
        $product_qty = $request->input('product_qty');
        $product = Product::getCartProduct($product_id);
        $price = $product[0]['offer_price'];

        $wishlist_array = [];

        foreach (Cart::instance('wishlist')->content() as $item) {
            $wishlist_array[] = $item->id;
        }
        if (in_array($product_id, $wishlist_array)) {
            $response['present'] = true;
            $response['msg'] = "Item is already in your wishlist";
        } else {
            $results = Cart::instance('wishlist')->add($product_id, $product[0]['title'], $product_qty, $price)->associate('App\Models\Product');

            if ($results) {
                $response['status'] = true;
                $response['msg'] = "Item has been saved wishlist";
                $response['wishlist_count'] = Cart::instance('wishlist')->count();
            }
        }
        return $response;
    }


    public function delete(Request $request)
    {
        $wish_id = $request->input('wish_id');
        Cart::instance('wishlist')->remove($wish_id);
        $response['status'] = true;
        $response['msg'] = 'Successfully remove wishlist item';
        $response['wishlist_count'] = Cart::instance('wishlist')->count();
        if ($request->ajax()) {
            $header = view('frontend.includes.topbar')->render();
            $wishlist_list = view('frontend.includes._wishlist_list')->render();
            $response['header'] = $header;
            $response['wishlist_list'] = $wishlist_list;
        }
        return $response;
    }
}
