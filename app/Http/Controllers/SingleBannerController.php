<?php

namespace App\Http\Controllers;

use App\Helper\Helper;
use App\Models\SingleBanner;
use Illuminate\Http\Request;
use App\Http\Requests\StoreSingleBannerRequest;
use App\Http\Requests\UpdateSingleBannerRequest;

class SingleBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $models = SingleBanner::orderBy('id', 'desc')->get();
        // dd($models);
        return view('backend.modules.single-banner.index', compact('models'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.modules.single-banner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreSingleBannerRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSingleBannerRequest $request)
    {
        $single_data = $request->all();
        if ($request->file('photo')) {
            $photo = $request->file('photo');
            $width = 575;
            $height = 200;
            $path = 'image/banner/single-banner/';
            $name = 'singel-banner' . '-' . rand(66666, 999999) . '.webp';
            $single_data['photo'] = $name;

            Helper::uploadImage($photo, $width, $height, $path, $name);
        }
        SingleBanner::create($single_data);
        session()->flash('msg', 'single banner create successfully');
        session()->flash('cls', 'success');
        return redirect()->route('single-banner.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SingleBanner  $singleBanner
     * @return \Illuminate\Http\Response
     */
    public function show(SingleBanner $singleBanner)
    {
        return view('backend.modules.single-banner.show', compact('singleBanner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SingleBanner  $singleBanner
     * @return \Illuminate\Http\Response
     */
    public function edit(SingleBanner $singleBanner)
    {
        return view('backend.modules.single-banner.edit', compact('singleBanner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateSingleBannerRequest  $request
     * @param  \App\Models\SingleBanner  $singleBanner
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSingleBannerRequest $request, SingleBanner $singleBanner)
    {
        $update_single_data = $request->all();

        if ($request->file('photo')) {
            $photo = $request->file('photo');
            $width = 570;
            $height = 200;
            $path = 'image/banner/single-banner/';
            $name = 'single-banner-image' . '-' . rand(66666, 99999) . '.webp';
            $update_single_data['photo'] = $name;

            if ($singleBanner->photo != null) {
                Helper::unlinkImage($path, $singleBanner->photo);
            }
            Helper::uploadImage($photo, $width, $height, $path, $name);
        }
        $singleBanner->update($update_single_data);
        session()->flash('msg', 'single-banner update successfully');
        session()->flash('cls', 'success');
        return redirect()->route('single-banner.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SingleBanner  $singleBanner
     * @return \Illuminate\Http\Response
     */
    public function destroy(SingleBanner $singleBanner)
    {
        if ($singleBanner->photo != null) {
            $path = 'image/banner/single-banner/';
            Helper::unlinkImage($path, $singleBanner->photo);
        }
        $singleBanner->delete();
        session()->flash('msg', 'single banner delete successfull');
        session()->flash('cls', 'warning');
        return redirect()->back();
    }


    public function singleStatus(Request $request)
    {
        $mode = $request->mode;

        if ($mode == 1) {
            SingleBanner::where('id', $request->id)->update(['status' => 1]);
        } else {
            SingleBanner::where('id', $request->id)->update(['status' => 2]);
        }
        return response()->json(['status' => $mode]);
    }
}
