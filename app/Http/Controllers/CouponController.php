<?php

namespace App\Http\Controllers;

use App\Models\Coupon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules;
use App\Http\Requests\StoreCouponRequest;
use App\Http\Requests\UpdateCouponRequest;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coupon_data = Coupon::orderBy('id', 'DESC')->get();
        return view('backend.modules.cupon.index', compact('coupon_data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.modules.cupon.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCouponRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCouponRequest $request)
    {
        $coupon = $request->all();
        Coupon::create($coupon);
        session()->flash('msg', 'Coupon create successfully');
        session()->flash('cls', 'success');
        return redirect()->route('cupon.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $coupon = Coupon::findOrFail($id);
        return view('backend.modules.cupon.show', compact('coupon'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coupon = Coupon::findOrFail($id);
        return view('backend.modules.cupon.edit', compact('coupon'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCouponRequest  $request
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCouponRequest $request,  $id)
    {
        $coupon = Coupon::findOrFail($id);
        $coupon->update($request->all());
        session()->flash('msg', 'Coupon Updated successfully');
        session()->flash('cls', 'success');
        return redirect()->route('cupon.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $coupon = Coupon::findOrFail($id);
        $coupon->delete();
        session()->flash('msg', 'Coupon deleted successfully');
        session()->flash('cls', 'warning');
        return redirect()->back();
    }

    public function couponStatus(Request $request)
    {
        $mode = $request->mode;
        if ($mode == 1) {
            Coupon::where('id', $request->value)->update(['status' => 1]);
        } else {
            Coupon::where('id', $request->value)->update(['status' => 2]);
        }
        return response()->json(['status' => $mode]);
    }
}
