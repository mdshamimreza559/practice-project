<?php

namespace App\Http\Controllers;

use App\Helper\Helper;
use App\Models\FetureBanne;
use Illuminate\Http\Request;
use App\Http\Requests\StoreFetureBanneRequest;
use App\Http\Requests\UpdateFetureBanneRequest;

class FetureBanneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $feature_banner = FetureBanne::orderBy('id', 'desc')->get();
        return view('backend.modules.feature-banner.index', compact('feature_banner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.modules.feature-banner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreFetureBanneRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFetureBanneRequest $request)
    {
        $feature_data = $request->all();

        if ($request->file('photo')) {
            $photo = $request->file('photo');
            $width = 270;
            $height = 460;
            $path = 'image/banner/feature-banner/';
            $name = 'feature-image-' . rand(111, 999) . '.webp';
            $feature_data['photo'] = $name;
            Helper::uploadImage($photo, $width, $height, $path, $name);
        }
        FetureBanne::create($feature_data);
        session()->flash('msg', 'Feature banner create successfully');
        session()->flash('cls', 'success');
        return redirect()->route('feature-banner.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FetureBanne  $fetureBanne
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $fetureBanne = FetureBanne::findOrFail($id);
        return view('backend.modules.feature-banner.show', compact('fetureBanne'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FetureBanne  $fetureBanne
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fetureBanne = FetureBanne::findOrFail($id);
        return view('backend.modules.feature-banner.edit', compact('fetureBanne'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateFetureBanneRequest  $request
     * @param  \App\Models\FetureBanne  $fetureBanne
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFetureBanneRequest $request, $id)
    {
        $fetureBanne = FetureBanne::findOrFail($id);
        $feature_data = $request->all();

        if ($request->file('photo')) {
            $photo = $request->file('photo');
            $width = 270;
            $height = 460;
            $path = 'image/banner/feature-banner/';
            $name = 'feature-image-' . rand(111, 999) . '.webp';
            $feature_data['photo'] = $name;
            if ($fetureBanne->photo != null) {
                Helper::unlinkImage($path, $fetureBanne->photo);
            }
            Helper::uploadImage($photo, $width, $height, $path, $name);
        }
        $fetureBanne->update($feature_data);
        session()->flash('msg', 'Feature banner update successfully');
        session()->flash('cls', 'success');
        return redirect()->route('feature-banner.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FetureBanne  $fetureBanne
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fetureBanne = FetureBanne::findOrFail($id);
        $path = 'image/banner/feature-banner/';
        if ($fetureBanne->photo != null) {
            Helper::unlinkImage($path, $fetureBanne->photo);
        }
        $fetureBanne->delete();
        session()->flash('msg', 'Feature banner delete successfully');
        session()->flash('cls', 'warning');
        return redirect()->route('feature-banner.index');
    }

    /**
     * Summary of featureStatus
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function featureStatus(Request $request)
    {
        $mode = $request->mode;

        if ($mode == 1) {
            FetureBanne::where('id', $request->id)->update(['status' => 1]);
        } else {
            FetureBanne::where('id', $request->id)->update(['status' => 2]);
        }
        return response()->json(['status' => $mode]);
    }
}
