<?php

namespace App\Http\Controllers;

use App\Helper\Helper;
use App\Models\Banner;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::orderBy('id', 'desc')->get();
        return view('backend.modules.banner.index', compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.modules.banner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $banner_data = $request->all();

        if ($request->file('photo')) {
            $photo = $request->file('photo');
            $width = 590;
            $height = 430;
            $path = 'image/banner/';
            $name = 'banner-img-' . rand(11111, 99999) . '.webp';
            $banner_data['photo'] = $name;
            Helper::uploadImage($photo, $width, $height, $path, $name);
        }
        Banner::create($banner_data);
        session()->flash('msg', 'banner create successfully');
        session()->flash('cls', 'success');
        return redirect()->route('banners.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function show(Banner $banner)
    {
        // dd($banner);
        return view('backend.modules.banner.show', compact('banner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function edit(Banner $banner)
    {
        // dd($banner);
        return view('backend.modules.banner.edit', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banner $banner)
    {
        $update_data = $request->all();
        $update_data['name'] = $request->input('name');
        if ($request->file('photo')) {
            $photo = $request->file('photo');
            $width = 590;
            $height = 430;
            $path = 'image/banner/';
            $name = 'banner-img-' . rand(22222, 99999) . '.webp';
            $update_data['photo'] = $name;

            if ($banner->photo != null) {
                Helper::unlinkImage($path, $banner->photo);
            }
            Helper::uploadImage($photo, $width, $height, $path, $name);
        }
        $banner->update($update_data);
        session()->flash('msg', 'Banner updated sucessfuly');
        session()->flash('cls', 'success');
        return redirect()->route('banners.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Banner  $banner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banner $banner)
    {
        if ($banner->photo != null) {
            $path = 'image/banner/';
            Helper::unlinkImage($path, $banner->photo);
        }
        $banner->delete();
        session()->flash('msg', 'Banner delete successfully');
        session()->flash('cls', 'warning');
        return redirect()->back();
    }


    /**
     * Summary of bannerStatus
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function bannerStatus(Request $request)
    {
        $mode = $request->mode;
        if ($mode == 1) {
            Banner::where('id', $request->id)->update(['status' => 1]);
        } else {
            Banner::where('id', $request->id)->update(['status' => 2]);
        }
        return response()->json(['msg' => 'Banner status updated successfully', 'status' => $mode]);
    }
}
