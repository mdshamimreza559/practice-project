<?php

namespace App\Http\Controllers;

use App\Helper\Helper;
use App\Models\ListBanner;
use Illuminate\Http\Request;
use App\Http\Requests\StoreListBannerRequest;
use App\Http\Requests\UpdateListBannerRequest;

class ListBannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list_banner = ListBanner::orderBy('id', 'desc')->get();
        return view('backend.modules.list-banner.index', compact('list_banner'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.modules.list-banner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreListBannerRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreListBannerRequest $request)
    {
        $list_banner_data = $request->all();

        if ($request->file('photo')) {
            $photo = $request->file('photo');
            $width = 370;
            $height = 230;
            $path = 'image/banner/list-banner/';
            $name = 'list-banner-image' . '-' . rand(11111, 99999) . '.webp';
            $list_banner_data['photo'] = $name;

            Helper::uploadImage($photo, $width, $height, $path, $name);
        }
        ListBanner::create($list_banner_data);
        session()->flash('msg', 'list banner create successfull');
        session()->flash('cls', 'success');
        return redirect()->route('list-banner.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ListBanner  $listBanner
     * @return \Illuminate\Http\Response
     */
    public function show(ListBanner $listBanner)
    {
        return view('backend.modules.list-banner.show', compact('listBanner'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ListBanner  $listBanner
     * @return \Illuminate\Http\Response
     */
    public function edit(ListBanner $listBanner)
    {
        return view('backend.modules.list-banner.edit', compact('listBanner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateListBannerRequest  $request
     * @param  \App\Models\ListBanner  $listBanner
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateListBannerRequest $request, ListBanner $listBanner)
    {
        $update_data = $request->all();

        if ($request->file('photo')) {
            $photo = $request->file('photo');
            $width = 370;
            $height = 230;
            $path = 'image/banner/list-banner/';
            $name = 'list-banner-image' . '-' . rand(11111, 99999) . '.webp';
            $update_data['photo'] = $name;

            if ($listBanner != null) {
                Helper::unlinkImage($path, $listBanner->photo);
            }
            Helper::uploadImage($photo, $width, $height, $path, $name);
        }
        $listBanner->update($update_data);
        session()->flash('msg', 'list banner updated successfully');
        session()->flash('cls', 'success');
        return redirect()->route('list-banner.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ListBanner  $listBanner
     * @return \Illuminate\Http\Response
     */
    public function destroy(ListBanner $listBanner)
    {
        if ($listBanner != null) {
            $path = 'image/banner/list-banner/';
            Helper::unlinkImage($path, $listBanner->photo);
        }
        $listBanner->delete();
        session()->flash('msg', 'list banner delete successfully');
        session()->flash('cls', 'warning');
        return redirect()->back();
    }

    public function listBannerStatus(Request $request)
    {
        $mode = $request->value;
        if ($mode == 1) {
            ListBanner::where('id', $request->id)->update(['status' => 1]);
        } else {
            ListBanner::where('id', $request->id)->update(['status' => 2]);
        }
        return response()->json(['status' => $mode]);
    }
}
