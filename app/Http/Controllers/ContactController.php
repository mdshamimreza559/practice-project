<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function contactStore(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:2|max:255',
            'email' => 'required|min:3|max:255',
            'subject' => 'required|max:255',
            'message' => 'required|max:1200',
        ]);
        $data =  $request->all();
        Contact::create($data);
        session()->flash('msg', 'your message send successfully');
        session()->flash('cls', 'success');
        return redirect()->back();
    }
}
