<?php

namespace App\Http\Controllers\Frontend;

use Rules\Password;
use App\Helper\Helper;
use App\Models\Banner;
use App\Models\OurTeam;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Category;
use App\Models\ListBanner;
use App\Models\FetureBanne;
use App\Models\SubCategory;
use App\Models\SingleBanner;
use Illuminate\Http\Request;

use App\Http\Middleware\User;
use App\Models\Backend\Brand;
use App\Models\ProductReview;
use App\Models\Backend\AboutUs;
use App\Models\ProductAttribute;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\FetureProductsBanner;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\FetureBanneController;

class FrontendController extends Controller
{

    public function quickView(Request $request)
    {
        $product = Product::where('status', 1)->where('id', $request->id)->first();
        return view('frontend.pages.quickview', compact('product'));
    }

    /**
     * Summary of index
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $single_banner = SingleBanner::where('status', 1)->orderBy('id', 'desc')->take(2)->get();

        $list_banner = ListBanner::where('status', 1)->orderBy('id', 'desc')->take(2)->get();
        $s_list_banner = ListBanner::where('status', 1)->orderBy('id', 'desc')->skip(2)->take(3)->get();
        $feature_product_banner = FetureBanne::where('status', 1)->orderBy('id', 'desc')->take(1)->get();

        $banners = Banner::where('status', 1)->orderBy('id', 'desc')->take(3)->get();
        $categories = Category::with('subcategory')->where('status', 1)->orderBy('id', 'desc')->get();
        $feature_product = Product::with('category')->where('status', 1)->where('is_featured', 1)->take(4)->orderBy('id', 'desc')->get();
        $new_products = Product::with('category', 'brand',)->where('trendding_products', 0)->where('is_featured', 0)->where('status', 1)->orderBy('id', 'desc')->take(8)->get();

        $trendding_products = Product::with('category', 'subcategory', 'brand')->where('trendding_products', 1)->where('status', 1)->take(9)->get();

        // best selling
        $items = DB::table('order_products')->select('product_id', DB::raw('COUNT(product_id) as count'))->groupBy('product_id')->orderBy('count', 'desc')->get();

        $product_id = [];
        foreach ($items as $item) {
            $product_id[] =  $item->product_id;
        }

        if (!empty($product_id)) {
            $best_selling =  Helper::topRated($product_id);
        }

        // Top rated products
        $top_rated = DB::table('product_reviews')->select('product_id', DB::raw('AVG(rate) as count'))->groupBy('product_id')->orderBy('count', 'desc')->get();

        $top_rate = [];
        foreach ($top_rated as $item) {
            $top_rate[] = $item->product_id;
        }

        $placeholders = implode(',', array_fill(0, count($top_rate), '?'));

        $top_rates = Product::whereIn('id', $top_rate)
            ->orderByRaw("field(id,{$placeholders})", $top_rate)->take(6)->get();


        return view('frontend.index', compact('new_products', 'categories', 'banners', 'feature_product',  'trendding_products', 'single_banner', 'best_selling', 'top_rates', 'list_banner', 's_list_banner', 'feature_product_banner'));
    }

    /**
     * Summary of categories
     * @param Request $request
     * @param mixed $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function categories(Request $request, $slug)
    {
        $banners = SingleBanner::where('status', 1)->orderBy('id', 'desc')->take(1)->get();

        $categories = Category::with('products')->where('slug', $slug)->first();
        // dd($categories);
        $side_categories = Category::where('status', 1)->orderBy('id', 'desc')->get();
        $brands = Brand::where('status', 1)->orderBy('id', 'desc')->get();
        $route = 'categories';
        $sort = '';
        if ($request->sort != null) {
            $sort = $request->sort;
        }
        if ($categories == null) {
            return view('frontend.error.404');
        } else {
            if ($sort == 'high-price') {
                $products = Product::where('status', 1)->where('category_id', $categories->id)->orderBy('offer_price', 'DESC')->paginate(12);
                // return $products;
            } elseif ($sort == 'low-high') {
                $products = Product::where('status', 1)->where('category_id', $categories->id)->orderBy('offer_price', 'ASC')->paginate(12);
            } elseif ($sort == 'new-to-old-product') {
                $products = Product::where('status', 1)->where('category_id', $categories->id)->orderBy('id', 'DESC')->paginate(12);
            } elseif ($sort == 'old-to-new-product') {
                $products = Product::where('status', 1)->where('category_id', $categories->id)->orderBy('id', 'ASC')->paginate(12);
            } elseif ($sort == 'alphabetical') {
                $products = Product::where('status', 1)->where('category_id', $categories->id)->orderBy('title', 'ASC')->paginate(12);
            } else {
                $products = Product::where('status', 1)->where('category_id', $categories->id)->paginate(12);
            }
        }

        if ($request->ajax()) {
            $view = view('frontend.pages._single_categeories')->render();
            return response()->json(['html' => $view]);
        }
        return view('frontend.pages.category', compact('banners', 'route', 'categories', 'products', 'brands', 'side_categories'));
    }

    /**
     * Summary of singleProduct
     * @param mixed $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function singleProduct($slug)
    {
        $single_products = Product::with('productAttr', 'category')->where('slug', $slug)->where('status', 1)->first();

        $related_products = Product::where('category_id',  $single_products->category_id)->get();

        // $review = ProductReview::with('user')
        //     ->where('product_id', $single_products->id)
        //     ->latest()
        //     ->get();
        // dd($single_products);

        return view('frontend.pages.single-page', compact('single_products', 'related_products'));
    }
    /**
     * Summary of userLogin
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function userLogin()
    {
        return view('frontend.pages.login');
    }

    /**
     * Summary of loginSubmit
     * @param Request $request
     * @return mixed
     */
    public function loginSubmit(Request $request)
    {
        $this->validate($request, [
            'email' => 'email|required|exists:users,email',
            'password' => 'required|min:6',
        ]);

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'role' => 0])) {
            Session::put('user', $request->email);
            if (Session::get('url.intendend')) {
                return Redirect::to(Session::get('url.intended'));
            } else {
                return redirect()->route('front.index');
            }
        } elseif (Auth::attempt(['email' => $request->email, 'password' => $request->password, 'role' => 1])) {
            return redirect()->route('dashboard.index');
        } else {
            return back()->with('error', 'Email & password dont\'t match');
        }
    }

    public function userLogout()
    {
        Session::forget('user');
        Auth::logout();
        return redirect()->route('front.index');
    }

    public function productBrand($slug)
    {
        $banners = SingleBanner::where('status', 1)->orderBy('id', 'desc')->take(1)->get();
        $brands = Brand::where('status', 1)->where('slug', $slug)->first();
        $products = Product::where('brand_id', $brands->id)->where('status', 1)->paginate(5);

        return view('frontend.pages.brand', compact('banners', 'brands', 'products'));
    }

    /**
     * Summary of shop
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function shop(Request $request)
    {
        $products = Product::query();

        // category Filter
        $cat_ids = [];
        if (!empty($_GET['category'])) {
            $slugs = explode(',', $_GET['category']);
            $cat_ids = Category::select('id')->whereIn('slug', $slugs)->pluck('id')->toArray();
            $products = $products->whereIn('category_id', $cat_ids);
        }

        // Brand Filter
        if (!empty($_GET['brands'])) {
            $slugs = explode(',', $_GET['brands']);
            $brand_ids = Brand::select('id')->whereIn('slug', $slugs)->pluck('id')->toArray();
            $products = $products->whereIn('brand_id', $brand_ids);
        }

        // Size Filter
        if (!empty($_GET['size'])) {
            $products = $products->where('size', $_GET['size']);
        }
        // Sort Filter

        if (!empty($_GET['sortBy'])) {
            $sort = $_GET['sortBy'];
            if ($sort == 'high-price') {
                $products =  $products->where('status', 1)->orderBy('offer_price', 'DESC')->paginate(15);
            }
            if ($sort == 'low-high') {
                $products =  $products->where('status', 1)->orderBy('offer_price', 'ASC')->paginate(15);
            }
            if ($sort == 'new-to-old-product') {
                $products =  $products->where('status', 1)->orderBy('id', 'DESC')->paginate(15);
            }
            if ($sort == 'old-to-new-product') {
                $products =  $products->where('status', 1)->orderBy('id', 'ASC')->paginate(15);
            }
            if ($sort == 'alphabetical') {
                $products =  $products->where('status', 1)->orderBy('title', 'ASC')->paginate(15);
            }
        } else {
            $products =   $products->with('brand')->where('status', 1)->orderBy('id', 'desc')->paginate(15);
        }
        $banners = SingleBanner::where('status', 1)->orderBy('id', 'desc')->take(1)->get();


        $categories = Category::with('subcategory')->where('status', 1)->take(6)->orderBy('id', 'desc')->get();
        $filter_category = Category::with('subcategory', 'products')->where('status', 1)->take(4)->orderBy('id', 'desc')->get();
        $brands = Brand::with('products')->where('status', 1)->orderBy('id', 'desc')->take(7)->get();

        $count_products = Product::where(function ($q) use ($request, $cat_ids) {
            if (isset($request->category) && !empty($cat_ids)) {
                $q->whereIn('category_id', $cat_ids);
            }
        })->where('status', 1)->get();

        return view('frontend.pages.shop', compact('banners', 'count_products', 'products', 'categories', 'brands', 'filter_category'));
    }

    /**
     * Summary of shopFilter
     * @param Request $request
     * @return mixed
     */
    public function shopFilter(Request $request)
    {
        $data = $request->all();

        // Category Sort
        $catUrl = '';
        if (!empty($data['category'])) {
            foreach ($data['category'] as $category) {
                if (empty($catUrl)) {
                    $catUrl .= '&category=' . $category;
                } else {
                    $catUrl .= ',' . $category;
                }
            }
        }

        // Sort Filter
        $sortUrl = '';
        if (!empty($data['sortBy'])) {
            $sortUrl .= '&sortBy=' . $data['sortBy'];
        }

        // Brand Filter
        $brandUrl = '';
        if (!empty($data['brands'])) {
            foreach ($data['brands'] as $brand) {
                if (empty($brandUrl)) {
                    $brandUrl .= '&brands=' . $brand;
                } else {
                    $brandUrl .= ',' . $brand;
                }
            }
        }

        // Size Filtter
        $productSize = '';
        if (!empty($data['size_final'])) {
            $productSize .= '&size=' . $data['size_final'];
        }
        return \redirect()->route('shop',  $brandUrl . $catUrl . $sortUrl . $productSize);
    }

    /**
     * Summary of autoSearch
     * @param Request $request
     * @return array
     */
    public function autoSearch(Request $request)
    {
        $query = $request->get('term', '');
        $products = Product::where('title', 'LIKE', '%' . $query . '%')->where('status', 1)->get();
        $data = array();

        foreach ($products as $product) {
            $data[] = array(
                'value' => $product->title,
                'id' => $product->id,
            );
        }
        if (count($data)) {
            return $data;
        } else {
            return ['value' => 'No Product found', 'id' => ''];
        }
    }

    /**
     * Summary of search
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function search(Request $request)
    {
        $data = $request->input('search');

        $products = Product::where('title', 'LIKE', '%' . $data . '%')->where('status', 1)->get();

        $banners = SingleBanner::where('status', 1)->orderBy('id', 'desc')->take(1)->get();

        $categories = Category::with('subcategory')->where('status', 1)->take(6)->orderBy('id', 'desc')->get();
        $filter_category = Category::with('subcategory', 'products')->where('status', 1)->take(4)->orderBy('id', 'desc')->get();
        $brands = Brand::with('products')->where('status', 1)->orderBy('id', 'desc')->take(7)->get();


        return view('frontend.pages.shop', compact('banners',  'products', 'categories', 'brands', 'filter_category'));
    }
    /**
     * Summary of productriview
     * @param Request $request
     * @return mixed
     */
    public function productriview(Request $request)
    {
        $this->validate($request, [
            'rate' => 'required|numeric',
            'reason' => 'nullable|string|max:255',
            'review' => 'nullable|string|max:255',
        ]);
        $review = $request->all();

        ProductReview::create($review);

        return redirect()->back();
    }

    /**
     * Summary of sizeFilter
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function sizeFilter(Request $request)
    {
        $size = ProductAttribute::where('id', $request->id)->first();
        $product = Product::where('id', $size->product_id)->first();
        $attr = ProductAttribute::where('product_id', $size->product_id)->get();
        // return $size;
        return view('frontend.pages._single_size', compact('product', 'size', 'attr'));
    }

    /**
     * Summary of aboutUs
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function aboutUs()
    {
        $about = AboutUs::first();
        $ourTeam = OurTeam::where('status', 1)->orderBy('id', 'desc')->get();
        return view('frontend.pages.about', compact('about', 'ourTeam'));
    }

    public function contactUs()
    {
        return view('frontend.pages.contact');
    }
}
