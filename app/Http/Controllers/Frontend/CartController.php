<?php


namespace App\Http\Controllers\Frontend;

use App\Models\Coupon;
use App\Models\Product;
use App\Models\Setting;
use Illuminate\Http\Request;

use function PHPUnit\Framework\at;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Gloudemans\Shoppingcart\Facades\Cart;

class CartController extends Controller
{


    /**
     * Summary of cartStore
     * @param Request $request
     * @return bool|string
     */
    public function cartStore(Request $request)
    {
        if (Auth::user()) {
            $product_qty = $request->input('product_qty');
            $product_id = $request->input('product_id');
            $product = Product::getCartProduct($product_id);
            $price = $product[0]['offer_price'];
            $cart_array = [];
            foreach (Cart::instance('shopping')->content() as $item) {
                $cart_array[] = $item->id;
            }
            $results = Cart::instance('shopping')->add($product_id, $product[0]['title'], $product_qty, $price)->associate('App\Models\Product');
            if ($results) {
                $response['status'] = true;
                $response['product_id'] = $product_id;
                $response['total'] = Cart::subtotal();
                $response['cart_count'] = Cart::instance('shopping')->count();

                $response['msg'] = 'Successfully add cart item';
            }
            if ($request->ajax()) {
                $header = view('frontend.includes.topbar')->render();
                $cart_count = Cart::instance('shopping')->count();
                $response['header'] = $header;
                $response['cart_count'] = $cart_count;
            }
            return json_encode($response);
        } else {
            $response['msg'] = 'Log in first';
            $response['status'] = 'false';
            $response['url'] = route('user.login');
            return json_encode($response);
        }
    }

    /**
     * Summary of cartDelete
     * @param Request $request
     * @return bool|string
     */
    public function cartDelete(Request $request)
    {
        $cart_data = $request->input('cart_id');
        Cart::instance('shopping')->remove($cart_data);
        $response['status'] = true;
        $response['msg'] = 'Successfully remove cart item';
        $response['total'] = Cart::subtotal();
        $response['cart_count'] = Cart::instance('shopping')->count();

        if ($request->ajax()) {
            $header = view('frontend.includes.topbar')->render();
            $cart_lists = view('frontend.includes._cart_lists')->render();
            $response['header'] = $header;
            $response['cart_lists'] = $cart_lists;
        }
        return json_encode($response);
    }
    /**
     * Summary of cartDetails
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function cartDetails()
    {
        $setting = Setting::first();
        return view('frontend.pages.cart', compact('setting'));
    }

    /**
     * Summary of cartCheckOut
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function cartCheckOut()
    {

        return view('frontend.pages.checkout');
    }

    public function cartUpdate(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required|numeric'
        ]);
        $rowId = $request->input('rowId');
        $product_qty = $request->input('product_qty');
        $stockQuantity = $request->input('stockQuantity');

        if ($product_qty > $stockQuantity) {
            $message = "We currently do't item enough stock";
            $response['status'] = false;
        } elseif ($product_qty < $stockQuantity) {
            $message = "You can't less than 1 quantity";
        } else {
            Cart::instance('shopping')->update($rowId, $product_qty);
            $message = "successfully update product quantity";
            $response['status'] = true;
            $response['total'] = Cart::subtotal();
            $response['cart_count'] = Cart::instance('shopping')->count();
        }

        if ($request->ajax()) {
            $header = view('frontend.includes.topbar')->render();
            $cart_list = view('frontend.includes._cart_lists')->render();
            $response['message'] = $message;
            $response['header'] = $header;
            $response['cart_list'] = $cart_list;
        }
        return $response;







        // $this->validate($request, [
        //     'product_id' => 'required|numeric'
        // ]);
        // $rowId = $request->input('id');
        // $product_qty = $request->input('product_id');
        // $stockQuantity = $request->input('product_qty');

        // if ($product_qty > $stockQuantity) {
        //     $message = 'We currently do not item enough stock';
        //     $response['status'] = false;
        // } elseif ($product_qty < 1) {
        //     $message = "You can't less than 1 quantity";
        //     $response['status'] = false;
        // } else {
        //     Cart::instance('shopping')->update($rowId, $product_qty);
        //     $message = "Quantity updated successfully";
        //     $response['total'] = Cart::subtotal();
        //     $response['cart_count'] = Cart::instance('shopping')->count();
        //     $response['status'] = true;
        // }
        // if ($request->ajax()) {
        //     $header = view('frontend.includes.topbar')->render();
        //     $cart = view('frontend.includes._cart_lists')->render();
        //     $response['header'] = $header;
        //     $response['cart_list'] = $cart;
        //     $response['message'] = $message;
        // }
        // return $response;
    }

    public function couponAdd(Request $request)
    {
        $coupon = Coupon::where('code', $request->code)->first();
        // dd($coupon);
        if (!$coupon) {
            session()->flash('msg', 'Invalid coupon code! please valid coupon code');
            session()->flash('cls', 'warning');
            return redirect()->back();
        }
        if ($coupon) {
            $total_price = Cart::instance('shopping')->subtotal();
            session()->put('coupon', [
                'id' => $coupon->id,
                'code' => $coupon->code,
                'value' => $coupon->discount($total_price),
            ]);
            session()->flash('msg', 'Successfully coupon applied');
            session()->flash('cls', 'success');
            return redirect()->back();
        }
    }
}
