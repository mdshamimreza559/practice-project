<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Order;
use App\Mail\OrderMail;
use App\Models\Setting;
use App\Models\Shipping;
use Illuminate\Support\Str;
use App\Models\OrderProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Gloudemans\Shoppingcart\Facades\Cart;

class CheckOutController extends Controller
{

    /**
     * Summary of checkOut1
     * @return mixed
     */
    public function checkOut1()
    {

        if (Auth::check()) {
            $setting = Setting::first();
            return view('frontend.pages.checkout.checkout', compact('setting'));
        } else {
            return redirect()->route('user.login');
        }
    }

    /**
     * Summary of checkOut1Store
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */

    public function checkOut1Store(Request $request)
    {
        $cart_array = [];
        $cart_qty = [];
        foreach (Cart::instance('shopping')->content() as $item) {
            $cart_array[] = $item->id;
            $cart_qty[] = $item->qty;
            $cart_price[] = $item->price;
        }
        Session::put(
            'checkout',
            [
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'company' => $request->company,
                'email' => $request->email,
                'phone' => $request->phone_number,
                'country' => $request->country,
                'address' => $request->address,
                'apartment_suite' => $request->apartment_suite,
                'city' => $request->city,
                'state' => $request->state,
                'postcode' => $request->postcode,
                'note' => $request->note,
                'sfirst_name' => $request->sfirst_name,
                'slast_name' => $request->slast_name,
                'scompany' => $request->scompany,
                'semail' => $request->semail,
                'sphone' => $request->sphone,
                'scountry' => $request->scountry,
                'saddress' => $request->saddress,
                'sapartment_suite' => $request->sapartment_suite,
                'scity' => $request->scity,
                'sstate' => $request->sstate,
                'spostcode' => $request->spostcode,
                'sub_total' => str_replace(',', '', $request->sub_total),
                'total_amount' => $request->total_amount,
                'product_id' => $cart_array,
                'quantity' => $cart_qty,
                'price' => $cart_price,
            ]
        );
        $setting = Setting::first();
        $shippings = Shipping::where('status', 1)->orderBy('id', 'desc')->get();
        return view('frontend.pages.checkout.checkout2', compact('shippings', 'setting'));
    }

    /**
     * Summary of checkOut2Store
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function checkOut2Store(Request $request)
    {
        Session::push(
            'checkout',
            [
                'price' => $request->price,
            ]
        );
        $setting = Setting::first();
        return view('frontend.pages.checkout.checkout3', compact('setting'));
    }

    public function checkOut3Store(Request $request)
    {
        // return $request->all();
        Session::push(
            'checkout',
            [
                'payment_method' => $request->payment_method,
                'payment_status' => 'unpaid',
            ]
        );
        $subtotal = Cart::instance('shopping')->subtotal();

        $checkout = Session::get('checkout')[0]['price'];
        $setting = Setting::first();
        return view('frontend.pages.checkout.checkout4', compact('subtotal',  'checkout', 'setting'));
    }

    public function checkOut4Store(Request $request)
    {
        try {
            $order = new Order();

            $order['user_id'] = Auth::user()->id;
            $order['order_number'] = Str::upper('EM-' . Str::random(7));
            if (Session::has('coupon')) {
                $order['coupon'] = Session::get('coupon')['value'];
            } else {
                $order['coupon'] = 0;
            }

            $order['sub_total'] =  (float) str_replace(',', '', Session::get('checkout')['sub_total']);

            if (Session::get('checkout')[0]['price'] == 'Free') {
                $order['total_amount'] = (float) str_replace(',', '', Session::get('checkout')['sub_total']) + 0;
            } else {
                $order['total_amount'] = (float) str_replace(',', '', Session::get('checkout')['sub_total']) + Session::get('checkout')[0]['price'];
            }

            $order['payment_method'] = Session::get('checkout')['1']['payment_method'];
            if (Session::get('checkout')[0]['price'] == 'Free') {
                $order['delivery_charge'] = 0;
            } else {
                $order['delivery_charge'] = Session::get('checkout')['0']['price'];
            }
            $order['payment_status'] = Session::get('checkout')['1']['payment_status'];
            $order['condition'] = 'pending';

            $order['first_name'] = Session::get('checkout')['first_name'];
            $order['last_name'] = Session::get('checkout')['last_name'];
            $order['email'] = Session::get('checkout')['email'];
            $order['phone'] = Session::get('checkout')['phone'];
            $order['country'] = Session::get('checkout')['country'];
            $order['company_name'] = Session::get('checkout')['company'];
            $order['address'] = Session::get('checkout')['address'];
            $order['apartment_suite'] = Session::get('checkout')['apartment_suite'];
            $order['city'] = Session::get('checkout')['city'];
            $order['state'] = Session::get('checkout')['state'];
            $order['postcode'] = Session::get('checkout')['postcode'];
            $order['note'] = Session::get('checkout')['note'];

            $order['sfirst_name'] = Session::get('checkout')['sfirst_name'];
            $order['slast_name'] = Session::get('checkout')['slast_name'];
            $order['semail'] = Session::get('checkout')['semail'];
            $order['sphone'] = Session::get('checkout')['sphone'];
            $order['scountry'] = Session::get('checkout')['scountry'];
            $order['scompany_name'] = Session::get('checkout')['scompany'];
            $order['saddress'] = Session::get('checkout')['saddress'];
            $order['sapartment_suite'] = Session::get('checkout')['sapartment_suite'];
            $order['scity'] = Session::get('checkout')['city'];
            $order['sstate'] = Session::get('checkout')['sstate'];
            $order['spostcode'] = Session::get('checkout')['spostcode'];

            $status = $order->save();

            $product_id = count(Session::get('checkout')['product_id']);
            for ($i = 0; $i < $product_id; $i++) {
                $order_product = new OrderProduct;
                $order_product['order_id'] = $order->id;
                $order_product['product_id'] = Session::get('checkout')['product_id'][$i];
                $order_product['quantity'] = Session::get('checkout')['quantity'][$i];
                $order_product['price'] = Session::get('checkout')['price'][$i];
                $order_product->save();
            }
            if ($status) {
                // Mail::to($order['email'])->cc('mdshamimreza559@gmail.com')->bcc($order['semail'])->send(new OrderMail($order));
                Cart::instance('shopping')->destroy();
                Session::forget('coupon');
                Session::forget('checkout');
                return redirect()->route('checkout.complete', $order['order_number']);
            } else {
                return redirect()->route('chechkout1');
            }
        } catch (\Throwable $th) {
            return 'Something went wrong. Please try again';
        }
    }

    /**
     * Summary of checkOutComplete
     * @param mixed $order
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function checkOutComplete($order)
    {
        $order = $order;
        $order_details = Order::with('orderProducts.product')->where('user_id', Auth::user()->id)->latest()->first();
        $setting = Setting::first();
        return view('frontend.pages.checkout.complete', compact('order', 'order_details', 'setting'));
    }
}
