<?php

namespace App\Http\Requests;

use App\Models\Coupon;
use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateCouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'code' => ['required', 'min:4', 'max:255', Rule::unique('coupons')->ignore($this->user()->id, 'id')],
            'status' => 'required',
            'value' => 'required',
            'type' => 'required',
        ];
    }
}
